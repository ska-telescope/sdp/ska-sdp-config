"""Processing Block management for the SDP Configuration Database."""

from ..entity import ProcessingBlock
from .entity_operations import CollectiveEntityOperations


# pylint: disable=too-few-public-methods
class ProcessingBlockOperations(CollectiveEntityOperations):
    """Database operations related to Processing Block management."""

    PREFIX = "/pb"
    MODEL_CLASS = ProcessingBlock
    HAS_STATE = True
    HAS_OWNER = True
