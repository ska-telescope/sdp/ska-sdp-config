"""High-level API tests on flow."""

from pathlib import Path

import pytest
from pydantic import AnyUrl
from pydantic_core import Url, ValidationError

from ska_sdp_config import Config
from ska_sdp_config.entity.common import TangoAttributeUrl
from ska_sdp_config.entity.common.path import PVCPath
from ska_sdp_config.entity.flow import (
    ChannelAddressMapping,
    DataProduct,
    DataQueue,
    Display,
    Flow,
    FlowSource,
    SharedMem,
    SpeadStream,
    TangoAttribute,
    TangoAttributeMap,
    TelModel,
)

PB_ID = "pb-test-00000000-0"


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_flow"


def test_create_telmodel_flow(cfg: Config):
    """Test TelModel flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                kind="telmodel",
                name="reference",
            ),
            sink=TelModel(
                pvc_data_dir=PVCPath(
                    k8s_namespaces=["dp-shared", "dp-shared-p"],
                    k8s_pvc_name="shared-storage",
                    pvc_mount_path="/data",
                    pvc_subpath=Path(f"product/{PB_ID}/sdp/telmodel"),
                )
            ),
            sources=[
                FlowSource(
                    uri=Url(
                        "gitlab://gitlab.com/ska-telescope/sdp/"
                        "ska-sdp-tmlite-repository"
                        "?~6f2bfea63adbe4ad87578a63f36363318455967e#tmdata"
                    ),
                ),
                FlowSource(
                    uri=(
                        "gitlab://gitlab.com/ska-telescope/ska-telmodel"
                        "?~c068e6b990b0476afc57e98f23814a3ae5338b92#tmdata"
                    ),
                ),
                FlowSource(
                    uri=Url(
                        "gitlab://gitlab.com/ska-telescope/mccs/ska-low-mccs"
                        "?~9ae6772fd9e3a12d43e67d52ce182b690e3caccd#tmdata"
                    ),
                ),
            ],
            data_model="TelModel",
        )
        txn.flow.create(flow)
        assert txn.flow.path(flow.key) == f"/flow/{PB_ID}:telmodel:reference"

        result = txn.flow.get(flow.key)

        assert result == flow
        assert isinstance(result.sink, TelModel)
        assert (
            str(result.sink.pvc_data_dir)
            == f"/data/product/{PB_ID}/sdp/telmodel"
        )


def test_create_sharedmem_flow(cfg: Config):
    """Test SharedMem flow model."""
    for txn in cfg.txn():
        spead_flow = Flow(
            key=Flow.Key(pb_id=PB_ID, kind="spead", name="spead-data"),
            sink=SpeadStream(
                channel_map={
                    "calibration:b": {
                        "vis0": ChannelAddressMapping(
                            {
                                0: (0, 9000, 1),
                                1000: (1, 10000, 1),
                                2000: (2, 11000, 1),
                            }
                        ),
                    }
                },
                receiver_version="5.2.2",
            ),
            sources=[],
            data_model="Visibility",
        )
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID, kind="sharedmem", name="visibility-ingest"
            ),
            sink=SharedMem(
                impl="plasma",
                host_path="/tmp/plasma/socket",
            ),
            sources=[
                FlowSource(
                    uri=spead_flow.key,
                    function="ska-sdp-realtime-receive-processors:receiver",
                )
            ],
            data_model="Visibility",
        )
        txn.flow.create(spead_flow)
        txn.flow.create(flow)
        assert (
            txn.flow.path(flow.key)
            == f"/flow/{PB_ID}:sharedmem:visibility-ingest"
        )

        result = txn.flow.get(flow.key)
        assert result == flow

        flow_source = result.sources[0].uri
        assert txn.flow.get(flow_source).sink.channel_map["calibration:b"][
            "vis0"
        ] == {
            0: (0, 9000, 1),
            1000: (1, 10000, 1),
            2000: (2, 11000, 1),
        }


def test_create_dataproduct_flow(cfg: Config):
    """Test DataProduct flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                kind="data-product",
                name="visibilities",
            ),
            sink=DataProduct(
                data_dir=PVCPath(
                    k8s_namespaces=["dp-shared", "dp-shared-p"],
                    k8s_pvc_name="shared-storage",
                    pvc_mount_path=Path("/data"),
                    pvc_subpath=Path(f"product/{PB_ID}/sdp/vis"),
                ),
                paths=[
                    Path("out-{scan_id}-id.ms"),
                ],
            ),
            sources=[
                FlowSource(
                    uri=Flow.Key(
                        pb_id=PB_ID, kind="plasma", name="visibilities"
                    ),
                    function="ska-sdp-realtime-receive-processors:mswriter",
                )
            ],
            data_model="Visibility",
        )
        txn.flow.create(flow)
        assert (
            txn.flow.path(flow.key)
            == f"/flow/{PB_ID}:data-product:visibilities"
        )

        result = txn.flow.get(flow.key)
        assert result == flow
        assert isinstance(result.sink, DataProduct)
        assert str(result.sink.data_dir) == f"/data/product/{PB_ID}/sdp/vis"
        assert str(result.sink.paths[0]) == "out-{scan_id}-id.ms"


def test_create_dataqueue_flow(cfg: Config):
    """Test DataQueue flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                kind="data-queue",
                name="apparant-pointing",
            ),
            sink=DataQueue(
                topics=[(0, f"{PB_ID}.pointing.apparent")],
                host="kafka://ska-sdp-qa-kafka.dpshared.svc.cluster.local",
                format="npy",
            ),
            sources=[
                FlowSource(
                    uri=TangoAttributeUrl(
                        "tango://mid-dish001/telstate/0/pointing"
                    ),
                    function="ska-sp-lmc-queue-connector"
                    ":TangoSubscriptionSource",
                ),
                FlowSource(
                    uri=Url("tango://mid-dish002/telstate/0/pointing"),
                    function="ska-sp-lmc-queue-connector"
                    ":TangoSubscriptionSource",
                ),
            ],
            data_model="float[3]",
        )
        txn.flow.create(flow)
        assert (
            txn.flow.path(flow.key)
            == f"/flow/{PB_ID}:data-queue:apparant-pointing"
        )
        assert txn.flow.get(flow.key) == flow

    assert isinstance(flow.sink, DataQueue)
    assert flow.sink.host.bootstrap_address == (
        "ska-sdp-qa-kafka.dpshared.svc.cluster.local:9092"
    )


def test_create_display_flow(cfg: Config):
    """Test Display flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                kind="display",
                name="ingest-metrics",
            ),
            sink=Display(
                widget="textual",
                endpoint="https://sdhp.stfc.skao.int/dpshared/qa/api/metrics",
            ),
            sources=[
                FlowSource(
                    uri=Flow.Key(
                        pb_id=PB_ID, kind="data-queue", name="ingest-metrics"
                    ),
                    function="qa-display:display",
                )
            ],
            data_model="Metrics",
        )
        txn.flow.create(flow)
        assert (
            txn.flow.path(flow.key) == f"/flow/{PB_ID}:display:ingest-metrics"
        )
        assert txn.flow.get(flow.key) == flow


def test_create_tango_attribute_flow(cfg: Config):
    """Test Tango flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                kind="tango",
                name="ingest-metrics",
            ),
            sink=TangoAttribute(
                attribute_url="tango://mid-sdp/subarray/01/ingest_metrics",
                dtype="DevDouble",
                max_dim_x=0,
                max_dim_y=0,
                default_value=0,
            ),
            sources=[
                FlowSource(
                    uri=Flow.Key(
                        pb_id=PB_ID, kind="data-queue", name="ingest-metrics"
                    ),
                    function="ska-sdp-lmc-queue-connector:KafkaConsumerSource",
                ),
            ],
            data_model="Metrics",
        )
        txn.flow.create(flow)
        assert txn.flow.path(flow.key) == f"/flow/{PB_ID}:tango:ingest-metrics"

        result = txn.flow.get(flow.key)
        assert result == flow

        assert isinstance(result.sink, TangoAttribute)
        assert result.sink.attribute_url.device_url == AnyUrl(
            "tango://mid-sdp/subarray/01"
        )
        assert result.sink.attribute_url.device_name == "mid-sdp/subarray/01"
        assert result.sink.attribute_url.attribute_name == "ingest_metrics"


def test_create_tango_attribute_map_flow(cfg: Config):
    """Test Tango Attribute Map flow model."""
    for txn in cfg.txn():
        flow = Flow(
            key=Flow.Key(
                pb_id=PB_ID,
                name="ingest-metrics",
            ),
            sink=TangoAttributeMap(
                attributes=[
                    (
                        TangoAttribute(
                            attribute_url=(
                                "tango://mid-sdp/subarray/01/receiver_state"
                            ),
                            dtype="DevString",
                            max_dim_x=0,
                            max_dim_y=0,
                            default_value="unknown",
                        ),
                        TangoAttributeMap.DataQuery(
                            when="type=='visibility_receive'", select="state"
                        ),
                    ),
                    (
                        TangoAttribute(
                            attribute_url=(
                                "tango://mid-sdp/subarray/01/last_update"
                            ),
                            dtype="DevFloat",
                            max_dim_x=0,
                            max_dim_y=0,
                            default_value=0.0,
                        ),
                        TangoAttributeMap.DataQuery(
                            when="type=='visibility_receive'", select="time"
                        ),
                    ),
                    (
                        TangoAttribute(
                            attribute_url=(
                                "tango://mid-sdp/subarray/01/pointing_offset"
                            ),
                            dtype="DevFloat",
                            max_dim_x=3,
                            max_dim_y=0,
                            default_value="NaN",
                        ),
                        TangoAttributeMap.DataQuery(
                            select="[?[0]=='ska001'][[1], [2], [4]][]",
                        ),
                    ),
                ]
            ),
            sources=[
                FlowSource(
                    kind="flow",
                    uri=Flow.Key(
                        pb_id=PB_ID, kind="data-queue", name="ingest-metrics"
                    ),
                    function="ska-sdp-lmc-queue-connector:exchange",
                ),
            ],
            data_model="Metrics",
        )
        txn.flow.create(flow)
        assert (
            txn.flow.path(flow.key) == f"/flow/{PB_ID}:tangomap:ingest-metrics"
        )

        result = txn.flow.get(flow.key)
        assert result == flow

        assert isinstance(result.sink, TangoAttributeMap)
        assert len(result.sink.attributes) == 3


def test_tango_attribute_map_unique_validation():
    """Test that TangoAttributeMap validation requires attribute_url to
    be unique."""
    with pytest.raises(ValidationError, match="attribute_urls not unique"):
        TangoAttributeMap(
            attributes=[
                (
                    TangoAttribute(
                        attribute_url=("tango://mid-sdp/subarray/01/metrics"),
                        dtype="DevString",
                        max_dim_x=0,
                        max_dim_y=0,
                        default_value="unknown",
                    ),
                    TangoAttributeMap.DataQuery(
                        when="type=='visibility_receive'", select="state"
                    ),
                ),
                (
                    TangoAttribute(
                        attribute_url=("tango://mid-sdp/subarray/01/metrics"),
                        dtype="DevFloat",
                        max_dim_x=0,
                        max_dim_y=0,
                        default_value=0.0,
                    ),
                    TangoAttributeMap.DataQuery(
                        when="type=='visibility_receive'", select="time"
                    ),
                ),
            ]
        )


@pytest.mark.parametrize("use_key_kind", [True, False])
def test_optional_flow_kind(cfg: Config, use_key_kind: bool):
    """Test optional kind value on Flow.Key."""
    sink = TangoAttribute(
        attribute_url="tango://mid-sdp/subarray/01/ingest_metrics",
        dtype="DevDouble",
        max_dim_x=0,
        max_dim_y=0,
        default_value=0,
    )

    flow = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind=sink.kind if use_key_kind else None,
            name="ingest-metrics",
        ),
        sink=sink,
        sources=[
            FlowSource(
                uri=Flow.Key(
                    pb_id=PB_ID, kind="data-queue", name="ingest-metrics"
                ),
                function="ska-sdp-lmc-queue-connector:KafkaConsumerSource",
            )
        ],
        data_model="Metrics",
    )
    for txn in cfg.txn():
        txn.flow.create(flow)
        assert txn.flow.path(flow.key) == f"/flow/{PB_ID}:tango:ingest-metrics"
        assert txn.flow.get(flow.key) == flow


def test_flow_state(cfg: Config):
    """Test optional kind value on Flow.Key."""
    sink = TangoAttribute(
        attribute_url="tango://mid-sdp/subarray/01/ingest_metrics",
        dtype="DevDouble",
        max_dim_x=0,
        max_dim_y=0,
        default_value=0,
    )

    flow = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            name="ingest-metrics",
        ),
        sink=sink,
        sources=[
            FlowSource(
                uri=Flow.Key(
                    pb_id=PB_ID, kind="data-queue", name="ingest-metrics"
                ),
                function="ska-sdp-lmc-queue-connector:KafkaConsumerSource",
            )
        ],
        data_model="Metrics",
    )

    for txn in cfg.txn():
        txn.flow.create(flow)
        txn.flow.delete(flow)

    for txn in cfg.txn():
        txn.flow.state(flow).create({"test": "value"})

    for txn in cfg.txn():
        assert txn.flow.state(flow).get() == {"test": "value"}


def test_flow_connections(cfg: Config):
    """
    This test creates six data flow objects:
    1. data-queue for apparent pointing with an external tango source
    2. data-queue for commanded pointing with an external tango source
    3. spead-stream for with no source
    4. plasma to store data from spead with spead-stream (#3)  as source
    5. data-product for visibilities with plasma (#4) as source
    6. data-product for pointing with the two data-queues (#1, #2) as source

    Then checks that the sources have been correctly set up.
    I.e. they point to the correct flows in the db.
    """
    # apparent pointing
    apt_pointing = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="data-queue",
            name="apparent-pointing",
        ),
        sink=DataQueue(
            topics=[(0, f"{PB_ID}.pointing.apparent")],
            host="kafka://ska-sdp-qa-kafka.dpshared.svc.cluster.local:9092",
            format="npy",
        ),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl(
                    "tango://mid-dish001/telstate/0/achievedPointing"
                ),
                function="ska-sdp-lmc-queue-connector:TangoSubscriptionSource",
            ),
            FlowSource(
                uri=TangoAttributeUrl(
                    "tango://mid-dish002/telstate/0/achievedPointing"
                ),
                function="ska-sdp-lmc-queue-connector:TangoSubscriptionSource",
            ),
        ],
        data_model="float[3]",
    )

    # commanded pointing
    comm_pointing = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="data-queue",
            name="commanded-pointing",
        ),
        sink=DataQueue(
            topics=[(0, f"{PB_ID}.pointing.commanded")],
            host="ska-sdp-qa-kafka.dpshared.svc.cluster.local:9092",
            format="npy",
        ),
        sources=[
            FlowSource(
                uri=TangoAttributeUrl(
                    "tango://mid-dish001/telstate/0/desiredPointing"
                ),
                function="ska-sdp-lmc-queue-connector:TangoSubscriptionSource",
            ),
            FlowSource(
                uri=TangoAttributeUrl(
                    "tango://mid-dish002/telstate/0/desiredPointing"
                ),
                function="ska-sdp-lmc-queue-connector:TangoSubscriptionSource",
            ),
        ],
        data_model="float[3]",
    )

    # spead stream
    spead_data = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="spead",
            name="spead-data",
        ),
        sink=SpeadStream(
            channel_map={
                "science": {
                    "vis0": ChannelAddressMapping(
                        {
                            0: (0, 8000, 1),
                        }
                    )
                }
            },
            receiver_version="5.2.2",
        ),
        sources=[],
        data_model="float",
    )

    # visibilities in shared memory
    raw_vis_data = Flow(
        key=Flow.Key(pb_id=PB_ID, kind="sharedmem", name="visibility-ingest"),
        sink=SharedMem(
            impl="plasma",
            host_path="/tmp/plasma/socket",
        ),
        sources=[
            FlowSource(
                uri=spead_data.key,
                function="ska-sdp-realtime-receive-modules:receiver",
            )
        ],
        data_model="Visibility",
    )

    # rfi masked visibilities in shared memory
    rfi_masked_vis_data = Flow(
        key=Flow.Key(
            pb_id=PB_ID, kind="sharedmem", name="visibility-rfi-mask"
        ),
        sink=SharedMem(
            impl="plasma",
            host_path="/tmp/plasma/socket",
        ),
        sources=[
            FlowSource(
                uri=spead_data.key,
                function=(
                    "ska-sdp-realtime-receive-integration:rfi_mask_processor"
                ),
            )
        ],
        data_model="Visibility",
    )

    # visibility data product
    vis_data = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="data-product",
            name="visibility-data",
        ),
        sink=DataProduct(
            data_dir=PVCPath(
                k8s_namespaces=["dp-shared", "dp-shared-p"],
                k8s_pvc_name="shared-storage",
                pvc_mount_path=Path("/data"),
                pvc_subpath=Path(f"product/{PB_ID}/sdp/vis"),
            ),
            paths=[
                Path("out-{scan_id}-id.ms"),
            ],
        ),
        sources=[
            FlowSource(
                uri=rfi_masked_vis_data.key,
                function=(
                    "ska-sdp-realtime-receive-processors:mswriter_processor"
                ),
            ),
        ],
        data_model="Visibility",
    )

    # pointing data
    pointing_data = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="data-product",
            name="pointing-data",
        ),
        sink=DataProduct(
            data_dir=PVCPath(
                k8s_namespaces=["dp-shared", "dp-shared-p"],
                k8s_pvc_name="shared-storage",
                pvc_mount_path=Path("/data"),
                pvc_subpath=Path(f"product/{PB_ID}/sdp/vis"),
            ),
            paths=[
                Path("out-{scan_id}-id.ms"),
            ],
        ),
        sources=[
            FlowSource(
                uri=apt_pointing.key,
                function="ska-sdp-realtime-receive-processors:mswriter",
            ),
            FlowSource(
                uri=comm_pointing.key,
                function="ska-sdp-realtime-receive-processors:mswriter",
            ),
        ],
        data_model="PointingTable",
    )

    # connect and write
    flows = [
        apt_pointing,
        comm_pointing,
        spead_data,
        raw_vis_data,
        rfi_masked_vis_data,
        vis_data,
        pointing_data,
    ]
    for txn in cfg.txn():
        for flow in flows:
            txn.flow.create(flow)

    for txn in cfg.txn():
        vis = txn.flow.get(vis_data.key)
        plasma_data = txn.flow.get(vis.sources[0].uri)
        point = txn.flow.get(pointing_data.key)
        point_sources = [source.uri for source in point.sources]

    for txn in cfg.txn():
        assert vis.sources[0].uri == rfi_masked_vis_data.key
        assert plasma_data.sources[0].uri == spead_data.key
        assert apt_pointing.key in point_sources
        assert comm_pointing.key in point_sources


@pytest.mark.parametrize(
    "input_uri, expected_type",
    [
        (
            Flow.Key(pb_id=PB_ID, kind="sharedmem", name="visibility-ingest"),
            Flow.Key,
        ),
        (
            TangoAttributeUrl("tango://my-db:111/my-device/1/attribute"),
            TangoAttributeUrl,
        ),
        (Url("tango://my-db:111/my-device/3/attribute"), TangoAttributeUrl),
        ("tango://my-db:111/my-device/3/attribute", TangoAttributeUrl),
        ("https://this-is.my/url", AnyUrl),
        (Url("https://this-is.my/url"), AnyUrl),
        (TangoAttributeUrl("https://this-is.my/url"), AnyUrl),
    ],
)
def test_flow_source_uri(input_uri, expected_type, cfg):
    """
    Test that FlowSource.uri type is as expected from data
    """
    data_to_db = Flow(
        key=Flow.Key(
            pb_id=PB_ID,
            kind="display",
            name="pointing-data",
        ),
        sink=Display(
            widget="textual",
            endpoint="https://sdhp.stfc.skao.int/dpshared/qa/api/metrics",
        ),
        sources=[
            FlowSource(
                uri=input_uri,
                function="ska-sdp-realtime-receive-processors:mswriter",
            )
        ],
        data_model="PointingTable",
    )

    for txn in cfg.txn():
        txn.flow.create(data_to_db)

    for txn in cfg.txn():
        result = txn.flow.get(data_to_db.key)
        assert isinstance(result.sources[0].uri, expected_type)
