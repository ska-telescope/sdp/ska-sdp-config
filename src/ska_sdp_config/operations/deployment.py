"""Deployment management for the SDP Configuration Database."""

from ..entity import Deployment as DeploymentModel
from .entity_operations import CollectiveEntityOperations


# pylint: disable=too-few-public-methods
class DeploymentOperations(CollectiveEntityOperations):
    """Database operations related to deployment management."""

    PREFIX = "/deploy"
    MODEL_CLASS = DeploymentModel
    HAS_STATE = True
