"""High-level API tests on script."""

import pytest

import ska_sdp_config
from ska_sdp_config.entity import Script

# pylint: disable=missing-docstring,redefined-outer-name

SCRIPT_IMAGE = "nexus/script-test"


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_script"


def test_script_list_all(cfg):
    """Test list all scripts."""

    script_kind = "realtime"
    script_name = "test_realtime"
    script_version = "0.0.1"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script_sdp_version = "==0.1.0"
    script_key = Script.Key(
        kind=script_kind, name=script_name, version=script_version
    )
    script = Script(
        key=script_key, **script_image, sdp_version=script_sdp_version
    )

    # Check the script list is empty
    for txn in cfg.txn():
        scripts = [
            tuple(key.model_dump().values()) for key in txn.script.query_keys()
        ]
        assert scripts == []

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # List all the scripts
    for txn in cfg.txn():
        script_keys = [
            tuple(key.model_dump().values()) for key in txn.script.query_keys()
        ]
        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version

        created_script = txn.script.get(script)
        assert created_script.image == "nexus/script-test:0.0.1"
        assert created_script.sdp_version == "==0.1.0"


def test_script_list_kind(cfg):
    """Test scripts with specific kind."""

    script_kind = "batch"
    script_name = "test_batch"
    script_version = "0.0.1"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script_sdp_version = "==0.1.0"
    script_key = Script.Key(
        kind=script_kind, name=script_name, version=script_version
    )
    script = Script(
        key=script_key, **script_image, sdp_version=script_sdp_version
    )

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # List scripts with specified kind
    for txn in cfg.txn():
        script_keys = [
            tuple(key.model_dump().values())
            for key in txn.script.query_keys(kind=script_kind)
        ]

        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version


def test_script_list_kind_name(cfg):
    """Test scripts with specific kind and name."""

    script_kind = "batch"
    script_name = "test_vis_receive"
    script_version = "0.0.1"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script_sdp_version = "==0.1.0"
    script_key = Script.Key(
        kind=script_kind,
        name=script_name,
        version=script_version,
    )
    script = Script(
        key=script_key, **script_image, sdp_version=script_sdp_version
    )

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # List scripts of specified kind and name
    for txn in cfg.txn():
        script_keys = [
            tuple(key.model_dump().values())
            for key in txn.script.query_keys(
                kind=script_kind, name=script_name
            )
        ]
        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version


def test_script_create_update(cfg):
    """Test create and update script."""

    script_kind = "batch"
    script_name = "test_batch"
    script_version = "0.6.0"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script_image_2 = {
        "image": "nexus/script-test-realtime:0.6.0",
    }
    script_sdp_version = ">=0.21.0"

    script_key = Script.Key(
        kind=script_kind, name=script_name, version=script_version
    )
    script = Script(
        key=script_key, **script_image, sdp_version=script_sdp_version
    )

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # Read script and check it is equal to script
    for txn in cfg.txn():
        read_script = txn.script.get(script_key)
        assert read_script == script

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            script = Script(
                key=script_key, **script_image, sdp_version=script_sdp_version
            )
            txn.script.create(script)

    # Update script to script2 and sdp_version to None
    for txn in cfg.txn():
        script = Script(
            key=script_key,
            **script_image_2,
            sdp_version=None,
            parameters={"test": "test"},
        )
        txn.script.update(script)

    # Read script and check it is equal to script2
    for txn in cfg.txn():
        read_script = txn.script.get(script_key)
        assert read_script == script
        assert read_script.parameters == {"test": "test"}


def test_script_create_no_sdp_version(cfg):
    """Test creating a script without optional parameter sdp_version."""

    script_kind = "batch"
    script_name = "test_batch"
    script_version = "0.6.0"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}

    script_key = Script.Key(
        kind=script_kind, name=script_name, version=script_version
    )
    script = Script(key=script_key, **script_image)

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # Read script and check it is equal to script
    for txn in cfg.txn():
        read_script = txn.script.get(script_key)
        assert read_script == script


def test_delete_script(cfg):
    """Test deleting script."""

    script_kind = "realtime"
    script_name = "test_cbf_recv"
    script_version = "0.1.0"
    script_image = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script_sdp_version = "==0.1.0"
    script_key = Script.Key(
        kind=script_kind, name=script_name, version=script_version
    )
    script = Script(
        key=script_key, **script_image, sdp_version=script_sdp_version
    )

    # Create script
    for txn in cfg.txn():
        txn.script.create(script)

    # Read script and check it is equal to script
    for txn in cfg.txn():
        read_key = txn.script.get(script_key)
        assert read_key == script

    # Delete script
    for txn in cfg.txn():
        txn.script.delete(script_key)

    # Read script and check it is equal to None
    for txn in cfg.txn():
        read_script = txn.script.get(script_key)
        assert read_script is None


if __name__ == "__main__":
    pytest.main()
