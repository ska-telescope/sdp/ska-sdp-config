"""
SDP Component management for the SDP Configuration Database.

Note that the current contents of the SDP Configuration Database
usually contain nothing under ``/component/<name>``
but an ownership record under ``/component/<name>/owner``.
It is foreseen that this will not be the case anymore in future versions
of the SDP Configuration Database, and therefore we model components
as an entity type with multiple entities.
"""

from .entity_operations import CollectiveEntityOperations, OwnershipOperations


class ComponentOperations(CollectiveEntityOperations):
    """
    Database operations related to SDP components.
    """

    PREFIX = "/component"
    HAS_OWNER = True
    KEY_PARTS = {"component_name": "[a-zA-Z0-9_-]+"}

    # These two are implemented only to support the old Transaction
    # create_is_alive() and is_alive() methods. Otherwise they seem like an
    # oddity, and probably need to go away.

    def arbitrary_create_is_alive(self, key: str) -> str:
        """Take onwnership of an arbitrary key under the components path."""
        path = f"{self.PREFIX}/{key}"
        OwnershipOperations(self._txn, path).take()
        return path

    def arbitrary_check_is_alive(self, key: str) -> bool:
        """Check if the given arbitrary key exists under the components path"""
        path = f"{self.PREFIX}/{key}"
        return OwnershipOperations(self._txn, path).is_owned()
