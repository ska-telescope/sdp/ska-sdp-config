Changelog
=========

1.0.1
-----

- Increase timeouts to reduce likelihood of connection drops due to missing progress notifications
  (`MR151 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/151>`__)


1.0.0
-----

- SpeadStream flow updated to use network addresses per beam and scan type
  (`MR139 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/139>`__)
- Added ``AbsolutePurePath`` and ``RelativePurePath`` with correct validation for entity models
  (`MR148 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/148>`__)
- Dependencies are updated - see pyproject.toml in the linked MR
  (`MR147 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/147>`__)
- CHANGELOG and README are converted from md to rst
  (`MR147 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/147>`__)
- Moved schema doc page to rst format so m2r2 is not needed
  (`MR146 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/146>`__)
- Updated ska-sdp CLI get command to work properly with System entity
  (`MR145 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/145>`__)
- Updated ska-sdp CLI commands to work with Flow and System entities
  (`MR143 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/143>`__)
- Added ``import system`` functionality to ska-sdp CLI
  (`MR141 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/141>`__)

0.10.3
------

- Update TangoAttributeUrl regex to allow for underscores as well as
  dashes
  (`MR140 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/140>`__)

.. _section-1:

0.10.2
------

- Bugfix: SKB-685: do not commit updates if old and new value match
  (`MR138 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/138>`__)
- SpeadStream Flow type updated with extra allowed keys
  (`MR135 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/135>`__)
- Dependency entity allows “slurm” kind
  (`MR137 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/137>`__)

.. _section-2:

0.10.1
------

- Introduce KafkaURL for Flows
  (`MR130 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/130>`__)
- ``sdp-sdp import`` imports script data from Telescope Model Data
  (`MR129 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/129>`__,
  `MR131 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/131>`__)

.. _section-3:

0.10.0
------

- Update Script model in ska-sdp-config to take parameters
  (`MR126 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/126>`__)
- Regex for ``dependency: repository`` in ``/system`` is broadened
  (`MR117 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/117>`__)
- Bugfix: allow ``ska-sdp import`` to import script definitions without
  ``sdp_version``
  (`MR117 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/117>`__,
  `MR127 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/127>`__)
- **BREAKING** Remove ``subarray`` and ``controller`` Txn methods
  (`MR117 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/117>`__)

.. _section-4:

0.9.1
-----

- Refactor ska-sdp CLI tests
  (`MR115 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/115>`__)
- Improvements to ska-sdp CLI commands, update, get, list and create
  (`MR113 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/113>`__)
- Add TangoAttributeMap Flow sink
  (`MR114 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/114>`__)

.. _section-5:

0.9.0
-----

- **BREAKING** Introduce a single FlowSource model for any data flow
  source types. Currently, allows three types: other flow object, tango,
  or URL
  (`MR111 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/111>`__)
- Added sdp_version to Script entity
  (`MR110 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/110>`__)

.. _section-6:

0.8.0
-----

- Update ska-sdp CLI to adapt to changes in pydantic models
  (`MR109 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/109>`__)
- Remove more code from ska-sdp-config
  (`MR108 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/108>`__)
- **BREAKING** Added pydantic model for execution block and introduce EB
  state - i.e. the ``ExecutionBlock`` constructor has changed its
  signature
  (`MR107 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/107>`__)
- **BREAKING** Remove ``xx_id`` computed parameters and ``to_dict``
  methods from all pydantic entities
  (`MR107 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/107>`__)

.. _section-7:

0.7.0
-----

- Added pydantic model for /system (previously /config)
  (`MR105 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/105>`__)
- **BREAKING** Removed ``encoding`` member in favor of ``format`` for
  ``Flow`` entities
  (`MR104 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/104>`__)
- Added support for a tensor type string as the ``data_model`` field of
  ``Flow`` entities
  (`MR104 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/104>`__)
- **BREAKING** Remove old etcd3 backend
  (`MR103 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/103>`__)
- Bugfix: .get() operation is now able to load data from the database
  without a “key” in the data (relevant for string-type keys of models).
  (`MR102 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/102>`__)
- **BREAKING** Removed deprecated txn methods
  (`MR101 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/101>`__)

.. _section-8:

0.6.0
-----

- Fix pb_id and dpl_id being written into the DB
  (`MR99 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/99>`__)
- Added ``Flow`` entity
  (`MR92 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/92>`__)
- Introduced sub-objects in ``Transaction`` to deal with each entity
  separately.
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/87>`__)
- Entities can be modelled using ``pydantic``. Ported existing entity
  classes to be based on ``pydantic``. Added new ``Script`` pydantic
  model.
  (`MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/88>`__)
- User-provided transaction wrappers don’t need to implement the
  ``is_alive`` and ``set_alive`` methods, which was previously an
  implicit requirement.
  (`MR95 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/95>`__)
- Deprecated all methods of the ``Transaction`` object. Use the new
  sub-objects and *their* methods instead.
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/87>`__)
- Added ability to perform operations over arbitrary DB paths using the
  new ``Transaction.arbitrary`` object.
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/87>`__)
- ``Config`` needs to be explicitly closed (or used as a context
  manager), otherwise background threads might linger.
  (`MR97 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/97>`__)
- JSON Content is stored as compact as possible into the DB.
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/87>`__)
- **BREAKING** With the move to ``pydantic``, the ``Deployment``
  constructor has changed its signature.
  (`MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/88>`__)
- **BREAKING** With the move to ``pydantic``, the ``ProcessingBlock``
  constructor has changed its signature.
  (`MR88 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/88>`__)
- **BREAKING** Removed ``dict_to_json`` from ``ska_sdp_config.config``.
  If needed (but you shouldn’t), import it from
  ``ska_sdp_config.base_transaction``.
  (`MR87 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/87>`__)
- **BREAKING** ``Config.alive_key`` has been removed.
  (`MR96 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/96>`__)
- **BREAKING** ``Transaction.create_is_alive`` and
  ``Transaction.take_processing_block`` don’t take a ``lease: Lease``
  argument.
  (`MR96 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/96>`__)
- Fix thread leak in ``etcd3`` backend.
  (`MR93 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/93>`__)

.. _section-9:

0.5.6
-----

- Correctly revoke lease when connection to DB is closed
  (`MR86 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/86>`__)

.. _section-10:

0.5.5
-----

- Fix ``ska-sdp watch`` command
  (`MR85 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/85>`__)

.. _section-11:

0.5.4
-----

- Update documentation
  (`MR80 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/80>`__)
- Remove ``mod_revision``
  (`MR79 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/79>`__)
- Fix type hinting on ``DbTransaction.list_keys``
  (`MR81 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/81>`__)
- Fix wrapping of watcher transactions when a custom wrapper is supplied
  (`MR83 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/83>`__)

.. _section-12:

0.5.3
-----

Bugfix in delete functionality
(`MR78 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/78>`__)

.. _section-13:

0.5.2
-----

Updated to use the ``python-etcd3`` and ``etcd3-py`` url links from from
SKAO external group repositories. ``etcd3-py`` is pinned to ``v0.12.0``

.. _section-14:

0.5.1
-----

Fixed the delete method to resolve the indentation issue when the
``must_exist`` flag is set to True. Also fixed the lease issue where it
was getting expired and not able to keep the lease alive.

.. _section-15:

0.5.0
-----

Refactored the backend to use a new client ``python-etcd3``. There were
several issues with the previous client (``etcd3-py``). The new backend
implementation has been integrated with the existing configuration
library and have significant changes to some of the underlying designs,
particularly the watcher component.

.. _section-16:

0.4.5
-----

- ska-sdp CLI import function triggers an error when import fails, which
  is now propagated to the failure code using this functionality (allows
  for quick failure)
  (`MR40 <https://gitlab.com/ska-telescope/sdp/ska-sdp-config/-/merge_requests/40>`__)

.. _section-17:

0.3.4
-----

- Add methods for getting, creating and updating deployment state.

.. _section-18:

0.3.3
-----

- Update CLI to create realtime PBs and end SBIs.

.. _section-19:

0.3.2
-----

- Allow CLI to update and edit LMC device entries.
- Improve behaviour of CLI on Windows.

.. _section-20:

0.3.1
-----

- Bugfix in ``ska-sdp`` CLI: use correct Config DB prefix for SBIs:
  ``sb`` instead of ``sbi``.
- Publish package in new central artefact repository.

.. _section-21:

0.3.0
-----

- New CLI, called ``ska-sdp`` added and ``sdpcfg`` removed.
- Config.txn() can now create/delete/list workflow definitions from the
  Config DB.
