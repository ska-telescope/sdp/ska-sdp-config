"""
List keys (and optionally values) within the Configuration Database.

Usage:
    ska-sdp list (-a |--all) [options]
    ska-sdp list [options] pb [<date>]
    ska-sdp list [options] flow [<pb-id>]
    ska-sdp list [options] script [<kind>]
    ska-sdp list [options] (deployment|eb|controller|subarray)
    ska-sdp list (-h|--help)

Arguments:
    <date>      Date on which the processing block(s) were created. Expected format: YYYYMMDD
                If not provided, all pbs are listed.
    <pb-id>     Processing block id to list flows for. If not provided, all flows are listed
    <kind>      Kind of processing script definition. Batch or realtime.
                If not provided, all scripts are listed.

Options:
    -h, --help         Show this screen
    -q, --quiet        Cut back on unnecessary output
    -a, --all          List the contents of the Config DB, regardless of object type
    -v, --values       List all the values belonging to a key in the config db
    --prefix=<prefix>  Path prefix (in addition to standard Config paths, e.g. /test for testing)
"""  # noqa: E501 # pylint: disable=line-too-long

import json
import logging

from docopt import docopt

from ska_sdp_config.config import Transaction

LOG = logging.getLogger("ska-sdp")


def _get_data_from_db(txn: Transaction, path: str):
    """Get all key-value pairs from Config DB path."""
    keys = txn.raw.list_keys(path, recurse=8)
    values_dict = {key: txn.raw.get(key) for key in keys}

    return values_dict


def _log_results(
    key: str, value: str, key_match: str, list_values=True, quiet_logging=False
):
    """Log information based on user input arguments."""

    if key_match in key:
        value = json.loads(value)
        value_formatted = json.dumps(
            value,
            indent=4,
            ensure_ascii=False,
        )
        if quiet_logging:
            if list_values:
                LOG.info(value_formatted)
            else:
                LOG.info(key)
        else:
            if list_values:
                LOG.info("%s = %s", key, value_formatted)
            else:
                LOG.info(key)


def cmd_list(txn, path, args):
    """
    List raw keys/values from database and log them into the console.

    :param txn: Config object transaction
    :param path: path within the config db to list contents of
    :param args: CLI input args
    """
    values_dict = _get_data_from_db(txn, path)
    quiet = args["--quiet"]
    list_values = args["--values"]

    if args["pb"] and args["<date>"]:
        key_match = args["<date>"]
        log_message = f'Processing blocks for date {args["<date>"]}: '
    elif args["flow"] and args["<pb-id>"]:
        key_match = args["<pb-id>"]
        log_message = f'Flows for processing block {args["<pb-id>"]}: '
    elif args["script"] and args["<kind>"]:
        key_match = args["<kind>"].lower()
        log_message = f'Script definitions of kind {args["<kind>"]}: '
    else:
        key_match = ""
        log_message = f"Keys with prefix {path}: "

    if not quiet:
        LOG.info(log_message)

    for key, value in values_dict.items():
        _log_results(
            key, value, key_match, list_values=list_values, quiet_logging=quiet
        )


def main(argv, config):
    """Run ska-sdp list."""
    args = docopt(__doc__, argv=argv)

    object_dict = {
        "pb": args["pb"],
        "script": args["script"],
        "deploy": args["deployment"],
        "eb": args["eb"],
        "flow": args["flow"],
        "component/lmc-controller": args["controller"],
        "component/lmc-subarray": args["subarray"],
    }

    if args["--prefix"]:
        path = args["--prefix"].rstrip("/") + "/"
    else:
        path = "/"

    for sdp_object, exists in object_dict.items():
        if exists:
            path = path + sdp_object
            break  # only one can be true, or none

    for txn in config.txn():
        cmd_list(txn, path, args)
