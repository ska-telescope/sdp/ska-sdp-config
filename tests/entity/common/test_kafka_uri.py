import pytest
from pydantic import KafkaDsn, TypeAdapter
from pydantic_core import Url

from ska_sdp_config.entity.common.kafka_url import KafkaUrl

JSON_SCHEMA = {
    "format": "uri",
    "minLength": 1,
    "type": "string",
}


def test_kafka_json_schema():
    assert TypeAdapter(KafkaDsn).json_schema() == JSON_SCHEMA
    assert TypeAdapter(KafkaUrl).json_schema() == JSON_SCHEMA


@pytest.mark.parametrize(
    "value, expected_boostrap_address",
    [
        # no scheme
        ("localhost", "localhost:9092"),
        ("localhost:9093", "localhost:9093"),
        ("[::1]", "[::1]:9092"),
        ("[::1]:9093", "[::1]:9093"),
        # dns url
        ("kafka://localhost", "localhost:9092"),
        ("kafka://localhost:9093", "localhost:9093"),
        (Url("kafka://localhost"), "localhost:9092"),
        (Url("kafka://localhost:9093"), "localhost:9093"),
        (KafkaUrl("kafka://localhost"), "localhost:9092"),
        (KafkaUrl("kafka://localhost:9093"), "localhost:9093"),
        # ipv4 url
        ("kafka://127.0.0.1", "127.0.0.1:9092"),
        ("kafka://127.0.0.1:9093", "127.0.0.1:9093"),
        (Url("kafka://127.0.0.1"), "127.0.0.1:9092"),
        (Url("kafka://127.0.0.1:9093"), "127.0.0.1:9093"),
        (KafkaUrl("kafka://127.0.0.1"), "127.0.0.1:9092"),
        (KafkaUrl("kafka://127.0.0.1:9093"), "127.0.0.1:9093"),
        # ipv6 url
        ("kafka://[::1]", "[::1]:9092"),
        ("kafka://[::1]:9093", "[::1]:9093"),
        (Url("kafka://[::1]"), "[::1]:9092"),
        (Url("kafka://[::1]:9093"), "[::1]:9093"),
        (KafkaUrl("kafka://[::1]"), "[::1]:9092"),
        (KafkaUrl("kafka://[::1]:9093"), "[::1]:9093"),
    ],
)
def test_kafka_url(value, expected_boostrap_address):
    u = TypeAdapter(KafkaUrl).validate_python(value)
    assert u.scheme == "kafka"
    assert u.bootstrap_address == expected_boostrap_address
    assert str(u) == "kafka://" + expected_boostrap_address
