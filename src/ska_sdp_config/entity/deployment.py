"""Deployment configuration entities."""

from typing import Annotated, Literal

from pydantic import Field

from .base import MultiEntityBaseModel


class Deployment(MultiEntityBaseModel):
    """
    Deployment entity.

    Collects configuration information relating to a cluster
    configuration change.
    """

    # Permit identifiers up to 96 bytes in length
    key: Annotated[str, Field(pattern="^[A-Za-z0-9\\-]{1,96}$")]

    kind: Literal["helm", "slurm"]
    """The kind of deployment, currently "helm" and "slurm" are supported."""

    args: dict
    """
    A dictionary of values used to customise the deployment. In the case of
    helm deployments, these are Helm values. For slurm deployment these
    are used to construct a Slurm script.
    """
