"""High-level API tests on subarrays."""

import pytest

import ska_sdp_config


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_subarray"


def test_subarray_list(cfg):
    """Multiple Subarrays are created correctly"""
    subarray = "lmc-subarray"
    subarray1 = f"{subarray}-01"
    subarray2 = f"{subarray}-02"

    # Check subarray list is empty
    for txn in cfg.txn():
        subarrays = [
            comp for comp in txn.component.list_keys() if subarray in comp
        ]
        assert subarrays == []

    # Create first subarray
    for txn in cfg.txn():
        txn.component(subarray1).create({})

    # Check subarray list has entry
    for txn in cfg.txn():
        subarrays = [
            comp for comp in txn.component.list_keys() if subarray in comp
        ]
        assert subarrays == [subarray1]

    # Create second subarray
    for txn in cfg.txn():
        txn.component(subarray2).create({})

    # Check subarray list has both entries
    for txn in cfg.txn():
        subarrays = [
            comp for comp in txn.component.list_keys() if subarray in comp
        ]
        assert subarrays == sorted([subarray1, subarray2])


def test_subarray_create_update(cfg):
    """Subarray updated correctly"""
    subarray = "lmc-subarray-03"
    state1 = {
        "eb_id": "eb-test-20200727-00000",
        "state": "ON",
        "obs_state": "READY",
        "scan_type": "science",
        "scan_id": None,
    }
    state2 = {
        "eb_id": "eb-test-20200727-00000",
        "state": "ON",
        "obs_state": "SCANNING",
        "scan_type": "science",
        "scan_id": 1,
    }

    # Subarray has not been created, so should return None
    for txn in cfg.txn():
        state = txn.component(subarray).get()
        assert state is None

    # Create subarray as state1
    for txn in cfg.txn():
        txn.component(subarray).create(state1)

    # Read subarray and check it is equal to state1
    for txn in cfg.txn():
        state = txn.component(subarray).get()
        assert state == state1

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.component(subarray).create(state1)

    # Update subarray to state2
    for txn in cfg.txn():
        txn.component(subarray).update(state2)

    # Read subarray and check it is equal to state2
    for txn in cfg.txn():
        state = txn.component(subarray).get()
        assert state == state2
