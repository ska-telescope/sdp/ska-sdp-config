# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "src")))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Configuration Library"
copyright = "2019-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.1"
release = "1.0.1"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.viewcode",
]

exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"


# -- Extension configuration -------------------------------------------------

# Intersphinx configuration
intersphinx_mapping = {
    "python-etcd3": ("https://python-etcd3.readthedocs.io/en/latest/", None),
    "ska-sdp-integration": (
        "https://developer.skao.int/projects/ska-sdp-integration/en/latest/",
        None,
    ),
    "ska-sdp-lmc": ("https://developer.skao.int/projects/ska-sdp-lmc/en/latest/", None),
    "ska-telmodel": (
        "https://developer.skao.int/projects/ska-telmodel/en/latest/",
        None,
    ),
}
