include .make/base.mk
include .make/python.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-config
PROJECT_PATH = ska-telescope/sdp/ska-sdp-config
ARTEFACT_TYPE = python
CHANGELOG_FILE = CHANGELOG.rst

PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503

docs-pre-build:
	poetry config virtualenvs.in-project true
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs
	sphinx-lint $(DOCS_SOURCEDIR)
