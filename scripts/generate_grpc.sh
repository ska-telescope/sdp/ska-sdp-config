#!/bin/bash

# Bash script for (re-)generating GRPC definitions. This does the
# following:
#
# 1. Collect relevant protobuf definitions from the etcd
#    repositories and dependencies (gogoproto and google API)
# 2. Compile them using gprc_tools.protoc
# 3. Re-locate the modules into the ska_sdp_config Python module
#    to prevent them from potentially clashing
#
# Note that we currently utilise the gRPC definition that come with
# python-etcd3, so this script is here just for reference

OUT_PATH=$(pwd)/../src/ska_sdp_config/backend/grpc
OUT_MODULE=ska_sdp_config.backend.grpc
TEST_PYTHONPATH=$(pwd)/../src

# parameters: URL, path, branch, files
function get_files_from_git {
    git clone $1 $2 -b $3 -n --depth 1
    cd $2
    git checkout $3 -- $4
    cd ..
}

# Get etcd
echo "* Getting protobuf definitions..."
get_files_from_git \
    https://github.com/etcd-io/etcd \
    etcd \
    release-3.5 \
    api/*/*.proto
INC="--proto_path $(pwd)"
GEN="etcd/api/authpb/auth.proto etcd/api/mvccpb/kv.proto"

# Get gogoproto. Deprecated?
get_files_from_git \
    https://github.com/gogo/protobuf \
    gogoproto \
    v1.3.2 \
    gogoproto/*.proto
INC="${INC} --proto_path $(pwd)/gogoproto/"
GEN="${GEN} gogoproto/gogo.proto"

# Get google API. They don't have tags, so we check out
# master. Let's hope they know what they're doing.
get_files_from_git \
    https://github.com/googleapis/googleapis \
    googleapis \
    master \
    google/api/*.proto
INC="${INC} --proto_path $(pwd)/googleapis/"
GEN="${GEN} google/api/annotations.proto google/api/http.proto"

# Generate code
echo "* Generating Python code for rpc.proto and dependencies..."
cd etcd/api/etcdserverpb
mkdir -p ${OUT_PATH}

# Dependencies: Only protobuf definitions
python3 -m grpc_tools.protoc \
    ${INC} \
    --python_out ${OUT_PATH} \
    ${GEN}
# rpc.proto: Also GRPC stubs
python3 -m grpc_tools.protoc \
    ${INC} -I . \
    --python_out ${OUT_PATH} \
    --grpc_python_out ${OUT_PATH} \
    rpc.proto

# Rewrite all imports to new base
echo "* Rewriting imports to ${OUT_MODULE}..."
# parameters: path, module regexp, new module
function rewrite_from_imports {
    find $1 -name "*.py" | \
        xargs sed -i "s/^from $2\(.*\) import/from $3\1 import/"
}
rewrite_from_imports ${OUT_PATH} "etcd\\.api" "${OUT_MODULE}.etcd.api"
rewrite_from_imports ${OUT_PATH} "gogoproto" "${OUT_MODULE}.gogoproto"
rewrite_from_imports ${OUT_PATH} "google\\.api" "${OUT_MODULE}.google.api"

# Quick test
echo "* Checking that ${OUT_MODULE}.rpc_pb2.WatchRequest works..."
PYTHONPATH=${TEST_PYTHONPATH} python3 -c "import ${OUT_MODULE}.rpc_pb2; ${OUT_MODULE}.rpc_pb2.WatchRequest(); print('Success!')"
