"""High-level API tests on controller device."""

import pytest

import ska_sdp_config

# pylint: disable=missing-docstring,redefined-outer-name


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_controller"


def test_controller_create_update(cfg):
    controller = "lmc-controller"
    state1 = {"state": "OFF"}
    state2 = {"state": "ON"}

    # Controller has not been created, so should return None
    for txn in cfg.txn():
        state = txn.component(controller).get()
        assert state is None

    # Create controller as state1
    for txn in cfg.txn():
        txn.component(controller).create(state1)

    # Read controller and check it is equal to state1
    for txn in cfg.txn():
        state = txn.component(controller).get()
        assert state == state1

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.component(controller).create(state1)

    # Update controller to state2
    for txn in cfg.txn():
        txn.component(controller).update(state2)

    # Read controller and check it is equal to state2
    for txn in cfg.txn():
        state = txn.component(controller).get()
        assert state == state2
