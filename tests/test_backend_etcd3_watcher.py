"""Tests for etcd3 watcher"""

# pylint: disable=protected-access
# pylint: disable=cell-var-from-loop
# pylint: disable=unnecessary-lambda

import datetime
import functools
import logging
import time

import pytest

# Import etcd3_backend fixture
from .test_backend_etcd3 import (  # noqa: F401, pylint:disable=unused-import
    etcd3,
)

PREFIX = "/__test"

get_time = functools.partial(datetime.datetime, tzinfo=datetime.timezone.utc)
LOGGER = logging.getLogger(__name__)


def test_watcher_txn_simple(etcd3_backend):
    """Test transactions within watcher"""

    key = PREFIX + "/test_watcher_txn_simple"

    # Check that a simple transaction works
    etcd3_backend.create(key + "/test", "foo")
    for watch in etcd3_backend.watcher():
        for txn in watch.txn():
            assert txn.get(key + "/test") == "foo"
        break

    etcd3_backend.delete(key + "/test")


def test_watcher_txn_bakein(etcd3_backend):
    """Check that revision stays baked-in between transactions when
    there's no visible update."""
    key = PREFIX + "/test_watcher_txn_bakein"
    etcd3_backend.create(key + "/test", "foo")
    for watch in etcd3_backend.watcher(requery_progress=0.2):
        # This will start a watcher on "/test"
        for txn in watch.txn():
            assert txn.get(key + "/test") == "foo"
        # We will request progress, baking revision in
        for txn in watch.txn():
            # This update will therefore be missed
            etcd3_backend.update(key + "/test", "fuu")
            assert txn.get(key + "/test") == "foo"
        # The next transaction however will see it - at least if we
        # wait slightly for the watcher notification to arrive
        time.sleep(0.05)
        for txn in watch.txn():
            assert txn.get(key + "/test") == "fuu"
        break

    etcd3_backend.delete(key + "/test")


@pytest.mark.parametrize("wait_time", [0.05, 0.2])
def test_watcher_txn_bakein2(etcd3_backend, wait_time):
    """Check that revision stays baked-in between transactions when
    there's no visible update"""

    key = PREFIX + "/test_watcher_txn_bakein2"
    requery_progress = 0.1

    etcd3_backend.create(key + "/test", "foo")
    etcd3_backend.create(key + "/test2", "bar")
    for watch in etcd3_backend.watcher(requery_progress=requery_progress):
        # Changing another key will be completely missed though
        for txn in watch.txn():
            assert txn.get(key + "/test") == "foo"
        # At this point we are watching - do it again to bake in
        for txn in watch.txn():
            assert txn.get(key + "/test") == "foo"
            etcd3_backend.update(key + "/test2", "baz")
        # Now the update above will be missed if we wait less than
        # requery_progress - because that's how long the backend
        # assumes the revision from the last transaction is current in
        # absence of evidence to the contrary (i.e. a watcher
        # notification, which we won't get because we aren't watching
        # test2 yet)
        time.sleep(wait_time)
        for txn in watch.txn():
            if wait_time < requery_progress:
                assert txn.get(key + "/test2") == "bar"
            else:
                assert txn.get(key + "/test2") == "baz"
        # Now we are watching test2, so we should learn about the
        # update relatively quickly
        time.sleep(0.05)
        for txn in watch.txn():
            assert txn.get(key + "/test2") == "baz"
        break

    etcd3_backend.delete(key + "/test")
    etcd3_backend.delete(key + "/test2")


# pylint: disable=too-many-branches
@pytest.mark.timeout(2)
@pytest.mark.parametrize("restart_watch_connections", [False, True])
def test_transaction_watchers(etcd3_backend, restart_watch_connections):
    """Tests the ever-tricky cases where list and get watchers need to be
    replaced by each other."""

    key = PREFIX + "/test_transaction_watchers"
    etcd3_backend.create(key, "test")
    etcd3_backend.create(key + "/1", "test")

    # pylint: disable=W0212
    for i, watch in enumerate(etcd3_backend.watcher()):
        for txn in watch.txn():
            txn.get(key)
        # Restart connection if requested (i.e. coarsely simulate
        # connection loss)
        if restart_watch_connections:
            watch._start_watch_connection()
        if i == 0:
            # Watches start immediately now
            assert len(watch._conn.get_watchers) == 1
            assert len(watch._conn.list_watchers) == 0
            for txn in watch.txn():
                txn.list_keys(key + "/")
                txn.get(key + "/1")
                txn.get(key + "/2")
            assert len(watch._conn.get_watchers) == 1
            assert len(watch._conn.list_watchers) == 1
        if i == 1:
            # Get watcher for "key", list watcher for "key/*"
            assert len(watch._conn.get_watchers) == 1
            assert len(watch._conn.list_watchers) == 1
            for txn in watch.txn():
                txn.get(key + "/1")
                txn.get(key + "/2")
            # As we did not list keys, the corresponding watcher will
            # be cancelled, and replaced by two individual watchers
            # tracking the keys above.
        if i == 2:
            # Get watchers for "key", "key/1" and "key/2"
            assert len(watch._conn.get_watchers) == 3
            assert len(watch._conn.list_watchers) == 0
            for txn in watch.txn():
                txn.get(key + "/1")
        if i == 3:
            # Get watchers for "key" and "key/1"
            assert len(watch._conn.get_watchers) == 2
            assert len(watch._conn.list_watchers) == 0
            for txn in watch.txn():
                txn.get(key + "/1")
                txn.get(key + "/2")
                txn.list_keys(key + "/")
            assert len(watch._conn.list_watchers) == 1
        if i == 4:
            # Get watchers for "key", list watcher for "key/*"
            assert len(watch._conn.get_watchers) == 1
            assert len(watch._conn.list_watchers) == 1
            for txn in watch.txn():
                txn.list_keys(key + "/")
        if i == 5:
            # Still get watchers for "key", list watcher for "key/*"
            assert len(watch._conn.get_watchers) == 1
            assert len(watch._conn.list_watchers) == 1
        if i == 6:
            # Only watcher for "key"
            assert len(watch._conn.get_watchers) == 1
            break
        etcd3_backend.update(key, str(i))
    assert i == 6  # pylint: disable=undefined-loop-variable

    etcd3_backend.delete(key, recursive=True, must_exist=False)


@pytest.mark.timeout(2)
def test_transaction_watch_delete(etcd3_backend):
    """Testing two overlapping list watchers and delete a
    key in their range."""
    key = PREFIX + "/test_watch_delete"
    key2 = key + "_"
    etcd3_backend.create(key2, "asd")

    for i, watch in enumerate(etcd3_backend.watcher()):
        for txn in watch.txn():
            keys = txn.list_keys(key)
            keys2 = txn.list_keys(key2)
        if i == 0:
            assert keys == [key + "_"]
            assert keys2 == [key + "_"]
            etcd3_backend.delete(key2)
        else:
            assert keys == []
            assert keys2 == []
            # If we get here, it works correctly
            break

    etcd3_backend.delete(key, must_exist=False, recursive=True)


@pytest.mark.timeout(2)
@pytest.mark.parametrize("restart_watch_connections", [False, True])
def test_transaction_watch_list(etcd3_backend, restart_watch_connections):
    """Tests that list watchers trigger in the right situations."""
    key = PREFIX + "/test_transaction_watch_list"
    etcd3_backend.create(key + "/a", "test")
    etcd3_backend.create(key + "/b", "test2")
    etcd3_backend.create(key + "/c", "test3")

    # Use a timeout to make sure we don't block. A bit of an
    # inexact science.
    for i, watch in enumerate(etcd3_backend.watcher(timeout=0.25)):
        for txn in watch.txn():
            keys = txn.list_keys(key + "/")
        # Restart connection if requested (i.e. coarsely simulate
        # connection loss)
        if restart_watch_connections:
            watch._retry_loop(lambda: watch._start_watch_connection())
        if i == 0:
            assert not watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            # Deleting a key in range should cause loop
            etcd3_backend.delete(key + "/a")
        if i == 1:
            assert not watch._got_timeout
            assert keys == [key + "/b", key + "/c"]
            # Creating a key in range should cause loop
            etcd3_backend.create(key + "/a", "asd")
        if i == 2:
            assert not watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            # Updating a key should *not* cause a loop in theory
            # because the key list doesn't change, however the
            # watcher backend doesn't make this distinction.
            etcd3_backend.update(key + "/a", "asd2")
        if i == 3:
            assert not watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            # Except of course if we also read it
            for txn in watch.txn():
                txn.get(key + "/a")
            etcd3_backend.update(key + "/a", "asd3")
        if i == 4:
            assert not watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            # Similarly, updating keys at higher or lower levels
            # should be ignored
            etcd3_backend.create(key, "parent")
            etcd3_backend.create(key + "/a/b", "child")
        if i == 5:
            assert watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            # Manually trigger watch, should prevent a timeout
            watch.trigger()
        if i == 6:
            assert not watch._got_timeout
            assert keys == [key + "/a", key + "/b", key + "/c"]
            break

    etcd3_backend.delete(key, must_exist=False, recursive=True)


def test_watcher_set_wake_up_at_from_none(etcd3_backend):
    """When `wake_up_at` is set the first time on a watcher object (because in
    that case it is still None), it sets to the given 'wake_up' value
    """
    now = get_time(2022, 10, 9, 1, 12, 10)
    wake_up = get_time(2022, 10, 9, 1, 12, 11)
    for watcher in etcd3_backend.watcher():
        # set `wake_up_at` the first time
        watcher.set_wake_up_at(wake_up)
        assert watcher.get_timeout(now) == 1
        break


def test_watcher_set_wake_up_at_new_wake_up(etcd3_backend):
    """When watcher already has `wake_up_at` set, and we set a new one which is
    earlier in time than the existing one, then `wake_up_at` is reset to the
    new one."""
    now = get_time(2022, 5, 9, 1, 11, 10)
    wake_up1 = get_time(2022, 10, 9, 1, 12, 11)
    wake_up2 = get_time(2022, 5, 9, 1, 11, 11)
    for watcher in etcd3_backend.watcher():
        # set `wake_up_at` the first time
        watcher.set_wake_up_at(wake_up1)

        # reset `wake_up_at`; new value is earlier than old one
        watcher.set_wake_up_at(wake_up2)
        assert watcher.get_timeout(now) == 1
        break


def test_watcher_set_wake_up_at_new_wake_up_too_late(etcd3_backend):
    """When watcher already has `wake_up_at` set, and we set a new one which is
    later in time than the existing one, then `wake_up_at` is not reset.
    """
    now = get_time(2022, 10, 9, 1, 11, 10)
    wake_up1 = get_time(2022, 10, 9, 1, 12, 11)
    wake_up2 = get_time(2023, 5, 9, 1, 11, 11)
    for watcher in etcd3_backend.watcher():
        # set `wake_up_at` the first time
        watcher.set_wake_up_at(wake_up1)

        # reset `wake_up_at`; new value is later than old one
        watcher.set_wake_up_at(wake_up2)

        assert watcher.get_timeout(now) == 61
        break


def test_watcher_iteration_timeout(etcd3_backend):
    """
    Test that the watcher.__iter__ loop correctly
    sets and resets the timeout for the _wait_txn object,
    depending on the alarm.
    """
    for i, watcher in enumerate(etcd3_backend.watcher()):
        if i == 0:
            assert watcher.get_timeout() is None

            watcher.set_wake_up_at(datetime.datetime.now())

            assert watcher.get_timeout() == 0

        if i == 1:
            assert watcher.get_timeout() is None
            break


def test_watcher_wake_up(etcd3_backend):
    """
    Check that watcher actually wakes up at specified point in time
    """
    wake_up = datetime.datetime.now() + datetime.timedelta(seconds=0.2)
    for i, watcher in enumerate(etcd3_backend.watcher()):
        if i == 0:
            watcher.set_wake_up_at(wake_up)
        else:
            break
    assert datetime.datetime.now() >= wake_up
    assert datetime.datetime.now() - datetime.timedelta(seconds=0.1) < wake_up


def test_watcher_wake_up_2(etcd3_backend):
    """
    Same as test_watcher_wake_up, but while watching a key
    """
    wake_up = datetime.datetime.now() + datetime.timedelta(seconds=0.2)
    for i, watcher in enumerate(etcd3_backend.watcher()):
        for txn in watcher.txn():
            txn.get(PREFIX + "/test_watcher_wake_up_2")
        if i == 0:
            watcher.set_wake_up_at(wake_up)
        else:
            break
    assert datetime.datetime.now() >= wake_up
    assert datetime.datetime.now() - datetime.timedelta(seconds=0.1) < wake_up


def test_watcher_set_timeout(etcd3_backend):
    """
    Current timeout is set to 60 s;
    wake_up_at value is not set, therefore tested
    method returns the value of the current timeout.
    """
    for watcher in etcd3_backend.watcher(timeout=60):
        # check that timeout is set
        assert watcher._timeout == 60
        break


def test_watcher_timeout(etcd3_backend):
    """
    Check that timeout correctly forces a loop after given time
    """
    start = datetime.datetime.now()
    for i, _watcher in enumerate(etcd3_backend.watcher(timeout=0.1)):
        if i == 5:
            break
    assert datetime.datetime.now() >= start + datetime.timedelta(seconds=0.5)
    assert datetime.datetime.now() < start + datetime.timedelta(seconds=0.6)


def test_watcher_timeout_2(etcd3_backend):
    """
    Check that timeout correctly forces a loop after given time, when
    a key is read
    """
    expected = datetime.datetime.now()
    for i, watcher in enumerate(etcd3_backend.watcher(timeout=0.1)):
        assert datetime.datetime.now() >= expected
        assert datetime.datetime.now() < expected + datetime.timedelta(
            seconds=0.05
        )
        expected = datetime.datetime.now() + datetime.timedelta(seconds=0.1)
        for txn in watcher.txn():
            txn.get(PREFIX + "/test_watcher_timeout_2")
        if i == 5:
            break


def test_watcher_timeout_and_wake_up(etcd3_backend):
    """
    Check that timeout correctly forces a loop after given time, when
    a key is read
    """
    wake_up = datetime.datetime.now() + datetime.timedelta(seconds=0.28)
    expected = datetime.datetime.now()
    for i, watcher in enumerate(etcd3_backend.watcher()):
        assert datetime.datetime.now() >= expected
        assert datetime.datetime.now() < expected + datetime.timedelta(
            seconds=0.05
        )
        if i == 3:
            break
        # Set timeout + wake_up
        watcher.set_timeout(0.1)
        watcher.set_wake_up_at(wake_up)
        # First two iterations, timeout will wake us up. Last
        # iteration, designated wake up time.
        if i < 2:
            expected = datetime.datetime.now() + datetime.timedelta(
                seconds=0.1
            )
        else:
            expected = wake_up
