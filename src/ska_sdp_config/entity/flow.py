"""Flow entity Model and related sub-entities."""

from __future__ import annotations

from typing import Annotated, Any, Literal, Sequence, TypeVar

from pydantic import AnyUrl, Field, NonNegativeInt, computed_field

from ska_sdp_config.entity.common import (
    AnyPurePath,
    KafkaUrl,
    ProcessingBlockId,
    PVCPath,
    TangoArgType,
    TangoAttributeUrl,
    TangoDataType,
)

from .base import EntityBaseModel, EntityKeyBaseModel, MultiEntityBaseModel


class TelModel(EntityBaseModel):
    """TelModel flow entity."""

    kind: Literal["telmodel"] = "telmodel"
    pvc_data_dir: PVCPath | None
    """Optional output directory of datamodels."""
    key: str = "instrument/ska1_low/layout/low-layout.json"


ScanTypeId = Annotated[str, Field(pattern=r"^[a-z0-9-:]+$")]
"""Observation ScanType ID."""


BeamId = Annotated[str, Field(pattern=r"^[a-z0-9]+$")]
"""Observation Beam ID."""

ReceiverId = NonNegativeInt
"""SPEAD Receiver ID."""

ChannelAddressMapping = dict[
    NonNegativeInt, tuple[ReceiverId, NonNegativeInt, NonNegativeInt]
]
"""A contiguous, ordered mapping of observation channels to receivers.

`dict[start_channel, (receiver_id, port_start, port_increment))`, e.g.

>>> from pydantic import TypeAdapter
>>> from ska_sdp_config.entity.flow import ChannelAddressMapping
>>> channel_map = TypeAdapter(ChannelAddressMapping).validate_python({
...     0: (0, 8000, 1),
...     400: (1, 8000, 1),
...     800: (2, 8000, 1),
... })
"""


class SpeadStream(EntityBaseModel):
    """A SPEAD flow entity."""

    kind: Literal["spead"] = "spead"
    channel_map: dict[ScanTypeId, dict[BeamId, ChannelAddressMapping]]
    """The channel to network address mapping for scans and beams."""
    receiver_version: str
    """Receiver OCI image version."""
    streams: int = 1
    """Total number of streams per receiver."""
    transport_protocol: Literal["tcp", "udp"] = "udp"
    """The spead stream transport protocol."""
    continuous_mode: bool = False
    """
    Flag for enabling receivers to re-create the streams and resume receiving
    data after all end of streams are reached.
    """

    @computed_field
    @property
    def instances(self) -> int:
        """Number of receiver instances required for all scans."""
        return max(
            (
                len(beam)
                for scan_type in self.channel_map.values()
                for beam in scan_type.values()
            ),
            default=0,
        )


class SharedMem(EntityBaseModel):
    """Shared Memory flow entity e.g. Plasma."""

    kind: Literal["sharedmem"] = "sharedmem"
    impl: Literal["plasma"]
    """The shared memory implementation."""
    host_path: AnyPurePath
    """The Path of the shared memory socket."""


KafkaTopicName = Annotated[str, Field(pattern=r"^[A-Za-z0-9-_.]+$")]
"""A Kafka topic name. Legal characters including alphanumeric,
`-`, `_`, and `.`"""


_T = TypeVar("_T")

ChannelMap = Sequence[tuple[NonNegativeInt, _T]]
"""A contiguous, ordered mapping of channels to a value. Tuples
represent `(start_channel, value)`, e.g.

>>> from ipaddress import IPv4Address
>>> from pydantic import TypeAdapter
>>> from ska_sdp_config.entity.flow import ChannelMap
>>> channel_map = TypeAdapter(ChannelMap[IPv4Address]).validate_python([
...     (0, '192.168.0.1'),
...     (400, '192.168.0.2')
... ])
"""


class DataQueue(EntityBaseModel):
    """Data Queue flow entity e.g. Kafka topic."""

    kind: Literal["data-queue"] = "data-queue"
    topics: KafkaTopicName | ChannelMap[KafkaTopicName]
    """The topic names."""
    host: KafkaUrl
    """The host URL of the data queue service."""
    format: Literal["json", "npy", "npz", "msgpack_numpy"]
    """The data encoded format."""


class Display(EntityBaseModel):
    """Data Display flow entity e.g. QA Display."""

    kind: Literal["display"] = "display"
    widget: str
    """The type of display widget."""
    endpoint: AnyUrl
    """The display URL endpoint."""


class DataProduct(EntityBaseModel):
    """Data Product flow entity e.g. Measurement Set."""

    kind: Literal["data-product"] = "data-product"

    data_dir: Annotated[
        PVCPath | AnyPurePath, Field(union_mode="left_to_right")
    ]
    """Output directory of data products."""
    paths: list[AnyPurePath]
    """Channel mapping of output data products relative to the persistent
    volume directory."""


class TangoAttribute(EntityBaseModel):
    """Tango attribute flow entity.

    This entity instructs to flow data into a single Tango attribute.
    """

    kind: Literal["tango"] = "tango"
    attribute_url: TangoAttributeUrl
    """Fully qualified tango attribute URL, e.g.
    `"tango://mid-sdp/subarray/01/some_attribute"`."""
    dtype: TangoArgType
    """Attribute data type corresponding to a `tango.ArgType`."""
    max_dim_x: NonNegativeInt = 0
    """Size of the slowest changing dimension."""
    max_dim_y: NonNegativeInt = 0
    """Size of the fastest changing dimension."""
    default_value: TangoDataType = ""
    """Default attribute value at creation casted to the selected dtype."""


class TangoAttributeMap(EntityBaseModel):
    """Tango attribute map flow entity.

    This entity instructs to flow data into multiple Tango attributes.
    """

    class DataQuery(EntityBaseModel):
        """
        Decomposed JMESPath query for locating data within a data model.
        """

        when: str | None = None
        """JMESPath when query clause on the flow data model."""
        select: str = "@"
        """JMESPath select query clause on the flow data model."""

    kind: Literal["tangomap"] = "tangomap"
    attributes: list[tuple[TangoAttribute, DataQuery]]
    """
    Mapping of TangoAttributes with unique attribute URL to a data query.
    """

    def model_post_init(self, __context: Any):
        attribute_urls = [
            attribute[0].attribute_url for attribute in self.attributes
        ]
        if len(set(attribute_urls)) != len(attribute_urls):
            raise ValueError(
                f"attribute_urls not unique for {self.attributes}"
            )


class FlowSource(EntityBaseModel):
    """
    A generic flow source pointing to either:
    - an existing flow entry in the configuration database
    - external source, such as a tango attribute
    - an external url
    """

    uri: Annotated[
        Flow.Key | TangoAttributeUrl | AnyUrl,
        Field(union_mode="left_to_right"),
    ]
    """
    Reference to the data source:
    - Flow.Key: key of a data flow entry
    - TangoAttributeUrl: tango attribute reference
    - AnyURL: url of any other kind
    """
    function: str | None = None
    """
    Python function, class or OCI image that will flow data from
    source to sink
    """
    parameters: dict | None = None
    """Function, class or OCI image parameters"""


class Flow(MultiEntityBaseModel):
    """Flow datastream entity.

    Contains configuration information relating to streaming data connections
    related to a processing job for SDP. Flows are associated with and persist
    for the lifetime of a ProcessingBlock.
    """

    class Key(EntityKeyBaseModel):
        """An SDP Flow primary key."""

        pb_id: ProcessingBlockId
        """The ID of the Processing Block that this flow belongs to."""
        kind: Annotated[str | None, Field(pattern=r"^[A-Za-z0-9-]{1,96}$")] = (
            None
        )
        """The kind of sink this flow moves data to."""
        name: Annotated[str, Field(pattern=r"^[A-Za-z0-9-]{1,96}$")]
        """The name of this flow."""

    key: Annotated[Key, Field(exclude=True)]
    sink: Annotated[
        TelModel
        | SharedMem
        | DataQueue
        | Display
        | DataProduct
        | TangoAttribute
        | TangoAttributeMap
        | SpeadStream,
        Field(discriminator="kind"),
    ]
    """The Flow output definition for data to be streamed to."""

    sources: list[FlowSource]
    """A list containing sources of flow data."""

    data_model: Annotated[str, Field(pattern=r"^[A-Za-z0-9\,\[\]]+$")]
    """The DataModel that the flow data represents."""

    def model_post_init(self, __context: Any) -> None:
        super().model_post_init(__context)
        if self.key.kind is None:
            self.key.kind = self.sink.kind
        else:
            assert self.key.kind == self.sink.kind
