"""High-level API tests on Deployment entries."""

import pytest

import ska_sdp_config

# pylint: disable=missing-docstring,redefined-outer-name


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_deploy"


def test_deploy_create(cfg):
    deploy_id = "proc-20210824-00099-script"
    chart = {"chart": "script", "values": {}}

    deploy = ska_sdp_config.Deployment(key=deploy_id, kind="helm", args=chart)

    # Deployment has not been created, so should return None
    for txn in cfg.txn():
        state = txn.deployment.get(deploy_id)
        assert state is None

    # Create Deployment
    for txn in cfg.txn():
        txn.deployment.create(deploy)

    # Read Deployment and check it is equal to input value
    for txn in cfg.txn():
        state = txn.deployment.get(deploy_id)
        assert state == deploy

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.deployment.create(deploy)


def test_deploy_state(cfg):
    deploy_id = "proc-20210824-00099-script"

    # Deployment state has not been created, so should return None
    for txn in cfg.txn():
        state = txn.deployment.state(deploy_id).get()
        assert state is None

    # Create Deployment state
    for txn in cfg.txn():
        txn.deployment.state(deploy_id).create({"state": "RUNNING"})

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.deployment.state(deploy_id).create({"state": "RUNNING"})

    # Read Deployment state and check it is equal to input value
    for txn in cfg.txn():
        state = txn.deployment.state(deploy_id).get()
        assert state == {"state": "RUNNING"}

    # Update Deployment state
    for txn in cfg.txn():
        txn.deployment.state(deploy_id).update({"state": "FINISHED"})

    # Read Deployment state and check it is equal to updated value
    for txn in cfg.txn():
        state = txn.deployment.state(deploy_id).get()
        assert state == {"state": "FINISHED"}


def test_deploy_delete(cfg):
    deploy_id = "test-delete-deploy-000000-0000"
    chart = {"chart": "script", "values": {}}

    deploy = ska_sdp_config.Deployment(key=deploy_id, kind="helm", args=chart)

    # Create Deployment
    for txn in cfg.txn():
        txn.deployment.create(deploy)
        txn.deployment.state(deploy_id).create({"state": "FINISHED"})

    # check that deployment and its state now exist
    for txn in cfg.txn():
        deployments = txn.deployment.list_keys()
        deploy_state = txn.deployment.state(deploy_id).get()
        assert deploy_id in deployments
        assert deploy_state == {"state": "FINISHED"}

    # delete PB and its state -- tested method
    for txn in cfg.txn():
        txn.deployment.delete(deploy_id, recurse=True)

    # check that both deployment and its state are deleted
    for txn in cfg.txn():
        deployments = txn.deployment.list_keys()
        deploy_state = txn.deployment.state(deploy_id).get()
        assert deploy_id not in deployments
        assert deploy_state is None


def test_slurm_deployment(cfg):
    """slurm-type deployments are allowed"""
    dpl_settings = {
        "key": "test-deploy-slurm-000000-0000",
        "kind": "slurm",
        "args": {},
    }

    deploy = ska_sdp_config.Deployment(**dpl_settings)

    for txn in cfg.txn():
        txn.deployment.create(deploy)

    for txn in cfg.txn():
        result = txn.deployment.get(dpl_settings["key"])
        assert result.kind == "slurm"
