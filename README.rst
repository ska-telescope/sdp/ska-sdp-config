SDP Configuration Library
=========================

This is the frontend module for accessing SKA SDP configuration
information. It provides ways for SDP controller and processing
components to discover and manipulate the intended state of the system.

At the moment this is implemented on top of ``etcd``, a highly-available
database. This library provides primitives for atomic queries and
updates to the stored configuration information.

Installation
------------

Install with pip:

.. code:: bash

   pip install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple ska-sdp-config

Basic Usage
-----------

Make sure you have a database backend accessible (etcd3 is supported at
the moment). Location can be configured using the ``SDP_CONFIG_HOST``
and ``SDP_CONFIG_PORT`` environment variables. The defaults are
``127.0.0.1`` and ``2379``, which should work with a local ``etcd``
started without any configuration.

This should give you access to SDP configuration information, for
instance try:

.. code:: python

   import ska_sdp_config

   config = ska_sdp_config.Config()

   for txn in config.txn():
       for pb_id in txn.processing_block.list_keys(key_prefix=prefix):
           pb = txn.processing_block.get(pb_id)
           print("{} ({}:{})".format(pb_id, pb.script["name"], pb.script["version"]))

To read a list of currently active processing blocks with their
associated scripts.

Command line
------------

This package also comes with a command line utility for easy access to
configuration data. See documentation at: `CLI for
SDP <https://developer.skao.int/projects/ska-sdp-config/en/latest/cli.html>`__

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Contributing to this repository
-------------------------------

`Black <https://github.com/psf/black>`__,
`isort <https://pycqa.github.io/isort/>`__, and various linting tools
are used to keep the Python code in good shape. Please check that your
code follows the formatting rules before committing it to the
repository. You can apply Black and isort to the code with:

.. code:: bash

   make python-format

and you can run the linting checks locally using:

.. code:: bash

   make python-lint

The linting job in the CI pipeline does the same checks, and it will
fail if the code does not pass all of them.

Creating a new release
----------------------

When you are ready to make a new release:

- Check out the master branch
- Update the version number in ``.release`` with

  - ``make bump-patch-release``,
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- Manually update the version number in

  - ``src/ska_sdp_config/version.py``

- Update ``CHANGELOG.rst`` for the new version
- Create the git tag with ``make git-create-tag``
- Push the changes with ``make git-push-tag``
