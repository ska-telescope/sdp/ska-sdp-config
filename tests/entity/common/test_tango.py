from json import loads

import pytest
from pydantic import AnyUrl, TypeAdapter, ValidationError

from ska_sdp_config.entity.common import TangoAttributeUrl
from ska_sdp_config.entity.common.tango import TANGO_ATTRIBUTE_RE

TangoAttributeUrl_SCHEMA = {
    "format": "uri",
    "minLength": 1,
    "type": "string",
    "pattern": TANGO_ATTRIBUTE_RE.pattern,
}


@pytest.mark.parametrize(
    "json,scheme,device,attr,port,fragment",
    [
        (
            '"tango://mid-sdp/subarray/01/ingest_metrics"',
            "tango",
            "mid-sdp/subarray/01",
            "ingest_metrics",
            None,
            None,
        ),
        (
            '"tango://mid-sdp.local:80/subarray/01/ingest_metrics#dbase=no"',
            "tango",
            "mid-sdp.local/subarray/01",
            "ingest_metrics",
            80,
            "dbase=no",
        ),
        (  # allow underscores as well as dashes in device domain
            '"tango://mid_sdp/subarray-test/01/ingest_metrics"',
            "tango",
            "mid_sdp/subarray-test/01",
            "ingest_metrics",
            None,
            None,
        ),
        (  # allow underscores as well as dashes in device family
            '"tango://mid-sdp/tm_leaf_node/SKA001/sourceOffset"',
            "tango",
            "mid-sdp/tm_leaf_node/SKA001",
            "sourceOffset",
            None,
            None,
        ),
    ],
)
def test_tango_attribute_url(json, scheme, device, attr, port, fragment):
    ta = TypeAdapter(TangoAttributeUrl)
    url: AnyUrl = ta.validate_json(json)
    assert ta.json_schema() == TangoAttributeUrl_SCHEMA

    assert isinstance(url, TangoAttributeUrl)
    assert url.scheme == scheme
    assert url.device_name == device
    assert url.attribute_name == attr
    assert url.port == port
    assert url.fragment == fragment
    assert ta.dump_python(url, mode="json") == loads(json)
    assert json == ta.dump_json(url).decode("utf-8")


@pytest.mark.parametrize(
    "json,match",
    [
        pytest.param(
            '"http://mid-sdp/subarray/01"',
            "URL scheme should be 'tango'",
            id="scheme-pattern",
        ),
        pytest.param(
            '"http://mid-sdp/subarray/01/ingest_metrics"',
            "URL scheme should be 'tango'",
            id="scheme",
        ),
        pytest.param(
            '"tango://mid-sdp/subarray/01"',
            "String should match pattern",
            id="pattern",
        ),
        pytest.param(
            '"tango://mid-sdp/subarray/01/ingest_metrics/extra"',
            "String should match pattern",
            id="pattern",
        ),
    ],
)
def test_tango_attribute_url_invalid(json, match):
    ta: TypeAdapter[TangoAttributeUrl] = TypeAdapter(TangoAttributeUrl)
    with pytest.raises(ValidationError, match=match):
        ta.validate_json(json)
