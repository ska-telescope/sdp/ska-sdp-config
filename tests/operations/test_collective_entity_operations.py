"""Tests for generic multi-entity operations support."""

import itertools
import logging

import pytest

from ska_sdp_config.operations.entity_operations import (
    CollectiveEntityOperations,
    InvalidKey,
)


class _SingleKeyPartEntityOperations(CollectiveEntityOperations):
    HAS_OWNER = True
    HAS_STATE = True
    KEY_PARTS = {"name": "[0-9a-zA-Z_-]+"}
    PREFIX = "/single-key-part-entity"


class _MultiKeyPartEntityOperations(CollectiveEntityOperations):
    HAS_OWNER = True
    HAS_STATE = True
    KEY_PARTS = {"number": "[0-9]+", "alpha": "[a-zA-Z]+"}
    PREFIX = "/multi-key-part-entity"


@pytest.fixture(name="prefix")
def _prefix_fixture() -> str:
    return "/__test_operations"


@pytest.fixture(name="txn")
def _custom_txn_fixture(cfg):
    for txn in cfg.txn():
        # Inject our entity operations for this test, hacky
        # pylint: disable-next=protected-access
        base_txn = txn.script._txn
        txn.single = _SingleKeyPartEntityOperations(base_txn)
        txn.multi = _MultiKeyPartEntityOperations(base_txn)
        txn.arbitrary._known_roots.add(_SingleKeyPartEntityOperations.PREFIX)
        txn.arbitrary._known_roots.add(_MultiKeyPartEntityOperations.PREFIX)
        yield txn


def test_base_entity_attributes(txn):
    """Basic key/path informtaion from individual operations object"""

    def assert_multipart_attributes(**kwargs):
        ops = txn.multi(**kwargs)
        assert ops.key == "0:a"
        assert ops.key_parts == {"number": "0", "alpha": "a"}
        assert ops.path == "/multi-key-part-entity/0:a"

    # by key parts and full key
    assert_multipart_attributes(number="0", alpha="a")
    assert_multipart_attributes(alpha="a", number="0")
    assert_multipart_attributes(key="0:a")

    def assert_singlepart_attributes(*args, **kwargs):
        ops = txn.single(*args, **kwargs)
        assert ops.key == "value"
        assert ops.key_parts == {"name": "value"}
        assert ops.path == "/single-key-part-entity/value"

    # by key parts, full key and positional arg
    assert_singlepart_attributes(name="value")
    assert_singlepart_attributes(key="value")
    assert_singlepart_attributes("value")


@pytest.mark.parametrize(
    "invalid_part_name", ("numbers", "numbe", "numbeR", "extra")
)
def test_indexing_with_invalid_part_names(txn, invalid_part_name):
    """Can't index with invalid key part names."""
    indexing_kwargs = {invalid_part_name: "doesnt-matter"}
    with pytest.raises(ValueError, match="is not a valid key part"):
        txn.multi(**indexing_kwargs)


@pytest.mark.parametrize(
    "invalid_part_values", (("a", "0"), ("0a", "a0"), ("", "a"), ("0", ""))
)
def test_indexing_with_invalid_values(txn, invalid_part_values):
    """Can't index with invalid key part values."""
    indexing_kwargs = {
        "number": invalid_part_values[0],
        "alpha": invalid_part_values[1],
    }
    with pytest.raises(ValueError, match="is not a valid value"):
        txn.multi(**indexing_kwargs)


def test_indexing_with_explicit_key(txn):
    """Indexing with an explicit, valid key"""
    assert txn.multi(key="0:a") is not None
    assert txn.single(key="a") is not None
    with pytest.raises(ValueError, match="'key' cannot be combined"):
        txn.multi(key="0:a", number="0")
    with pytest.raises(ValueError, match="'key' cannot be combined"):
        txn.single(key="a", name="a")


def test_indexing_with_positional_arg(txn):
    """Indexing with positional arg is supported for single-part keys"""
    assert txn.single("a") is not None
    with pytest.raises(ValueError, match="is not a valid value"):
        txn.single("not a supported value")
    with pytest.raises(ValueError, match="Only single positional argument"):
        txn.single("a", "b")
    with pytest.raises(ValueError, match="positional and keyword arguments"):
        txn.multi("0", alpha="a")
    with pytest.raises(ValueError, match="Positional arguments unsupported"):
        txn.multi("0")


@pytest.mark.parametrize("invalid_key", ("", "0", "a", "0:", ":a", "a:0"))
def test_indexing_with_invalid_explicit_key(txn, invalid_key):
    """Indexing into multientity with invalid keys."""
    with pytest.raises(InvalidKey):
        txn.multi(key=invalid_key)


def test_list_keys(txn):
    """Listing keys works as expected, with exact/prefix/suffix variants."""
    assert txn.multi.list_keys() == []
    for number, alpha in itertools.product(
        ("000", "010", "020", "999"), ("aaa", "aba", "aca", "zzz")
    ):
        txn.multi(number=number, alpha=alpha).create({})
    assert len(txn.multi.list_keys()) == 16
    for key in txn.multi.query_keys():
        assert len(key) == 7
    assert len(txn.multi.list_keys(number="000")) == 4
    assert len(txn.multi.list_keys(number="010")) == 4
    assert len(txn.multi.list_keys(number_prefix="0")) == 12
    assert len(txn.multi.list_keys(number_prefix="9")) == 4
    assert len(txn.multi.list_keys(number_suffix="9")) == 4


def test_empty_query_argument(txn):
    """Empty query arguments are ignored."""
    txn.multi(key="0:a").create({})
    txn.multi(key="0:b").create({})
    txn.multi(key="1:a").create({})
    txn.multi(key="1:b").create({})
    assert len(txn.multi.list_keys(number="")) == 4
    assert len(txn.multi.list_keys(number="", alpha="a")) == 2


def test_list_values(txn):
    """Listing values works as expected."""
    assert txn.multi.list_values() == []
    for number, alpha in itertools.product(
        ("000", "010", "020", "999"), ("aaa", "aba", "aca", "zzz")
    ):
        txn.multi(number=number, alpha=alpha).create({"n": number, "a": alpha})
    assert len(txn.multi.list_values()) == 16
    for key, value in txn.multi.query_values():
        assert key == value["n"] + ":" + value["a"]


@pytest.mark.parametrize(
    "invalid_part_name", ("numbers", "numbe", "numbeR", "extra")
)
def test_list_keys_with_invalid_part_name(txn, invalid_part_name):
    """Invalid key part names in queries are caught."""
    # These are verified only if there are results to process
    txn.multi(number="0", alpha="a").create({})
    query_kwargs = {invalid_part_name: "doesnt-matter"}
    with pytest.raises(ValueError, match="is not valid"):
        txn.multi.list_keys(**query_kwargs)


def test_invalid_keys_in_db_are_skipped(txn, caplog: pytest.LogCaptureFixture):
    """Rouge entries under the prefix are ignored when querying."""

    txn.arbitrary(
        _MultiKeyPartEntityOperations.PREFIX + "/not-a-valid-key"
    ).create({})

    with caplog.at_level(logging.WARNING):
        assert not txn.multi.list_keys()
    expected_warnings = list(
        record
        for record in caplog.records
        if "doesn't have expected format" in record.getMessage()
        and logging.WARNING == record.levelno
    )
    assert len(expected_warnings) == 1


def test_model_operations_unavailable(txn):
    """
    Test that the convenience methods available to entities modelled via
    pydantic are not available for plain dict-based entities.
    """
    methods = (
        "get",
        "exists",
        "state",
        "ownership",
        "is_alive",
        "take_ownership_if_not_alive",
        "create",
        "update",
        "create_or_update",
        "delete",
    )
    match = "Only available for entities with a pydantic model"
    _dummy = None
    for method in methods:
        with pytest.raises(NotImplementedError, match=match):
            getattr(txn.single, method)(_dummy)
        with pytest.raises(NotImplementedError, match=match):
            getattr(txn.multi, method)(_dummy)
