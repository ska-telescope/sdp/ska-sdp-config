"""
Command line utility for interacting with SKA Science Data Processor (SDP).

Usage:
    ska-sdp COMMAND [options] [SDP_OBJECT] [<args>...]
    ska-sdp COMMAND (-h|--help)
    ska-sdp (-h|--help)

SDP Objects:
    pb           Interact with processing blocks
    script       Interact with available processing script definitions
    deployment   Interact with deployments
    eb           Interact with execution blocks
    flow         Interact with flows
    controller   Interact with Tango controller device
    subarray     Interact with Tango subarray device
    system       Interact with system configuration

Commands:
    list           List information of object from the Configuration DB
    get | watch    Print all the information (i.e. value) of a key in the Config DB
    create         Create a new, raw key-value pair in the Config DB;
                   Run a processing script; Create a deployment
    update         Update a raw key value from CLI
    edit           Edit a raw key value from text editor
    delete         Delete a single key or all keys within a path from the Config DB
    end            Stop/Cancel execution block
    import         Import system or processing script definitions from file or URL

Options:
    --prefix=<prefix>   Global prefix to paths (e.g. /test for testing)
"""  # noqa: E501 # pylint: disable=line-too-long

import logging
import sys

from docopt import docopt

from ska_sdp_config import config
from ska_sdp_config.ska_sdp_cli import (
    sdp_create,
    sdp_delete,
    sdp_end,
    sdp_get,
    sdp_import,
    sdp_list,
    sdp_update,
)

LOG = logging.getLogger("ska-sdp")
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler(sys.stdout))

COMMAND = "COMMAND"


def main(argv=None):
    """Run ska-sdp."""
    if argv is None:
        argv = sys.argv[1:]

    args = docopt(__doc__, argv=argv, options_first=True)
    prefix = args.get("--prefix")
    if prefix is None:
        prefix = ""

    cfg = config.Config(global_prefix=prefix)
    status = 0

    if args[COMMAND] == "list":
        sdp_list.main(argv, cfg)

    elif args[COMMAND] == "get" or args[COMMAND] == "watch":
        sdp_get.main(argv, cfg)

    elif args[COMMAND] == "create":
        sdp_create.main(argv, cfg)

    elif args[COMMAND] == "update" or args[COMMAND] == "edit":
        sdp_update.main(argv, cfg)

    elif args[COMMAND] == "delete":
        sdp_delete.main(argv, cfg)

    elif args[COMMAND] == "import":
        status = sdp_import.main(argv, cfg)

    elif args[COMMAND] == "end":
        sdp_end.main(argv, cfg)

    else:
        LOG.error(
            "Command '%s' is not supported. Run 'ska-sdp --help' to view "
            "usage.",
            args[COMMAND],
        )

    return status


def main_with_exit_status(argv=None):
    """Run ska-sdp and set exit status."""
    sys.exit(main(argv))


if __name__ == "__main__":
    main()
