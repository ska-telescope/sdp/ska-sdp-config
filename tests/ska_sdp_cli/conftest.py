"""CLI fixtures"""

import pytest

from ska_sdp_config import ProcessingBlock
from ska_sdp_config.entity import Script, flow

SCRIPT1 = Script(
    key=Script.Key(kind="batch", name="test", version="0.0.1"), image="image1"
)
SCRIPT2 = Script(
    key=Script.Key(kind="batch", name="test", version="0.0.2"),
    image="image2",
    parameters={"test": "test"},
)
PB1 = ProcessingBlock(
    key="pb-test-20240820-00000", eb_id=None, script=SCRIPT1.key
)
PB2 = ProcessingBlock(
    key="pb-test-20240821-00001",
    eb_id="eb-test-20240821-00001",
    script=SCRIPT2.key,
)
FLOW1 = flow.Flow(
    key=flow.Flow.Key(
        pb_id=PB1.key,
        kind="data-product",
        name="pointing-offset",
    ),
    sink=flow.DataProduct(
        kind="data-product",
        data_dir="/wow",
        paths=[],
    ),
    sources=[],
    data_model="PointingTable",
)
FLOW2 = flow.Flow(
    key=flow.Flow.Key(
        pb_id=PB2.key,
        kind="data-queue",
        name="pointing-offset",
    ),
    sink=flow.DataQueue(
        topics="some-kafka-topic",
        host="some-kafka-host:9092",
        format="json",
    ),
    sources=[],
    data_model="PointingTable",
)


@pytest.fixture(name="cfg_data")
def cfg_with_data_fixt(cfg):  # noqa: F811
    """
    Fixture of temporary config backend, with data in the db,
    which are deleted after the test(s) finished executing.

    Uses the etcd3 fixture.
    """
    for txn in cfg.txn():
        txn.arbitrary("/my_path").create({"key": "MyValue"})
        txn.processing_block.create(PB1)
        txn.processing_block.state(PB1.key).create({})
        txn.processing_block.create(PB2)
        txn.script.create(SCRIPT1)
        txn.script.create(SCRIPT2)
        txn.flow.create(FLOW1)
        txn.flow.state(FLOW1).create({"status": "TEST"})
        txn.flow.create(FLOW2)
        txn.flow.state(FLOW2).create({"status": "TEST"})

    yield cfg
