"""Tests for generic single entity operations support."""

import pytest

from ska_sdp_config import ConfigCollision, ConfigVanished
from ska_sdp_config.base_transaction import BaseTransaction
from ska_sdp_config.operations.entity_operations import (
    EntityOperations,
    OwnedEntityOperationsMixIn,
    StatefulEntityOperationsMixIn,
)


class _SingleEntityOperations(
    EntityOperations,
    OwnedEntityOperationsMixIn,
    StatefulEntityOperationsMixIn,
):
    PATH = "/single-entity"

    def __init__(self, txn: BaseTransaction):
        super().__init__(txn, self.PATH)


@pytest.fixture(name="prefix")
def _prefix_fixture() -> str:
    return "/__test_operations"


def _create_single_entity_operations(txn) -> _SingleEntityOperations:
    # pylint: disable-next=protected-access
    base_txn = txn.script._txn
    return _SingleEntityOperations(base_txn)


@pytest.fixture(name="ops")
def _custom_txn_fixture(cfg):
    for txn in cfg.txn():
        yield _create_single_entity_operations(txn)


def test_entity_key_and_path(ops: _SingleEntityOperations):
    """Basic key/path informtaion from operations object"""
    assert ops.key is None
    assert ops.path == "/single-entity"
    assert 'path="/single-entity' in str(ops)


def test_entity_crud_ops(ops: _SingleEntityOperations):
    """Create/read/update/delete work as expeceted."""
    value_original = {"a": 1}
    value_updated = {"a": 0}

    assert ops.get() is None
    assert not ops.exists()
    ops.create(value_original)
    assert ops.get() == value_original
    assert ops.exists()
    with pytest.raises(ConfigCollision):
        ops.create(value_original)
    ops.update(value_updated)
    assert ops.get() == value_updated
    ops.create_or_update(value_original)
    assert ops.get() == value_original
    ops.delete()
    assert ops.get() is None
    with pytest.raises(ConfigVanished):
        ops.delete()
    with pytest.raises(ConfigVanished):
        ops.update(value_updated)
    ops.create_or_update(value_updated)
    assert ops.get() == value_updated


def test_take_ownership(cfg):
    """Taking ownership works and uses the Config's ownership record."""

    for txn in cfg.txn():
        # Store something under the entity's path to check that
        # revoking the lease doesn't remove the entity itself
        ops = _create_single_entity_operations(txn)
        ops.create({})

    def _assert_owned(ops, owned, owner_info):
        assert owned == ops.ownership.is_owned()
        assert owned == ops.ownership.is_owned_by_this_process()
        assert owned == ops.is_alive()
        assert owner_info == ops.ownership.get()
        # the entity itself always remains
        assert ops.exists()

    # Take ownership explicitly
    for txn in cfg.txn():
        ops = _create_single_entity_operations(txn)
        ops.ownership.take()
        _assert_owned(ops, True, cfg.owner)

    # Remove client lease, there's no ownership anymore
    cfg.revoke_lease()
    for txn in cfg.txn():
        ops = _create_single_entity_operations(txn)
        _assert_owned(ops, False, None)

    # Take ownership via indirect method
    for txn in cfg.txn():
        ops = _create_single_entity_operations(txn)
        ops.take_ownership_if_not_alive()
        _assert_owned(ops, True, cfg.owner)


def test_entity_state(ops: _SingleEntityOperations):
    """State can be managed successfully."""
    sate_original = {"a": 1}
    sate_updated = {"a": 0}
    state = ops.state
    assert state.get() is None
    state.create(sate_original)
    assert state.get() == sate_original
    state.update(sate_updated)
    assert state.get() == sate_updated
