"""High-level config tests."""

import threading

import pytest

import ska_sdp_config
from ska_sdp_config.config import Config, Transaction


@pytest.fixture(scope="module", name="prefix")
def prefix_fxt():
    """Database prefix."""
    return "/__test_config"


@pytest.mark.parametrize(
    "owned_entity",
    [("component", "test"), ("processing_block", "pb-a-00000000-b")],
)
def test_alive(cfg, prefix):
    """Test alive methods."""

    assert not cfg.is_alive()
    cfg.set_alive()
    assert cfg.is_alive()
    for txn in cfg.txn():
        assert txn.self
        assert txn.self.is_alive()
    for txn in cfg.txn():
        assert txn.raw.get(prefix + txn.self.path + "/owner") is not None
    cfg.set_alive()
    assert cfg.is_alive()
    threads = threading.enumerate()
    found = False
    for thread in threads:
        if thread.name.find("keepalive"):
            found = True
            break
    assert found


@pytest.mark.parametrize("owned_entity", [("component", "test")])
def test_new_leases_work(cfg):
    """Test that the internal lease is recreated and kept alive."""

    def _assert_transition_from_not_alive_to_alive():
        assert not cfg.is_alive()
        cfg.set_alive()
        assert cfg.is_alive()

    # Revoke the lease, owned entity is not alive,
    # but setting it alive works again
    _assert_transition_from_not_alive_to_alive()
    cfg.revoke_lease()
    _assert_transition_from_not_alive_to_alive()


def test_alive_none(cfg):
    """Test alive methods when no entity of interest is provided."""

    for func in (cfg.is_alive, cfg.set_alive):
        with pytest.raises(TypeError) as err:
            func()
        assert "No entity provided at creation time" in str(err.value)

    # can't use txn.self
    for txn in cfg.txn():
        assert txn.self is None


@pytest.mark.parametrize("key", ["my-component/owner", "some-key"])
def test_txn_create_is_alive(key, cfg, prefix):
    """
    Test txn.create_is_alive creates any key
    that is passed to it as a string.
    """
    for txn in cfg.txn():
        txn.component.arbitrary_create_is_alive(key)

    for txn in cfg.txn():
        assert txn.component.arbitrary_check_is_alive(key)

    # pylint: disable=protected-access
    for txn in cfg._backend.txn():
        alive_keys = txn.list_keys(f"{prefix}/component", recurse=6)
        assert f"{prefix}/component/{key}" in alive_keys


@pytest.mark.parametrize("owned_entity", [("component", "test")])
def test_wrapper(cfg):
    """Test transaction wrapper."""

    # pylint: disable= too-few-public-methods
    class Test:
        """Wrapper."""

        def __init__(self, txn: Transaction):
            self.txn = txn

        def do_stuff(self) -> str:
            """stuff"""
            return "done"

    for txn in cfg.txn():
        assert isinstance(txn, Transaction)
    cfg.wrapper = Test
    assert cfg.wrapper == Test

    # Test liveness actions, these used to fail
    cfg.set_alive()
    assert cfg.is_alive()

    for txn in cfg.txn():
        assert isinstance(txn, Test)
        assert isinstance(txn.txn, Transaction)
        assert txn.do_stuff() == "done"

    for watcher in cfg.watcher(timeout=0.1):
        for txn in watcher.txn():
            assert isinstance(txn, Test)
            assert isinstance(txn.txn, Transaction)
            assert txn.do_stuff() == "done"
        break


def test_close_config(prefix, cfg):
    """
    Test that client lease is revoked when a connection to the
    db is closed by the config object that created it.
    """
    with ska_sdp_config.Config(global_prefix=prefix) as config:
        for txn in config.txn():
            txn.component.arbitrary_create_is_alive("test-close-lease/owner")
        # pylint: disable=protected-access
        for txn in config._backend.txn():
            alive_keys = txn.list_keys(f"{prefix}/component", recurse=6)
            assert f"{prefix}/component/test-close-lease/owner" in alive_keys

    # pylint: disable=protected-access
    for txn in cfg._backend.txn():
        alive_keys = txn.list_keys(f"{prefix}/component", recurse=6)
        assert f"{prefix}/component/test-close-lease/owner" not in alive_keys


def test_component_name_is_deprecated():
    """Test component_name argument deprecation"""
    with pytest.warns(DeprecationWarning):
        cfg = Config(component_name="blah")
    for txn in cfg.txn():
        assert txn.self
        assert txn.self.path == "/component/blah"
