"""Execution block configuration entities."""

from typing import Annotated

from pydantic import Field

from .base import MultiEntityBaseModel
from .common import ExecutionBlockId, ProcessingBlockId


class ExecutionBlock(MultiEntityBaseModel):
    """Execution block entity.

    Collects configuration information relating to an execution block for the
    SDP.
    """

    key: ExecutionBlockId
    """ID of the Execution Block"""

    beams: Annotated[list, Field(default_factory=list)]
    """Beam parameters for the purpose of the Science Data Processor"""

    channels: Annotated[list, Field(default_factory=list)]
    """
    Channels. This is basically a list of spectral windows per channel
    configuration.
    """

    context: Annotated[dict, Field(default_factory=dict)]
    """Free-form information from OET to describe the observing context"""

    fields: Annotated[list, Field(default_factory=list)]
    """List of fields/targets"""

    max_length: Annotated[float | None, Field(default=None)]
    """Maximum duration of the execution block in seconds."""

    pb_batch: Annotated[list[ProcessingBlockId], Field(default_factory=list)]
    """Batch processing blocks."""

    pb_realtime: Annotated[
        list[ProcessingBlockId], Field(default_factory=list)
    ]
    """Real-time processing blocks."""

    polarisations: Annotated[list, Field(default_factory=list)]
    """Polarisation definitions"""

    resources: Annotated[dict, Field(default_factory=dict)]
    """Free-form Resources allocated to the execution block."""

    scan_types: Annotated[list, Field(default_factory=list)]
    """
    Scan types. Associates scans with per-beam fields & channel configurations
    """

    subarray_id: Annotated[
        str | None, Field(pattern=r"^[0-9]+$", default=None)
    ]
    """Subarray with which this EB is associated."""
