"""Deployment configuration entities."""

from .base import EntityBaseModel


class Owner(EntityBaseModel):
    """
    Owner information for an entity, uniquely identifying the process owning
    the entity on a distributed system.
    """

    pid: int
    """The process ID of the entity's owner"""

    hostname: str
    """The name of the host where the process is running"""

    command: list[str]
    """The command line used to start the process"""
