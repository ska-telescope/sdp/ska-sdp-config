"""
Flow management for SDP Configuration Database.
"""

from ..entity import Flow
from .entity_operations import CollectiveEntityOperations


class FlowOperations(CollectiveEntityOperations[Flow, Flow.Key]):
    """Database operations related to flows management."""

    PREFIX = "/flow"
    MODEL_CLASS = Flow
    HAS_OWNER = True
    HAS_STATE = True
