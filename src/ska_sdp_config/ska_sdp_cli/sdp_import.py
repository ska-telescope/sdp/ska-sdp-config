# pylint: disable=line-too-long
"""
Import processing script definitions into the Configuration Database.
It currently supports input as a yaml file or URL containing a json string.
Note that the input can contain a system defintion, a single script,
multiple versions of a script, or multiple scripts.

Usage:
    ska-sdp import (system|script|scripts) [options] <file-or-url>
    ska-sdp import (-h|--help)

Arguments:
    <file-or-url>   File or URL to import script definitions from.
                    URL can also be a pointer to Telescope Model Data in the
                    format of: "tmdata::<tmdata-source-url>::<tmdata-file>"
                    <tmdata-file> is optional and defaults to "ska-sdp/scripts/scripts.yaml"

Options:
    -h, --help      Show this screen
    --sync          Delete scripts not in the input (only works for script/scripts)
"""  # noqa: E501

import logging
import os

import requests
import yaml
from docopt import docopt
from ska_telmodel.data import TMData
from yaml.parser import ParserError
from yaml.scanner import ScannerError

from ska_sdp_config.config import Transaction
from ska_sdp_config.entity import Script
from ska_sdp_config.entity.system import System

LOG = logging.getLogger("ska-sdp")


def read_input(input_object: str) -> dict:
    """
    Read script definitions from file or URL.

    :param input_object: input filename or URL
    :returns: definitions converted into dict
    """
    if os.path.isfile(input_object):
        with open(input_object, "r", encoding="utf8") as file:
            data = file.read()

    else:
        with requests.get(input_object, timeout=60.0) as response:
            data = response.text

    definitions = yaml.safe_load(data)

    return definitions


def read_tmdata(input_object: str) -> (dict, TMData):
    """
    Read data from Telescope Model Data repository

    :param input_object: reference to tmdata in the form of
            tmdata::<comma-separated-source-list>::<file-to-load>
    :returns: definitions converted into dict
    """
    tmdata = input_object.split("::")
    sources = [tmdata[1]]
    if len(tmdata) == 2:
        # if no file is given, default to the following
        tmdata.append("ska-sdp/scripts/scripts.yaml")
    file_name = tmdata[2]

    tmdata = TMData(sources, update=True)
    definitions = tmdata[file_name].get_dict()

    return definitions, tmdata


def _parse_structured(definitions: dict) -> dict:
    """
    Parse structured script definitions.

    :param definitions: structured script definitions
    :returns: dictionary mapping (kind, name, version) to definition.
    """
    repositories = {
        repo["name"]: repo["path"] for repo in definitions["repositories"]
    }
    scripts = {
        (w["kind"], w["name"], v): {
            "image": repositories[w["repository"]]
            + "/"
            + w["image"]
            + ":"
            + v,
            "sdp_version": w.get("sdp_version"),
            "parameters": w.get("parameters", {}),
        }
        for w in definitions["scripts"]
        for v in w["versions"]
    }
    return scripts


def _parse_flat(definitions: dict) -> dict:
    """
    Parse flat script definitions.

    :param definitions: flat script definitions
    :returns: dictionary mapping (kind, name, version) to definition.
    """
    scripts = {
        (w["kind"], w["name"], w["version"]): {
            "image": w["image"],
            "sdp_version": w.get("sdp_version"),
            "parameters": w.get("parameters", {}),
        }
        for w in definitions["scripts"]
    }
    return scripts


def _parse_tmdata(definitions: dict, tmdata: TMData) -> dict:
    """
    Parse tmdata script definitions.

    :param definitions: script definitions loaded from tmdata
    :returns: dictionary mapping (kind, name, version) to definition.
    """

    base_schema_dir = "ska-sdp/scripts/"

    def handle_missing_schema(script_def):
        try:
            script_schema_file = f"{base_schema_dir}{script_def.get('schema')}"
            return tmdata[script_schema_file].get_dict()
        except AttributeError:
            return {}

    scripts = {
        (w["kind"], w["name"], w["version"]): {
            "image": w["image"],
            "sdp_version": w.get("sdp_version"),
            "parameters": handle_missing_schema(w),
        }
        for w in definitions["scripts"]
    }

    return scripts


def parse_definitions(definitions: dict, tmdata=None) -> dict:
    """
    Parse script definitions.

    :param definitions: script definitions
    :param tmdata: TMData object if loading from telescope model data
    :returns: dictionary mapping (kind, name, version) to definition.

    """
    if tmdata:
        scripts = _parse_tmdata(definitions, tmdata)

    else:
        if "repositories" in definitions:
            scripts = _parse_structured(definitions)
        else:
            scripts = _parse_flat(definitions)

    return scripts


def import_system(txn: Transaction, system_defs: dict):
    """
    Import the system definition into the Configuration Database.

    :param txn: Config object transaction
    :param system_defs: dictionary of system definition
    """
    system_data = System(**system_defs)
    current_system = txn.system.get()
    if current_system is None:
        LOG.info("Creating /system entry")
        txn.system.create(system_data)
    else:
        LOG.info("Updating /system entry")
        txn.system.update(system_data)


def import_scripts(txn: Transaction, scripts: dict, sync: bool = True):
    """
    Import the script definitions into the Configuration Database.

    :param txn: Config object transaction
    :param scripts: dictionary of script definitions
    :param sync: delete scripts not in the input
    """
    # Create sorted list of existing and new scripts
    existing_scripts = [
        tuple(key.model_dump().values()) for key in txn.script.query_keys()
    ]

    all_scripts = sorted(list(set(existing_scripts) | set(scripts.keys())))

    change = False
    for key in all_scripts:
        script_key = Script.Key(kind=key[0], name=key[1], version=key[2])
        if key in scripts:
            old_value = txn.script.get(script_key)
            new_value = scripts[key]

            if old_value is None:
                LOG.info("Creating %s", key)
                script = Script(key=script_key, **new_value)
                txn.script.create(script)
                change = True
            elif new_value != old_value:
                LOG.info("Updating %s", key)
                script = Script(key=script_key, **new_value)
                txn.script.update(script)
                change = True

        elif sync:
            LOG.info("Deleting %s", key)
            txn.script.delete(script_key)
            change = True
    if not change:
        LOG.info("No changes")


def main(argv, config):
    """Run ska-sdp import."""
    args = docopt(__doc__, argv=argv)

    LOG.info(
        "Importing system or processing script definitions from %s",
        args["<file-or-url>"],
    )

    if args["<file-or-url>"].startswith("tmdata:"):
        definitions, tmdata = read_tmdata(args["<file-or-url>"])

    else:
        tmdata = None
        try:
            definitions = read_input(args["<file-or-url>"])
        except (
            requests.exceptions.MissingSchema,
            requests.exceptions.ConnectionError,
        ):
            # MissingSchema exception raised when json file doesn't exist
            # ConnectionError raised when URL is wrong
            LOG.error("Bad file name or URL. Please fix, and retry.")
            return 1
        except (ParserError, ScannerError):
            LOG.error("Malformed YAML/JSON file. Please fix, and retry.")
            return 1

    if args["system"]:
        for txn in config.txn():
            import_system(txn, definitions)
        LOG.info("Import system definition finished successfully.")
    elif args["script"] or args["scripts"]:
        scripts = parse_definitions(definitions, tmdata=tmdata)

        for txn in config.txn():
            import_scripts(txn, scripts, sync=args["--sync"])

        LOG.info("Import scripts finished successfully.")
    else:
        LOG.warning("You must import system or script/scripts definition.")
    return 0
