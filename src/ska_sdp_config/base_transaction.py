"""Common code for Transactions and its high-level operations."""

import json
from typing import Callable, Optional

from .backend import DbTransaction, Lease
from .entity import Owner


def dict_to_json(obj: dict) -> str:
    """Format a dictionary for writing it into the database.

    :param obj: Dictionary object to format
    :returns: String representation
    """
    # We only write dictionaries (JSON objects) at the moment
    assert isinstance(obj, dict)
    # Export to JSON. No need to convert to ASCII, as the backend
    # should handle unicode. Otherwise we optimise for legibility
    # over compactness.
    return json.dumps(
        obj,
        ensure_ascii=False,
        separators=(",", ":"),
        sort_keys=True,
    )


class BaseTransaction:
    """
    Common low-level operations for Transaction operations that are all
    prefixed with a common path.
    """

    # pylint: disable-next=too-many-arguments
    def __init__(
        self,
        owner: Owner,
        lease_getter: Callable[[], Lease],
        txn: DbTransaction,
        global_prefix: str,
    ):
        """Instantiate transaction."""
        self._owner = owner
        self._lease_getter = lease_getter
        self._txn = txn
        self._global_prefix = global_prefix

    def _full_path(self, path: str):
        return self._global_prefix + path

    @property
    def owner(self) -> Owner:
        """Process identification used when claiming ownership."""
        return self._owner

    @property
    def lease(self) -> Lease:
        """A Lease for ownership operations to use."""
        return self._lease_getter()

    def get(self, path: str) -> Optional[dict]:
        """Get a JSON object from the database."""
        txt = self._txn.get(self._full_path(path))
        if txt is None:
            return None
        return json.loads(txt)

    def exists(self, path: str) -> bool:
        """Whether the path exists in the database."""
        return self.get(path) is not None

    def create(
        self, path: str, obj: dict, lease: Optional[Lease] = None
    ) -> None:
        """Set a new path in the database to a JSON object."""
        self._txn.create(self._full_path(path), dict_to_json(obj), lease)

    def update(self, path: str, obj: dict) -> None:
        """Set a existing path in the database to a JSON object."""
        self._txn.update(self._full_path(path), dict_to_json(obj))

    def create_or_update(
        self, path: str, value: dict, lease: Lease | None = None
    ) -> None:
        """Set a new or existing path in the database to a JSON object."""
        if self.get(path) is None:
            self.create(path, value, lease)
        else:
            self.update(path, value)

    def delete(self, path: str, recurse: bool = True) -> None:
        """
        Delete one or more JSON objects from the database

        :param path: path to JSON object in DB
        :param recurse: if True, delete object recursively
                        (i.e. all objects whose paths start with path)
        """
        if recurse:
            for key in self._txn.list_keys(self._full_path(path), recurse=8):
                self._txn.delete(key)
        else:
            self._txn.delete(self._full_path(path))

    def list_keys(self, path: str, prefix: str = "") -> list[str]:
        """
        List keys under given path.

        :param path: Path of keys to query.
        :param prefix: Additional prefix after path for matching keys.
        :returns: key list with path stripped.
        """
        full_path = self._full_path(path)
        keys = self._txn.list_keys(full_path + prefix)
        assert all(key.startswith(full_path) for key in keys)
        return list(key[len(full_path) :] for key in keys)
