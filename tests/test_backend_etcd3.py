"""Tests for etcd3 """

# pylint: disable=R0801

import datetime
import functools
import logging
import os

import pytest

from ska_sdp_config.backend import (
    ConfigCollision,
    ConfigVanished,
    Etcd3Backend,
)

PREFIX = "/__test"

get_time = functools.partial(datetime.datetime, tzinfo=datetime.timezone.utc)
LOGGER = logging.getLogger(__name__)


@pytest.fixture(scope="function", name="etcd3_backend")
def etcd3():
    """
    Deletes the prefix key from the database.

    The is used in more than one test, so it must be a fixture.

    """
    host = os.getenv("SDP_CONFIG_HOST", "127.0.0.1")
    port = os.getenv("SDP_CONFIG_PORT", "2379")
    with Etcd3Backend(host=host, port=port) as etcd3_backend:
        etcd3_backend.delete(PREFIX, must_exist=False, recursive=True)
        yield etcd3_backend
        etcd3_backend.delete(PREFIX, must_exist=False, recursive=True)


def test_valid(etcd3_backend):
    """
    Testing to check if all the errors being raised are
    captured correctly.

    :param etcd3_backend: etcd3 backend fixture
    """
    with pytest.raises(ValueError, match="must start"):
        etcd3_backend.create("", "")
    with pytest.raises(ValueError, match="must start"):
        etcd3_backend.create("foo", "")
    with pytest.raises(ValueError, match="trailing"):
        etcd3_backend.create(PREFIX + "/", "")
    with pytest.raises(ValueError, match="trailing"):
        etcd3_backend.update(PREFIX + "/", "")
    with pytest.raises(ValueError, match="trailing"):
        etcd3_backend.get(PREFIX + "/")
    with pytest.raises(ValueError, match="trailing"):
        for txn in etcd3_backend.txn():
            txn.get(PREFIX + "/")


def test_create(etcd3_backend):
    """
    Test creating keys and values and also updating values.

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_create"

    etcd3_backend.create(key, "foo")
    with pytest.raises(ConfigCollision):
        etcd3_backend.create(key, "foo")

    # Check value
    value, ver = etcd3_backend.get(key)
    assert value == "foo"

    # Update, check again. Make sure version was incremented
    etcd3_backend.update(key, "bar")
    value2, ver2 = etcd3_backend.get(key)
    assert value2 == "bar"
    assert ver2.revision > ver.revision

    # Check that we can obtain the previous version
    value3, ver3 = etcd3_backend.get(key, ver)
    assert value3 == "foo"
    assert ver3.revision > ver.revision

    # Delete key
    etcd3_backend.delete(key)
    with pytest.raises(ConfigVanished):
        etcd3_backend.delete(key)


def test_list(etcd3_backend):
    """
    Test for getting list of keys.

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_list"

    # Create a bunch of keys
    etcd3_backend.create(key + "/a", "")
    etcd3_backend.create(key + "/ab", "")
    etcd3_backend.create(key + "/b", "")
    etcd3_backend.create(key + "/ax", "")
    etcd3_backend.create(key + "/ab/c", "")
    etcd3_backend.create(key + "/a/d", "")
    etcd3_backend.create(key + "/a/d/x", "")

    # Try listing
    assert etcd3_backend.list_keys(key + "/")[0] == [
        key + "/a",
        key + "/ab",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key)[0] == []
    assert etcd3_backend.list_keys(key + "/a")[0] == [
        key + "/a",
        key + "/ab",
        key + "/ax",
    ]
    assert etcd3_backend.list_keys(key + "/b")[0] == [key + "/b"]
    assert etcd3_backend.list_keys(key + "/a/")[0] == [key + "/a/d"]
    assert etcd3_backend.list_keys(key + "/ab/")[0] == [key + "/ab/c"]
    assert etcd3_backend.list_keys(key + "/ab")[0] == [key + "/ab"]

    # Try listing recursively
    assert etcd3_backend.list_keys(key, recurse=1)[0] == [
        key + "/a",
        key + "/ab",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key, recurse=(1,))[0] == [
        key + "/a",
        key + "/ab",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key, recurse=2)[0] == [
        key + "/a",
        key + "/a/d",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key, recurse=(2,))[0] == [
        key + "/a/d",
        key + "/ab/c",
    ]
    assert etcd3_backend.list_keys(key + "/", recurse=1)[0] == [
        key + "/a",
        key + "/a/d",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key, recurse=3)[0] == [
        key + "/a",
        key + "/a/d",
        key + "/a/d/x",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key, recurse=[3, 2, 1])[0] == [
        key + "/a",
        key + "/a/d",
        key + "/a/d/x",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
        key + "/b",
    ]
    assert etcd3_backend.list_keys(key + "/a", recurse=2)[0] == [
        key + "/a",
        key + "/a/d",
        key + "/a/d/x",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
    ]
    assert etcd3_backend.list_keys(key + "/a/", recurse=1)[0] == [
        key + "/a/d",
        key + "/a/d/x",
    ]
    assert set(etcd3_backend.list_keys("/", recurse=32)[0]) >= {
        key + "/a",
        key + "/a/d",
        key + "/a/d/x",
        key + "/ab",
        key + "/ab/c",
        key + "/ax",
    }

    # Remove
    etcd3_backend.delete(key, must_exist=False, recursive=True)


def test_lease(etcd3_backend):
    """
    Test creating and revoking lease and behaviour on expiry .

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_lease"
    lease = None
    try:
        lease = etcd3_backend.lease(ttl=5)
        etcd3_backend.create(key, "blub", lease=lease)
        with pytest.raises(ConfigCollision):
            etcd3_backend.create(key, "blub", lease=lease)
        assert lease.ttl != 0
    finally:
        if lease is not None:
            lease.revoke()
    # Key should have been removed by lease expiring
    with pytest.raises(ConfigVanished):
        etcd3_backend.delete(key)


def test_delete(etcd3_backend):
    """
    Testing to check if keys are deleted correctly.

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_delete"

    # Two passes - with and without deleting keys by PREFIX
    for del_prefix in [False, True]:
        # Create deeply recursive structure
        childs = ["/".join([key] + i * ["x"]) for i in range(10)]
        for i, child in enumerate(childs):
            etcd3_backend.create(child, i)

        # Create some keys in a parallel structure sharing a PREFIX
        if not del_prefix:
            etcd3_backend.create(key + "x", "keep!")
            etcd3_backend.create(key + "x/x", "keep!")

        # Delete root
        etcd3_backend.delete(key, prefix=del_prefix)
        for i, child in enumerate(childs):
            if i > 0:
                assert etcd3_backend.get(child)[0] == str(i), child
        assert (etcd3_backend.get(key + "x")[0] is None) == del_prefix
        assert etcd3_backend.get(key + "x/x")[0] == "keep!"

        # This should fail now
        with pytest.raises(ConfigVanished):
            etcd3_backend.delete(
                key, recursive=True, must_exist=True, prefix=del_prefix
            )
        for i, child in enumerate(childs):
            if i > 0:
                assert etcd3_backend.get(child)[0] == str(i), child
        assert (etcd3_backend.get(key + "x")[0] is None) == del_prefix
        assert etcd3_backend.get(key + "x/x")[0] == "keep!"

        # But this one should work, and remove remaining keys
        # (except those with the same PREFIX but different path
        #  unless we set the option before)
        etcd3_backend.delete(
            key, recursive=True, must_exist=False, prefix=del_prefix
        )
        for child in childs:
            assert etcd3_backend.get(child)[0] is None, child
        assert (etcd3_backend.get(key + "x")[0] is None) == del_prefix
        assert (etcd3_backend.get(key + "x/x")[0] is None) == del_prefix


def _make_txn(etcd3_backend, txn_mode, keys):
    """
    Return a function that creates transactions - depending
    on txn_mode wrapped inside watchers in different states.
    Transactions should all have the same semantics.
    """

    # Plain transaction
    if txn_mode == "txn":
        return etcd3_backend.txn

    # For every transaction, create a watcher and execute transaction
    # inside it
    if txn_mode == "watcher_1":

        def mk_txn():
            for watch in etcd3_backend.watcher():
                yield from watch.txn()
                break

        return mk_txn

    # Create one watcher, execute all transactions inside it
    if txn_mode == "watcher_n":
        watcher = iter(etcd3_backend.watcher())
        watch = next(watcher)

        def mk_txn_n():
            watcher  # pylint: disable=pointless-statement
            yield from watch.txn()

        return mk_txn_n

    # Create one watcher for every transaction, cache given keys
    # before executing transactions
    if txn_mode == "precache_1":

        def mk_txn_pc():
            for watch in etcd3_backend.watcher():
                for key in keys:
                    for txn in watch.txn():
                        txn.get(key)
                yield from watch.txn()
                break

        return mk_txn_pc

    # Create only one watcher, cache given keys, and execute all
    # transactions inside it
    if txn_mode == "precache_n":
        watcher = iter(etcd3_backend.watcher())
        watch = next(watcher)
        for key in keys:
            for txn in watch.txn():
                txn.get(key)

        def mk_txn_pc_n():
            watcher  # pylint: disable=pointless-statement
            yield from watch.txn()

        return mk_txn_pc_n

    raise ValueError(f"Unknown txn_mode {txn_mode}!")


@pytest.mark.parametrize(
    "txn_mode", ["txn", "watcher_1", "watcher_n", "precache_1", "precache_n"]
)
def test_transaction_simple(etcd3_backend, txn_mode):
    """
    Unit test for simple methods in Etcd3Transactions

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_txn"
    key2 = PREFIX + "/test_txn/2"
    mk_txn = _make_txn(etcd3_backend, txn_mode, [key, key2])

    # Make sure we can do simple things, like reading a key
    for txn in mk_txn():
        txn.create(key, "test")
    assert etcd3_backend.get(key)[0] == "test"

    for txn in mk_txn():
        assert txn.get(key) == "test"
        assert txn.get(key) == "test"
    for txn in mk_txn():
        txn.update(key, "test2")
    assert etcd3_backend.get(key)[0] == "test2"
    for txn in mk_txn():
        txn.update(key, "test3")
        assert txn.get(key) == "test3"
    assert etcd3_backend.get(key)[0] == "test3"
    etcd3_backend.delete(key)
    for txn in mk_txn():
        txn.create(key, "test")
        assert txn.get(key) == "test"
        txn.update(key, "test2")
        assert txn.get(key) == "test2"
    assert etcd3_backend.get(key)[0] == "test2"

    for txn in mk_txn():
        assert txn.get(key2) is None
        assert txn.get(key) == "test2"
        txn.create(key2, "test2")
        assert txn.get(key2) == "test2"
        txn.update(key, "test4")
        assert txn.get(key) == "test4"
        assert txn.get(key2) == "test2"
    assert etcd3_backend.get(key2)[0] == "test2"

    etcd3_backend.delete(key, recursive=True)


@pytest.mark.parametrize(
    "txn_mode", ["txn", "watcher_1", "watcher_n", "precache_1", "precache_n"]
)
def test_transaction_list_keys_w_delete(etcd3_backend, txn_mode):
    """
    txn.list_keys should not return a key that was set up
    to be deleted in the same txn loop.
    """
    key = PREFIX + "/test_txn_delete"
    mk_txn = _make_txn(etcd3_backend, txn_mode, [key])

    etcd3_backend.create(key, "1")
    for txn in mk_txn():
        keys = txn.list_keys(key)
        assert keys[0] == key

        # now delete it
        txn.delete(key)

        keys = txn.list_keys(key)
        assert keys == []


@pytest.mark.parametrize(
    "txn_mode", ["txn", "watcher_1", "watcher_n", "precache_1", "precache_n"]
)
def test_transaction_delete(etcd3_backend, txn_mode):
    """
    Unit test for delete in Etcd3Transactions

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_txn_delete"
    mk_txn = _make_txn(etcd3_backend, txn_mode, [key])

    etcd3_backend.create(key, "1")
    for txn in mk_txn():
        txn.delete(key)
    assert etcd3_backend.get(key)[0] is None

    etcd3_backend.create(key, "1")
    for txn in mk_txn():
        with pytest.raises(ConfigCollision):
            txn.create(key, "2")
        txn.delete(key)
        with pytest.raises(ConfigVanished):
            txn.update(key, "3")
        with pytest.raises(ConfigVanished):
            txn.delete(key)
        txn.create(key, "4")
    assert etcd3_backend.get(key)[0] == "4"

    etcd3_backend.delete(key)
    for txn in mk_txn():
        with pytest.raises(ConfigVanished):
            txn.update(key, "3")
        with pytest.raises(ConfigVanished):
            txn.delete(key)
        txn.create(key, "4")
        with pytest.raises(ConfigCollision):
            txn.create(key, "2")
        txn.update(key, "3")
        txn.delete(key)
    assert etcd3_backend.get(key)[0] is None


@pytest.mark.parametrize(
    "txn_mode", ["txn", "watcher_1", "watcher_n", "precache_1", "precache_n"]
)
@pytest.mark.parametrize(
    "recursive, prefix, expected_suffix",
    [
        (True, False, ["bc", "bc/b"]),
        (False, True, ["/b", "/b/c", "bc/b"]),
        (True, True, []),
        (False, False, ["/b", "/b/c", "bc", "bc/b"]),
    ],
)
def test_txn_delete_recursive(
    etcd3_backend, recursive, prefix, expected_suffix, txn_mode
):
    """
    Delete recursively and with prefix.
    If any of these are set to True, delete all keys that
    start with given path.
    """
    path = PREFIX + "/test_txn_delete"
    mk_txn = _make_txn(etcd3_backend, txn_mode, [path])

    etcd3_backend.create(path, "1")
    etcd3_backend.create(path + "/b", "1")
    etcd3_backend.create(path + "/b/c", "1")
    etcd3_backend.create(path + "bc", "1")
    etcd3_backend.create(path + "bc/b", "1")

    for txn in mk_txn():
        keys = txn.list_keys(path, recurse=4)
        assert len(keys) == 5
        txn.delete(path, recursive=recursive, prefix=prefix)

    keys = etcd3_backend.list_keys(path, recurse=4)[0]
    expected_keys = [path + suffix for suffix in expected_suffix]
    assert sorted(keys) == sorted(expected_keys)


def test_transaction_lease(etcd3_backend):
    """
    Unit test for lease in Etcd3Transactions

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_txn_lease"
    lease = None
    try:
        lease = etcd3_backend.lease(ttl=5)
        for txn in etcd3_backend.txn():
            txn.create(key, "blub", lease=lease)
            with pytest.raises(ConfigCollision):
                txn.create(key, "blub", lease=lease)
        for txn in etcd3_backend.txn():
            assert txn.get(key) == "blub"
        assert lease.ttl != 0
    finally:
        if lease is not None:
            lease.revoke()
    for txn in etcd3_backend.txn():
        assert txn.get(key) is None


# pylint: disable=W0631
@pytest.mark.parametrize(
    "txn_mode", ["txn", "watcher_1", "watcher_n", "precache_1", "precache_n"]
)
def test_transaction_on_commit(etcd3_backend, txn_mode):
    """
    Unit test for on_commit in Etcd3Transactions

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_transaction_on_commit"
    key2 = key + "/2"
    key3 = key + "/3"
    key4 = key + "/4"
    mk_txn = _make_txn(etcd3_backend, txn_mode, [key, key2, key3, key4])

    counter = {"i": 0}

    def increase_counter():
        counter["i"] += 1

    # Ensure reading is consistent
    etcd3_backend.create(key, "1")
    etcd3_backend.create(key2, "1")
    for i, txn in enumerate(mk_txn()):
        txn.on_commit(increase_counter)
        # First "get" should bake in revision, so subsequent calls
        # return values from this point in time
        txn.get(key)
        etcd3_backend.update(key2, "2")
        assert txn.get(key2)[0] == "1"
    # As this transaction is not writing anything, it will not get repeated
    assert i == 0
    assert counter["i"] == 0  # No commit happens

    # Now check the behaviour if we write something. This time we
    # might be writing an inconsistent value to the database, so the
    # transaction needs to be repeated (10 times!)
    for i, txn in enumerate(mk_txn()):
        txn.on_commit(increase_counter)
        value2 = txn.get(key2)
        if i < 10:
            etcd3_backend.update(key2, i)
        txn.create(key3, int(value2) + 1)
    assert i == 10
    assert etcd3_backend.get(key2)[0] == "9"
    assert etcd3_backend.get(key3)[0] == "10"
    assert counter["i"] == 1
    counter["i"] = 0

    # Same experiment, but with a key that didn't exist yet
    for i, txn in enumerate(mk_txn()):
        txn.on_commit(increase_counter)
        txn.get(key4)
        if i == 0:
            etcd3_backend.create(key4, "1")
        txn.update(key3, int(i))
    assert i == 1
    assert counter["i"] == 1
    etcd3_backend.delete(key4)
    counter["i"] = 0

    # This is especially important because it underpins the safety
    # check of create():
    for i, txn in enumerate(mk_txn()):
        txn.on_commit(increase_counter)
        # "Succeeds" first time only to cause the transaction to get
        # re-run to find a failure the second time around
        if i == 0:
            txn.create(key4, "2")
            etcd3_backend.create(key4, "1")
        if i == 1:
            with pytest.raises(ConfigCollision):
                txn.create(key4, "2")
    assert i == 1
    assert counter["i"] == 0  # No commit

    etcd3_backend.delete(key, recursive=True, must_exist=False)


def test_transaction_list(etcd3_backend):
    """
    Unit test for list_keys in Etcd3Transactions

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_txn_list"
    keys = [key + "/" + str(i) for i in range(5)]
    keys_sub = [key + "/sub" for key in keys]
    for txn in etcd3_backend.txn():
        for k in keys:
            txn.create(k, k)
            txn.create(k + "/sub", k + "/sub")

    # Ensure that we can list the keys
    assert etcd3_backend.list_keys(key + "/")[0] == keys
    for txn in etcd3_backend.txn():
        assert txn.list_keys(key + "/") == keys
        assert txn.list_keys(key + "/", recurse=1) == sorted(keys + keys_sub)
        assert txn.list_keys(key + "/", recurse=(1,)) == keys_sub
    for txn in etcd3_backend.txn():
        assert txn.list_keys(key + "/", recurse=(1,)) == keys_sub

    # Ensure that we can add another key into the range and have it
    # appear to the transaction *before* we commit
    keys.append(key + "/xxx")
    assert etcd3_backend.list_keys(key + "/")[0] == keys[:-1]
    for txn in etcd3_backend.txn():
        assert txn.list_keys(key + "/") == keys[:-1]
        txn.create(key + "/xxx", "xxx")
        assert txn.list_keys(key + "/", recurse=3) == sorted(keys + keys_sub)
        assert txn.list_keys(key + "/") == keys
        assert etcd3_backend.list_keys(key + "/")[0] == keys[:-1]
    assert etcd3_backend.list_keys(key + "/")[0] == keys

    # Test iteratively building up
    etcd3_backend.delete(key, recursive=True, must_exist=False)
    for i, k in enumerate(keys):
        for txn in etcd3_backend.txn():
            assert txn.list_keys(key + "/") == keys[:i]
            txn.create(k, k)

    # Removing keys causes a re-run, but a value update does not.
    for i, txn in enumerate(etcd3_backend.txn()):
        txn.list_keys(key + "/")
        etcd3_backend.delete(keys[5], must_exist=False)
        txn.update(keys[4], keys[3 - i])  # stand-in side effect
    assert i == 1
    assert etcd3_backend.get(keys[5])[0] is None

    # Adding a new key into the range should also invalidate the transaction
    for i, txn in enumerate(etcd3_backend.txn()):
        txn.list_keys(key + "/")
        if i == 0:
            etcd3_backend.create(keys[5], keys[5])
        txn.update(keys[4], keys[3 - i])  # stand-in side effect
    assert i == 1
    etcd3_backend.delete(key, recursive=True, must_exist=False)


@pytest.mark.timeout(10)
def test_transaction_retries(etcd3_backend):
    """
    Test for transaction retry behaviour

    :param etcd3_backend: etcd3 backend fixture
    """
    key = PREFIX + "/test_txn_retries"

    etcd3_backend.create(key, "0")

    # Check that we actually give up eventually
    with pytest.raises(RuntimeError, match="9 retries"):
        for i, txn in enumerate(etcd3_backend.txn(max_retries=9)):
            value2 = txn.get(key)
            etcd3_backend.update(key, i)
            txn.update(key, value2 + "x")
    assert i == 9
    assert etcd3_backend.get(key)[0] == "9"


def test_dropped_update_warning(etcd3_backend, caplog):
    """
    Check that a warning gets generated when we break a transaction
    loop with pending updates
    """

    for txn in etcd3_backend.txn():
        txn.create("/test_dropped_update_warning", "foo")
        break
    assert "Transaction loop aborted - dropping updates to" in caplog.text


# pylint: disable=protected-access
def test_transaction_commit_if_changed(etcd3_backend):
    """
    bugfix (SKB-685): etcd backend should not commit updates where
    the new value the key matches the existing value

    note: checking _success_list as an indicator of what is going
     to be committed within the txn loop works because committing
     only happens when the txn loop exits
    """
    key = PREFIX + "/test_txn_commit"
    value = "value"
    new_value = "new_value"

    # we create the key with a value, which should
    # result in changes to commit, i.e. it will be in "success_list"
    for txn in etcd3_backend.txn():
        txn.create(key, value)
        result = txn._success_list(txn._client.transactions)

        assert len(result) == 1

    # we update the existing value with a new one, which should
    # result in changes to commit, i.e. it will be in "success_list"
    for txn in etcd3_backend.txn():
        txn.update(key, new_value)
        result = txn._success_list(txn._client.transactions)

        assert len(result) == 1

    # next we update it to the exact same value, in which case
    # it should not be committed, since there are no changes,
    # i.e. the update is not in success_list
    for txn in etcd3_backend.txn():
        txn.update(key, new_value)
        result = txn._success_list(txn._client.transactions)

        # note: before bugfix, this returned 1
        assert len(result) == 0

    # finally, we delete the key which should result in a commit
    # i.e. it will be in "success_list"
    for txn in etcd3_backend.txn():
        txn.delete(key)
        result = txn._success_list(txn._client.transactions)

        assert len(result) == 1
