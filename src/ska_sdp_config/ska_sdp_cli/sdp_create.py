"""
Create SDP objects (deployment, script) in the Configuration Database.
Create a processing block to run a script.

Usage:
    ska-sdp create [options] pb <script> [<parameters>] [--eb=<eb-parameters>]
    ska-sdp create [options] deployment <item-id> <kind> <parameters>
    ska-sdp create [options] script <item-id> <value>
    ska-sdp create [options] flow <parameters>
    ska-sdp create (-h|--help)

Arguments:
    <script>            Script that the processing block will run, in the format:
                            kind:name:version
    <parameters>        Optional parameters for a script, with expected format:
                            '{"key1": "value1", "key2": "value2"}'
                        For deployments, expected format:
                            '{"chart": <chart-name>, "values": <dict-of-values>}'
                        For flow, expected format:
                            '{"key":{}, "sink":{}, "sources":[], "data_model":""}'
    <eb-parameters>     Optional eb parameters for a real-time script
    <item-id>           Id of the new deployment or script
    <kind>              Kind of the new deployment ("helm" or "slurm")
    <value>             Script definition

Options:
    -h, --help     Show this screen
    -q, --quiet    Cut back on unnecessary output

Example:
    ska-sdp create pb realtime:vis-receive:0.0.0 '{"": ""}'

Note: You cannot create processing blocks apart from when they are called to run a script.
Execution blocks are not allowed to be created on their own, but only
when you create a realtime processing block with --eb option on.
"""  # noqa: E501 # pylint: disable=line-too-long

import logging

import yaml
from docopt import docopt
from pydantic_core import ValidationError

from ska_sdp_config import entity

LOG = logging.getLogger("ska-sdp")


def cmd_create_eb(txn, parameters, pb_id):
    """
    Create an EB for a real-time processing block.

    :param txn: Config object transaction
    :param pb_id: Associated processing block ID
    :param parameters: execution block parameters, it can be None

    :return eb_id : ID of the created execution blocks
    """

    # Update EB with parameters from command line
    if parameters:
        pars = yaml.safe_load(parameters)
        try:
            eb_id = pars["key"]
            LOG.debug("Use provided execution block ID: %s", eb_id)
        except KeyError:
            # Create new execution block ID
            eb_id = txn.new_execution_block_id("sdpcli")
    else:
        eb_id = txn.new_execution_block_id("sdpcli")

    # Initialise Execution Block
    eblock = entity.ExecutionBlock(key=eb_id)
    if parameters:
        eblock = eblock.model_copy(update=pars)

    LOG.debug("Updating real time PB_ID to %s", pb_id)
    eblock.pb_realtime.append(pb_id)

    # Create initial EB state
    eblock_state = {
        "scan_type": None,
        "scan_id": None,
        "scans": [],
        "status": "ACTIVE",
    }

    # Create the EB and its state
    txn.execution_block.create(eblock)
    txn.execution_block.state(eb_id).create(eblock_state)

    return eb_id


def cmd_create_pb(txn, script: entity.Script.Key, parameters, eb_pars):
    """
    Create a processing block to run a script.

    :param txn: Config object transaction
    :param script: script key
    :param parameters: dict of script parameters, it can be None
    :param eb_pars: dict of execution block parameters, it can be
                     None

    :return pb_id: ID of the created processing block
    """
    eb_id = None

    if parameters is not None:
        pars = yaml.safe_load(parameters)
    else:
        pars = {}

    # Create new processing block ID, create processing block
    pb_id = txn.new_processing_block_id("sdpcli")

    if script.kind == "realtime":
        eb_id = cmd_create_eb(txn, eb_pars, pb_id)
        LOG.info("Execution block created with eb_id: %s", eb_id)
        LOG.info("The EB can be ended by running: ska-sdp end eb %s", eb_id)

    txn.processing_block.create(
        entity.ProcessingBlock(
            key=pb_id, eb_id=eb_id, script=script, parameters=pars
        )
    )

    return pb_id


def cmd_create_deploy(txn, deploy_kind, deploy_id, parameters):
    """
    Create a deployment.

    :param txn: Config object transaction
    :param deploy_kind: Kind of deployment, ("helm" or "slurm")
    :param deploy_id: ID of the deployment
    :param parameters: String of deployment parameters in the form of:
                       '{"chart": <chart-name>, "values": <dict-of-values>}'
    """
    params_dict = yaml.safe_load(parameters)
    txn.deployment.create(
        entity.Deployment(key=deploy_id, kind=deploy_kind, args=params_dict)
    )


def cmd_create_script(txn, script_key_parts, script_value):
    """
    Create a script.

    :param txn: Config object transaction
    :param script_key_parts: String of script parameters in the form of:
                       '{"kind": <script-kind>, "name": <script-name>,
                       "version":<version>}'
    :param script_value: Custom values containing image information
                         In the form of '{"image":<image>}'
                         Both string or dictionary are allowed
    """

    script_par = script_key_parts.split(":")
    if len(script_par) != 3:
        raise ValueError("Please specify script as 'kind:name:version'!")
    script_key = entity.Script.Key(
        kind=script_par[0], name=script_par[1], version=script_par[2]
    )
    if isinstance(script_value, str):
        scripts_dict = yaml.safe_load(script_value)
    elif isinstance(script_value, dict):
        scripts_dict = script_value
    else:
        raise ValueError("Script value format not supported.")

    if not scripts_dict.get("image"):
        LOG.error(
            "No valid image provided for the script, no script was created."
        )
    else:
        script_data = {"key": script_key, **scripts_dict}
        script = entity.Script(**script_data)
        txn.script.create(script)


def cmd_create_flow(txn, parameters):
    """
    Create a flow.

    :param txn: Config object transaction
    :param parameters: flow parameters
    """

    param_dict = yaml.safe_load(parameters)

    try:
        flow = entity.flow.Flow(**param_dict)
    except ValidationError as err:
        LOG.error("Input parameters were incorrect:\n%s", err)
        return

    flow_state = {"status": "PENDING"}

    txn.flow.create(flow)
    txn.flow.state(flow).create(flow_state)

    LOG.info("Flow created.")


def main(argv, config):
    """Run ska-sdp create."""
    args = docopt(__doc__, argv=argv)

    if args["pb"]:
        script_par = args["<script>"].split(":")
        if len(script_par) != 3:
            raise ValueError("Please specify script as 'kind:name:version'!")
        script_key = entity.Script.Key(
            kind=script_par[0], name=script_par[1], version=script_par[2]
        )

        for txn in config.txn():
            pb_id = cmd_create_pb(
                txn, script_key, args["<parameters>"], args["--eb"]
            )

        LOG.info("Processing block created with pb_id: %s", pb_id)

    if args["deployment"]:
        for txn in config.txn():
            cmd_create_deploy(
                txn, args["<kind>"], args["<item-id>"], args["<parameters>"]
            )

        LOG.info("Deployment created with id: %s", args["<item-id>"])

    if args["script"]:
        for txn in config.txn():
            cmd_create_script(txn, args["<item-id>"], args["<value>"])
        LOG.info("Script created.")

    if args["flow"]:
        for txn in config.txn():
            cmd_create_flow(txn, args["<parameters>"])
