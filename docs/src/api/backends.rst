Backends
--------

.. contents::
   :depth: 1
   :local:

Common
^^^^^^

.. automodule:: ska_sdp_config.backend.common
    :members:
    :undoc-members:

Etcd3 backend
^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.backend.etcd3
    :members:
    :undoc-members:

Etcd3 watcher
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.backend.etcd3_watcher
    :members:
    :undoc-members:

Memory backend
^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.backend.memory
    :members:
    :undoc-members:
