"""System config model entities"""

from typing import Annotated

from pydantic import Field, UrlConstraints
from pydantic_core import Url

from .base import BaseModel, EntityBaseModel


class SystemComponent(BaseModel):
    """
    SDP component entity e.g. lmc-controller.
    """

    devicename: str | None = Field(None, pattern=r"^[^/]+/[^/]+/[^/]+$")
    """The tango device name of this component."""

    image: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_:\./-]+$")]
    """The image used to launch this component."""

    version: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_\.-]+$")]
    """The version of this component."""


class SystemDependency(BaseModel):
    """
    SDP dependency entity e.g. ska-tango-base.
    """

    repository: (
        Annotated[
            Url,
            UrlConstraints(allowed_schemes=["https", "file"]),
        ]
        | Annotated[
            str,
            Field(pattern=r"(alias:|\@)\w([-_]?\w){0,}$"),
        ]
    )

    """The repository containing this dependency."""

    version: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_\.-]+$")]
    """The version of this dependency."""


class System(EntityBaseModel):  # noqa: E0601
    """
    Configuration details for SDP including components,
    dependencies and their versions.
    """

    version: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_\.-]+$")]
    """The version of SDP."""

    components: dict[str, SystemComponent]
    """The components comprising SDP."""

    dependencies: dict[str, SystemDependency]
    """The dependencies of SDP."""
