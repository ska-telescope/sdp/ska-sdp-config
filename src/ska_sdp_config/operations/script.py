"""
Script management for SDP Configuration Database.
"""

from ..entity import Script
from .entity_operations import CollectiveEntityOperations


class ScriptOperations(CollectiveEntityOperations[Script, Script.Key]):
    """Database operations related to scripts management."""

    PREFIX = "/script"
    MODEL_CLASS = Script
