# pylint: disable=missing-docstring
# pylint: disable=redefined-outer-name
# pylint: disable=too-many-statements

import pytest

from ska_sdp_config.backend import (
    ConfigCollision,
    ConfigVanished,
    MemoryBackend,
)
from ska_sdp_config.backend.memory import MemoryTransaction
from ska_sdp_config.base_transaction import dict_to_json

XYZ = "/x/y/z"


@pytest.fixture
def txn() -> MemoryTransaction:
    return next(MemoryBackend().txn())


def test_stuff(txn: MemoryTransaction):
    txn.create("/x", "v0")
    assert txn.get("/x") == "v0"
    txn.create("/x/y", "v1")
    assert txn.get("/x/y") == "v1"
    txn.update("/x/y", "v3")
    assert txn.get("/x/y") == "v3"
    txn.create(XYZ, "v2")

    with pytest.raises(ConfigCollision):
        txn.create("/x/y", "v")
    with pytest.raises(ConfigVanished):
        txn.update("/y/x", "v")
    with pytest.raises(ConfigVanished):
        txn.delete("/y/x")

    paths = txn.list_keys("/")
    assert paths == ["/x"]
    paths = txn.list_keys("/", recurse=1)
    assert paths == ["/x", "/x/y"]
    paths = txn.list_keys("/", recurse=2)
    assert paths == ["/x", "/x/y", XYZ]
    paths = txn.list_keys("/", recurse=3)
    assert paths == ["/x", "/x/y", XYZ]

    paths = txn.list_keys("/x")
    assert paths == ["/x"]
    paths = txn.list_keys("/x", recurse=1)
    assert paths == ["/x", "/x/y"]
    paths = txn.list_keys("/x", recurse=2)
    assert paths == ["/x", "/x/y", XYZ]
    paths = txn.list_keys("/x", recurse=3)
    assert paths == ["/x", "/x/y", XYZ]

    paths = txn.list_keys("/x/")
    assert paths == ["/x/y"]
    paths = txn.list_keys("/x/", recurse=1)
    assert paths == ["/x/y", XYZ]
    paths = txn.list_keys("/x/", recurse=2)
    assert paths == ["/x/y", XYZ]

    paths = txn.list_keys("/x/y")
    assert paths == ["/x/y"]
    paths = txn.list_keys("/x/y", recurse=1)
    assert paths == ["/x/y", XYZ]
    paths = txn.list_keys("/x/y", recurse=2)
    assert paths == ["/x/y", XYZ]

    paths = txn.list_keys("/x/y/")
    assert paths == [XYZ]
    paths = txn.list_keys("/x/y/", recurse=1)
    assert paths == [XYZ]

    paths = txn.list_keys(XYZ)
    assert paths == [XYZ]
    paths = txn.list_keys(XYZ, recurse=1)
    assert paths == [XYZ]

    txn.delete("/x")
    paths = txn.list_keys("/")
    assert len(paths) == 0

    txn.delete(XYZ)
    txn.delete(XYZ, must_exist=False)
    txn.create(XYZ, "v")
    txn.delete("/x", must_exist=False, recursive=True)
    assert len(txn.list_keys("/")) == 0

    txn.commit()
    assert next(iter(txn)) == txn

    assert txn.backend.lease() is not None
    txn.backend.close()


def test_state(txn: MemoryTransaction):
    txn.create("/lmc/controller", dict_to_json({"state": "standby"}))
    txn.delete("/lmc/controller", must_exist=False, recursive=True)
    paths = txn.list_keys("/")
    assert len(paths) == 0
