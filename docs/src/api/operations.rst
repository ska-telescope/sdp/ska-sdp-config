Operations
----------

.. automodule:: ska_sdp_config.operations
    :members:

Common
^^^^^^

.. automodule:: ska_sdp_config.operations.entity_operations
    :members:
    :special-members: __call__
    :show-inheritance:

Arbitrary
^^^^^^^^^

.. automodule:: ska_sdp_config.operations.arbitrary
    :members:
    :special-members: __call__
    :show-inheritance:

Component
^^^^^^^^^

.. automodule:: ska_sdp_config.operations.component
    :members:
    :special-members: __call__
    :show-inheritance:

Deployment
^^^^^^^^^^

.. automodule:: ska_sdp_config.operations.deployment
    :members:
    :show-inheritance:

Execution Block
^^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.operations.execution_block
    :members:
    :show-inheritance:

Processing Block
^^^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.operations.processing_block
    :members:
    :show-inheritance:

Script
^^^^^^

.. automodule:: ska_sdp_config.operations.script
    :members:
    :show-inheritance:

Flow
^^^^

.. automodule:: ska_sdp_config.operations.flow
    :members:
    :show-inheritance:
