"""
Interface specification for a back-end implementation.
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import datetime
from typing import TYPE_CHECKING, Any, Callable, Iterable, Optional, TypeVar

if TYPE_CHECKING:
    from ska_sdp_config.config import Transaction

#: Transaction wrapper signature.
TxnWrapper = TypeVar("TxnWrapper", bound=Callable[[Any], None])

#: Recursion type signature.
RecurseType = int | Iterable[int]


class Lease:
    """
    Lease type definition.

    This defines what a lease looks like, for the purposes of consistent
    typing and documentation. It is not intended to be inherited from by
    a subclass. A typical backend should cast the actual lease object
    to this type. It can be used directly by test backends such as the
    memory backend. Attempts to declare the lease types explicitly as a
    Union type have resulted in cyclic imports.
    """

    # pylint: disable=too-few-public-methods

    def __init__(self):
        self._alive = True

    @property
    @abstractmethod
    def id(self):  # pylint: disable=invalid-name
        """Lease id."""

    def __enter__(self):
        """Dummy enter method."""

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Dummy exit method."""

    def alive(self) -> bool:
        """
        Check aliveness

        :return: if alive
        """
        return self._alive

    def revoke(self):
        """Revoke lease"""


@dataclass(frozen=True)
class DbRevision:
    """
    Identifies the revision of the database.

    The `revision` attribute is the database revision at the point in time when
    the query was made. It can be used for querying a consistent snapshot.
    """

    revision: int


class Backend(ABC):
    """Back-end specification."""

    @abstractmethod
    def lease(self, ttl: float = 10) -> Lease:
        """
        Create a lease.

        :param ttl: time to live
        :return: lease
        """

    @abstractmethod
    def txn(self, max_retries: int = 64) -> Iterable[DbTransaction]:
        """Create a new transaction.

        Note that this uses an optimistic STM-style implementation,
        which cannot guarantee that a transaction runs through
        successfully. Therefore, this function returns an iterator,
        which loops until the transaction succeeds:

        .. code-block:: python

             for txn in etcd3.txn():
                 # ... transaction steps ...

        Note that this will in most cases only execute one
        iteration. If you actually want to loop - for instance because
        you intend to wait for something to happen in the
        configuration - use :py:meth:`watcher()` instead.

        :param max_retries: Maximum number of transaction loops
        :returns: Transaction iterator
        """

    @abstractmethod
    def watcher(
        self, timeout: float = None, txn_wrapper: TxnWrapper = None
    ) -> Iterable[Watcher]:
        """
        Create a watcher.

        Placeholder for now. Will be updated on later iteration.

        :param timeout: timeout in seconds
        :param txn_wrapper: wrapper (factory) to return transaction
        :return: watcher object
        """

    @abstractmethod
    def get(
        self, path: str, revision: Optional[DbRevision] = None
    ) -> tuple[str, DbRevision]:
        """
        Get value of a key.

        :param path: Path of key to query
        :param revision: to get
        :returns: value and revision
        """

    @abstractmethod
    def create(
        self, path: str, value: str, lease: Optional[Lease] = None
    ) -> None:
        """
        Create a key and initialise it with the value.

        :param path: Path to create
        :param value: Value to set
        :param lease: Lease to associate

        :raises: ConfigCollision if the key already exists
        """

    @abstractmethod
    def update(self, path: str, value: str) -> None:
        """
        Update an existing key. Fails if the key does not exist.

        :param path: Path to update
        :param value: New value of key
        :raises: ConfigVanished if the key does not exist
        """

    # pylint: disable=too-many-arguments
    # pylint: disable=too-many-positional-arguments
    @abstractmethod
    def delete(
        self,
        path: str,
        must_exist: bool = True,
        recursive: bool = False,
        prefix: bool = False,
        max_depth: int = 16,
    ) -> None:
        """
        Delete the given key or key range.

        :param path: path (prefix) of keys to remove
        :param must_exist: Fail if path does not exist?
        :param recursive: Delete children keys at lower levels recursively
        :param max_depth: Recursion limit
        :param prefix: Delete all keys at given level with prefix
        """

    @abstractmethod
    def list_keys(
        self, path: str, recurse: RecurseType = 0
    ) -> tuple[list[str], DbRevision]:
        """
        :param path: Prefix of keys to query. Append '/' to list
           child paths.
        :param recurse: Maximum recursion level to query. If iterable,
           cover exactly the recursion levels specified.
        :returns: (sorted key list, revision)
        """

    @abstractmethod
    def close(self) -> None:
        """
        Close the client connection.
        """

    def __enter__(self) -> "Backend":
        """Use for scoping client connection to a block."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        """Use for scoping client connection to a block."""
        self.close()
        return False


class DbTransaction(ABC):
    """
    Database transaction specification.
    """

    def __init__(self, backend: Backend):
        """
        Construct a transaction.

        :param backend: to wrap
        """
        self.backend = backend

    @abstractmethod
    def commit(self) -> bool:
        """
        Commit the transaction to the database.

        This can fail, in which case the transaction must get `reset`
        and built again.

        :returns: Whether the commit succeeded
        """

    @abstractmethod
    def reset(self, revision: Optional[DbRevision] = None) -> None:
        """
        Reset the transaction, so it can be restarted after commit().

        :param revision: to reset
        :raises: RuntimeError if the transaction is not committed.
        """

    @abstractmethod
    def get(self, path: str) -> Optional[str]:
        """
        Get value of a key.

        :param path: Path of key to query
        :returns: Key value or None if it doesn't exist.
        """

    @abstractmethod
    def create(
        self,
        path: str,
        value: str,
        lease: Optional[Lease],
    ) -> None:
        """
        Create a key and initialise it with the value.

        Fails if the key already exists. If a lease is given, the key will
        automatically get deleted once it expires.

        :param path: Path to create
        :param value: Value to set
        :param lease: Lease to associate
        :raises: ConfigCollision if the key already exists
        """

    @abstractmethod
    def update(self, path: str, value: str) -> None:
        """
        Update an existing key.

        Fails if the key does not exist.

        :param path: Path to update
        :param value: Value to set
        :raises: ConfigVanished if the key is not found
        """

    @abstractmethod
    def delete(
        self,
        path: str,
        must_exist: bool = True,
        recursive: bool = False,
    ):
        """
        Delete the given key.

        :param path: Path of key to remove
        :param must_exist: Fail if path does not exist?
        :param recursive: Delete children keys at lower levels recursively
            (not used yet)
        """

    @abstractmethod
    def list_keys(self, path: str, recurse: RecurseType = 0) -> list[str]:
        """
        List keys under given path.

        :param path: Prefix of keys to query. Append '/' to list
           child paths.
        :param recurse: Children depths to include in search
        :returns: sorted key list
        """


class Watcher(ABC):
    """
    Watcher objects.
    """

    def __init__(
        self,
        backend: Backend,
        timeout: Optional[float] = None,
        txn_wrapper: TxnWrapper = None,
    ):
        """
        Create watcher.

        :param backend: backend reference
        :param timeout: timeout in seconds
        :param txn_wrapper: wrapper (factory) to return transaction
        """
        self.backend = backend
        self._txn_wrapper = txn_wrapper
        self._timeout = timeout
        self._wake_up_at = None

    @abstractmethod
    def __iter__(self) -> Iterable[Watcher]:
        """Iterator."""

    def get_txn(self, txn: DbTransaction) -> DbTransaction | TxnWrapper:
        """
        Get transaction, via wrapper if it exists.

        :param txn: input transaction
        :return: output transaction
        """
        return txn if self._txn_wrapper is None else self._txn_wrapper(txn)

    @abstractmethod
    def txn(self) -> Iterable[Transaction | TxnWrapper]:
        """
        Create a transaction.

        :return: transaction
        """

    def set_timeout(self, timeout: Optional[float]) -> None:
        """
        Set a timeout in seconds.

        The watch loop will always repeat after waiting for the given
        amount of time. Overrides the last set timeout.

        :param timeout: Maximum time to wait per loop in seconds.
                        If ``None``, will wait indefinitely.
        """
        self._timeout = timeout

    def set_wake_up_at(self, wake_up: datetime):
        """Set a time at which to wake up the watcher.

        If `wake_up_at` is set, the watch loop will never block beyond
        the given point in time, even if the timeout is longer. If
        called multiple times, the earliest wake-up time is used. This
        will be reset after every watcher iteration.

        :param wake_up: datetime object indicating when the watcher
                        should iterate at the latest

        """
        if self._wake_up_at is not None:
            self._wake_up_at = min(self._wake_up_at, wake_up)
        else:
            self._wake_up_at = wake_up

    def get_timeout(self, now: datetime = None):
        """
        Determine the timeout for the next watcher iteration.

        :param now: If given, consider the given time "now" for the
          purpose of timeout calculation. Defaults to
          ``datetime.now``.

        :returns: seconds until the watcher should unblock
        """
        if self._wake_up_at is None:
            return self._timeout

        if now is None:
            now = datetime.now()
        if self._wake_up_at < now:
            return 0.0

        alarm_timedelta = self._wake_up_at - now
        if self._timeout is not None:
            if self._timeout <= alarm_timedelta.total_seconds():
                return self._timeout

        return alarm_timedelta.total_seconds()
