from pathlib import Path, PurePath, PurePosixPath, PureWindowsPath

import pytest
from pydantic import TypeAdapter, ValidationError

from ska_sdp_config.entity.common import AbsolutePurePath, AnyPurePath
from ska_sdp_config.entity.common.path import RelativePurePath

AnyPurePath_SCHEMA = {
    "format": "path",
    "type": "string",
}


@pytest.mark.parametrize(
    "class_type,schema",
    [
        pytest.param(AnyPurePath, AnyPurePath_SCHEMA, id="AnyPurePath"),
        pytest.param(
            AbsolutePurePath, AnyPurePath_SCHEMA, id="AbsolutePurePath"
        ),
        pytest.param(
            RelativePurePath, AnyPurePath_SCHEMA, id="RelativePurePath"
        ),
    ],
)
def test_path_schema(class_type, schema):
    """
    Test that verifies the path serialization behaviour
    without checking against the filesystem.
    """
    ta: TypeAdapter[class_type] = TypeAdapter(class_type)
    assert ta.json_schema() == schema


def test_path():
    json = '"/tmp/plasma/socket"'
    ta: TypeAdapter[AnyPurePath] = TypeAdapter(AnyPurePath)
    path: AnyPurePath = ta.validate_json(json)
    assert isinstance(path, PurePath)
    assert path.anchor == "/"
    assert path.parent == Path("/tmp/plasma")
    assert path.name == "socket"
    assert path.stem == "socket"
    assert path.suffix == ""
    assert path == ta.validate_python(Path("/tmp/plasma/socket"))
    assert ta.dump_python(path, mode="json") == "/tmp/plasma/socket"
    assert json == ta.dump_json(path).decode("utf-8")


def test_absolute_path():
    adapter = TypeAdapter(AbsolutePurePath)
    p = adapter.validate_python("/path")
    assert isinstance(p, PurePath)
    assert str(p) == "/path"

    p = adapter.validate_python(PurePosixPath("/path"))
    assert isinstance(p, PurePath)
    assert str(p) == "/path"

    p = adapter.validate_python(PureWindowsPath("C:\\path"))
    assert isinstance(p, PurePath)
    assert str(p) == "C:\\path"

    with pytest.raises(ValidationError, match="is not an absolute path"):
        adapter.validate_python("path")


def test_relative_path():
    adapter = TypeAdapter(RelativePurePath)
    p = adapter.validate_python("path")
    assert isinstance(p, PurePath)
    assert str(p) == "path"

    p = adapter.validate_python(PurePosixPath("path/location"))
    assert isinstance(p, PurePath)
    assert str(p) == "path/location"

    p = adapter.validate_python(PureWindowsPath("path\\location"))
    assert isinstance(p, PurePath)
    assert str(p) == "path\\location"

    with pytest.raises(ValidationError, match="is an absolute path"):
        adapter.validate_python("/path")
