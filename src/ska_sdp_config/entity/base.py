"""Base pydantic model classes for the SDP Configuration DB."""

from typing import Annotated, ClassVar, Iterable

from pydantic import BaseModel, Field
from pydantic.config import ConfigDict


def _for_db_validation(pattern: str):
    return pattern.lstrip("^").rstrip("$")


class EntityBaseModel(BaseModel):
    """Base class for all SDP entity models."""

    model_config = ConfigDict(
        strict=True,
        arbitrary_types_allowed=False,
        validate_assignment=True,
    )


class EntityKeyBaseModel(BaseModel):
    """Base class for all SDP entity key models that aren't simple strings."""

    model_config = ConfigDict(
        strict=True,
        extra="forbid",
        arbitrary_types_allowed=False,
        validate_assignment=True,
    )

    @classmethod
    def key_patterns(cls) -> Iterable[tuple[str, str]]:
        """
        An iterator over the names and RE patterns of all this key's fields.
        Returned patterns are stripped from any leading ``^`` and trailing
        ``$`` tokens to be able to use it in the context of database key
        validation.
        """
        for field, field_info in cls.model_fields.items():
            yield field, _for_db_validation(field_info.metadata[0].pattern)


class MultiEntityBaseModel(EntityBaseModel):
    """
    Base model for entity types that can have multiple values stored in the DB.
    """

    Key: ClassVar[type[str | EntityKeyBaseModel]] = str
    """
    The model for this entity's key. For entities whose key consists of
    multiple parts, an embedded Key class deriving from EntityKeyBaseModel
    should be defined with the appropriate fields. Entities with a single part
    will want to re-define the :attr:`key` field to add more specific patterns.
    """

    key: Annotated[Key, Field(pattern=".+")]
    """
    The primary key to this entity.
    """

    @classmethod
    def key_patterns(cls) -> Iterable[tuple[str, str]]:
        """
        An iterator over the names and RE patterns of fields that make up this
        entity's key. Returned patterns are stripped from any leading ``^`` and
        trailing ``$`` tokens to be able to use it in the context of database
        key validation.
        """
        if issubclass(cls.Key, str):
            # pylint: disable=unsubscriptable-object
            return [
                (
                    "key",
                    _for_db_validation(
                        cls.model_fields["key"].metadata[0].pattern
                    ),
                )
            ]
        assert issubclass(cls.Key, EntityKeyBaseModel)
        # pylint: disable-next=no-member
        return cls.Key.key_patterns()
