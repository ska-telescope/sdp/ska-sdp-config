"""
Get/Watch all information of a single key in the Configuration Database.

Usage:
    ska-sdp (get|watch) [options] system
    ska-sdp (get|watch) [options] <key>
    ska-sdp (get|watch) [options] pb <pb-id>
    ska-sdp (get|watch) [options] eb <eb-id>
    ska-sdp (get|watch) [options] deployment <dpl-id>
    ska-sdp (get|watch) [options] flow <flow-id>
    ska-sdp (get|watch) (-h|--help)

Arguments:
    <key>       Key within the Config DB.
                To get the list of all keys:
                    ska-sdp list -a
    <pb-id>     Processing block id to list all entries and their values for.
                Else, use key to get the value of a specific pb.
    <eb-id>     Execution block id to list all entries and their values for.
                Else, use key to get the value of a specific eb.
    <dpl-id>    Deployment id to list all entries and their values for.
                Else, use key to get the value of a specific deployment.
    <flow-id>   Flow to list all entries and their values for. Expected format:
                pb-id:kind:name
                Else, use key to get the value of a specific pb.

Options:
    -h, --help    Show this screen
    -q, --quiet   Cut back on unnecessary output
"""

import json
import logging

from docopt import docopt

LOG = logging.getLogger("ska-sdp")


def cmd_get(txn, key, quiet=False):
    """
    Get raw value from database.

    :param txn: Config object transaction
    :param key: Key within the Config DB to get the values of
    :param quiet: quiet logging
    """
    value = json.loads(txn.raw.get(key))
    value_formatted = json.dumps(
        value,
        indent=4,
        ensure_ascii=False,
    )
    if quiet:
        LOG.info(value_formatted)
    else:
        LOG.info("%s = %s", key, value_formatted)


def get_or_watch(config, watch):
    """
    Generate transactions to get or watch keys.

    :param config: Config client
    :param watch: watch keys?
    """
    if watch:
        # Use watcher to monitor changes; exit on keyboard interrupt
        try:
            for watcher in config.watcher():
                yield from watcher.txn()
        except KeyboardInterrupt:
            pass
    else:
        # Use straightforward transaction
        yield from config.txn()


def get_requested_object(config, path, args, arg_key):
    """
    Get/watch value for object, checking the requested ID

    :param config: Config client
    :param path: path to object in the database
    :param args: command line arguments
    :param arg_key: CLI argument key for the object (e.g. "<pb-id>")
    """

    for txn in get_or_watch(config, args["watch"]):
        keys = txn.raw.list_keys(path, recurse=8)
        for k in keys:
            if args[arg_key] and (args[arg_key] in k):
                cmd_get(txn, k, args["--quiet"])


def main(argv, config):
    """Run ska-sdp get."""
    args = docopt(__doc__, argv=argv)

    if args["<key>"]:
        key = args["<key>"]
        if key == "pb":
            LOG.error(
                "Cannot 'get' processing block without its ID. Run "
                "'ska-sdp get pb <pb-id>'"
            )
            return

        for txn in get_or_watch(config, args["watch"]):
            try:
                cmd_get(txn, key, args["--quiet"])
            except ValueError:
                # when not the full key/path is given, Config returns a
                # ValueError
                LOG.error(
                    "'%s' is not a valid key in the Config DB. "
                    "Run 'ska-sdp list -a' to list all valid keys.",
                    key,
                )
                return
    elif args["pb"]:
        get_requested_object(config, "/pb", args, "<pb-id>")
    elif args["eb"]:
        get_requested_object(config, "/eb", args, "<eb-id>")
    elif args["deployment"]:
        get_requested_object(config, "/deploy", args, "<dpl-id>")
    elif args["flow"]:
        get_requested_object(config, "/flow", args, "<flow-id>")
    elif args["system"]:
        for txn in get_or_watch(config, args["watch"]):
            cmd_get(txn, "/system", args["--quiet"])
