"""Tango type definitions for SDP config entity models."""

from __future__ import annotations

import re
from typing import Annotated, Any, Literal

from pydantic import (
    AnyUrl,
    GetCoreSchemaHandler,
    GetJsonSchemaHandler,
    UrlConstraints,
)
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import Url, core_schema

TANGO_ATTRIBUTE_RE = re.compile(
    r"^tango://((?:[\w.-]+)"
    r"|(?:[A-Za-z0-9-.]*(?:{[a-z_]+}[A-Za-z0-9-.]*)+)+)"
    r"(:[0-9]*)?"
    r"/([\w-]+)"
    r"/([A-Za-z0-9-]+)"
    r"/([\w-]+)"
    r"(#.*)?$"
)

TangoUrl = Annotated[AnyUrl, UrlConstraints(allowed_schemes=["tango"])]

TangoDataType = tuple[str, bytes] | int | float | bool | str
"""Supported tango attribute value types."""

TangoArgType = Literal[
    "DevEncoded",
    "DevString",
    "DevBoolean",
    "DevUChar",
    "DevUShort",
    "DevULong",
    "DevULong64",
    "DevShort",
    "DevLong",
    "DevLong64",
    "DevFloat",
    "DevDouble",
]
"""String literal corresponding to a tango.ArgType value."""


class TangoAttributeUrl(Url):
    """
    Tango URL to an attribute on a device instance. Supported expressions
    are of the form:

    `tango://<device_domain>[:<port>]/<device_family>/<device_member>/<device_attribute>[#nodb=yes|no]`

    For more information see:
    https://tango-controls.readthedocs.io/en/9.2.5/manual/C-naming.html
    """  # noqa: E501 # pylint: disable=line-too-long

    def __new__(cls, url: str):
        return super().__new__(cls, url)

    @property
    def device_url(self) -> TangoUrl:
        """The device URL."""
        matches = TANGO_ATTRIBUTE_RE.match(str(self))
        assert matches is not None
        return AnyUrl(
            f"tango://{matches[1]}{matches[2] or ''}/{matches[3]}/{matches[4]}"
        )

    @property
    def family_name(self) -> str:
        """The device family part of the URL."""
        matches = TANGO_ATTRIBUTE_RE.match(str(self))
        assert matches is not None
        return f"{matches[3]}"

    @property
    def member_name(self) -> str:
        """The device member part of the URL."""
        matches = TANGO_ATTRIBUTE_RE.match(str(self))
        assert matches is not None
        return f"{matches[4]}"

    @property
    def device_name(self) -> str:
        """The device name part of the URL."""
        matches = TANGO_ATTRIBUTE_RE.match(str(self))
        assert matches is not None
        return f"{matches[1]}/{matches[3]}/{matches[4]}"

    @property
    def attribute_name(self) -> str:
        """The attribute name part of the URL."""
        matches = TANGO_ATTRIBUTE_RE.match(str(self))
        assert matches is not None
        return matches[5]

    @classmethod
    def __validate(
        cls,
        value: AnyUrl | str,
    ) -> TangoAttributeUrl:
        url = TangoAttributeUrl(str(value))

        if url.scheme not in ("tango"):
            raise AssertionError("URL scheme should be 'tango'")

        if TANGO_ATTRIBUTE_RE.match(str(value)) is None:
            raise AssertionError(
                f"String should match pattern {TANGO_ATTRIBUTE_RE.pattern}"
            )

        return url

    @classmethod
    def __get_pydantic_core_schema__(
        cls, _source_type: Any, _handler: GetCoreSchemaHandler
    ) -> core_schema.CoreSchema:
        return core_schema.no_info_plain_validator_function(
            cls.__validate,
        )

    @classmethod
    def __get_pydantic_json_schema__(
        cls,
        _core_schema: core_schema.CoreSchema,
        _handler: GetJsonSchemaHandler,
    ) -> JsonSchemaValue:
        return {
            "format": "uri",
            "minLength": 1,
            "type": "string",
            "pattern": TANGO_ATTRIBUTE_RE.pattern,
        }
