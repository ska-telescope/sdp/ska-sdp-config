"""Common type definitions for SDP config entity models."""

from typing import Annotated

from pydantic import Field


def _unique_id_pattern(prefix: str) -> str:
    return r"^" + prefix + r"\-[a-z0-9]+\-[0-9]{8}\-[a-z0-9]+$"


ProcessingBlockId = Annotated[str, Field(pattern=_unique_id_pattern("pb"))]
"""Type definition for model fields holding a Processing Block ID"""

ExecutionBlockId = Annotated[str, Field(pattern=_unique_id_pattern("eb"))]
"""Type definition for model fields holding an Execution Block ID"""
