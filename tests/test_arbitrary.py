"""High-level API tests on arbitrary DB paths."""

import logging
import random
import string

import pytest

from ska_sdp_config import Config

logger = logging.getLogger(__name__)


@pytest.fixture(scope="module", name="prefix")
def prefix_fixture():
    """Database prefix."""
    return "/__test_arbitrary_paths"


@pytest.mark.parametrize("num_path_parts", list(range(1, 6)))
def test_arbitrary_access(cfg: Config, num_path_parts: int):
    """Arbitrary paths in the DB can be operated on."""

    # Root all paths on a known entity, least we get flooded with wranings
    arbitrary_path = "/eb/" + "/".join(
        "".join(random.choices(string.ascii_lowercase, k=10))
        for _part_count in range(num_path_parts)
    )
    value = {"dummy": 1}
    updated_value = {"dummy": 2}
    logger.info("Testing arbitrary access with path %s", arbitrary_path)

    # Doesn't exist
    for txn in cfg.txn():
        assert not txn.arbitrary(arbitrary_path).exists()

    # Create and read in a single txn
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        arbitrary_entity.create(value=value)
        assert arbitrary_entity.exists()
        assert arbitrary_entity.get() == value

    # Read in a new txn
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        assert arbitrary_entity.exists()
        assert arbitrary_entity.get() == value

    # Update, see results
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        arbitrary_entity.update(value=updated_value)
        assert arbitrary_entity.get() == updated_value

    # Read update in new txn
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        assert arbitrary_entity.get() == updated_value

    # Delete and read in a single txn
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        arbitrary_entity.delete()
        assert not arbitrary_entity.exists()
        assert arbitrary_entity.get() is None

    # Read in new txn
    for txn in cfg.txn():
        arbitrary_entity = txn.arbitrary(arbitrary_path)
        assert not arbitrary_entity.exists()
        assert arbitrary_entity.get() is None


@pytest.mark.parametrize(
    "path, warns",
    (
        ("/pb", False),
        ("/pb/a", False),
        # not an actual, valid path, but our check goes so far
        ("/eb/23/12", False),
        ("/unknown", True),
    ),
)
def test_accesss_validity(
    cfg: Config,
    path: str,
    warns: bool,
    recwarn: pytest.WarningsRecorder,
):
    """Check that warnings are generated for unknown paths"""
    for txn in cfg.txn():
        assert not txn.arbitrary(path).exists()
    if warns:
        assert len(recwarn) == 1
        w = recwarn.pop()
        assert w.category == UserWarning
    else:
        assert len(recwarn) == 0
