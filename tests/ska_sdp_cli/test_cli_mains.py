"""Test the main functions of the various ska-sdp commands"""

import tempfile

# pylint: disable=too-many-arguments
from unittest.mock import Mock, call, patch

import pytest
from packaging.version import Version

from ska_sdp_config.entity import Script
from ska_sdp_config.ska_sdp_cli import (
    sdp_create,
    sdp_delete,
    sdp_get,
    sdp_import,
    sdp_list,
    sdp_update,
    ska_sdp,
)
from ska_sdp_config.ska_sdp_cli.sdp_list import cmd_list

PATH_PREFIX = "ska_sdp_config.ska_sdp_cli"
MAIN = "main"
SKA_SDP = "ska_sdp"
SDP_LIST = "sdp_list"
SDP_GET = "sdp_get"
SDP_CREATE = "sdp_create"
SDP_UPDATE = "sdp_update"
SDP_DELETE = "sdp_delete"
SDP_IMPORT = "sdp_import"

PREFIX = "/__test_cli_main"


@pytest.fixture(scope="module", name="prefix")
def prefix_fixture():
    """Database prefix."""
    return PREFIX


class MockConfig:
    # pylint: disable=too-few-public-methods
    """MockConfig object for testing"""

    def __init__(self):
        pass

    def txn(self):
        """Mock transaction"""
        return [
            Mock(
                raw=Mock(
                    list_keys=Mock(
                        return_value=[
                            "/pb/pb-id-333/state",
                            "/pb/pb-id-333/owner",
                            "/eb/eb-id-444/state",
                            "/deploy/dpl-id-555/state",
                        ]
                    )
                )
            )
        ]


@pytest.mark.parametrize(
    "command, executable",
    [
        ("list", SDP_LIST),
        ("get", SDP_GET),
        ("watch", SDP_GET),
        ("create", SDP_CREATE),
        ("update", SDP_UPDATE),
        ("edit", SDP_UPDATE),
        ("delete", SDP_DELETE),
        ("import", SDP_IMPORT),
    ],
)
@patch(f"{PATH_PREFIX}.{SKA_SDP}.config")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_LIST}.{MAIN}")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_GET}.{MAIN}")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_CREATE}.{MAIN}")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_UPDATE}.{MAIN}")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_DELETE}.{MAIN}")
@patch(f"{PATH_PREFIX}.{SKA_SDP}.{SDP_IMPORT}.{MAIN}")
# pylint: disable=too-many-positional-arguments
def test_ska_sdp_main(
    mock_sdp_import,
    mock_sdp_delete,
    mock_sdp_update,
    mock_sdp_create,
    mock_sdp_get,
    mock_sdp_list,
    mock_config,
    command,
    executable,
):
    """
    ska_sdp.main calls the correct command main function based on input argv,
    and all other commands are not called.
    """
    command_dict = {
        SDP_LIST: mock_sdp_list,
        SDP_GET: mock_sdp_get,
        SDP_CREATE: mock_sdp_create,
        SDP_UPDATE: mock_sdp_update,
        SDP_DELETE: mock_sdp_delete,
        SDP_IMPORT: mock_sdp_import,
    }

    command_dict[executable].Config().return_value = Mock()
    mock_sdp_import.return_value = Mock()

    argv = [command]

    # run tested function
    ska_sdp.main(argv)

    # the command specified in argv is called
    command_dict[executable].assert_called_with(argv, mock_config.Config())

    # none of the other commands are called
    for key, mock in command_dict.items():
        if key != executable:
            mock.assert_not_called()


@pytest.mark.parametrize(
    "options, expected_path, expected_list",
    [
        (
            "-a",
            f"{PREFIX}/",
            [
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
                call(f"{PREFIX}/pb/pb-test-20240821-00001"),
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),
        (
            "pb",
            f"{PREFIX}/pb",
            [
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
                call(f"{PREFIX}/pb/pb-test-20240821-00001"),
            ],
        ),
        (
            "pb 20240820",
            f"{PREFIX}/pb",
            [
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
            ],
        ),
        (
            "--quiet script",
            f"{PREFIX}/script",
            [
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),
        (
            "script batch",
            f"{PREFIX}/script",
            [
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),
        (
            "script realtime",
            f"{PREFIX}/script",
            [],
        ),
        ("-q eb", f"{PREFIX}/eb", []),
        ("deployment", f"{PREFIX}/deploy", []),
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_LIST}.cmd_list")
def test_sdp_list_main(
    mock_list_cmd, options, expected_path, expected_list, cfg_data
):
    """
    cmd_list is called with the correct path constructed based on input
    options.
    """
    mock_list_cmd.side_effect = cmd_list
    argv = ["list"] + options.split() + ["--prefix", PREFIX]

    with patch("logging.Logger.info") as mock_log:
        # run tested function
        sdp_list.main(argv, cfg_data)

    assert expected_list in mock_log.call_args_list

    # [0][0] --> only one call, to get the tuple of args out of the call(),
    # need to go another layer deep
    result_calls = mock_list_cmd.call_args_list[0][0]
    assert result_calls[1] == expected_path


@pytest.mark.parametrize(
    "options, expected_args, times_called",
    [
        ("my-key", "my-key", 1),
        ("pb pb-id-333", "/pb/pb-id-333/state", 2),
        ("eb eb-id-444", "/eb/eb-id-444/state", 1),
        ("deployment dpl-id-555", "/deploy/dpl-id-555/state", 1),
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_GET}.cmd_get")
def test_sdp_get_main(mock_get_cmd, options, expected_args, times_called):
    """
    cmd_get is called with the correct arguments based on input options. When a
    single key is supplied, we check that cmd_get is called with that. When pb
    is given, with its id, we check that the cmd_get is called as many times as
    keys are turned for that pb-id, and check the first key returned.
    """
    mock_get_cmd.return_value = Mock()
    config = MockConfig()
    argv = ["get"] + options.split()

    # run tested function
    sdp_get.main(argv, config)

    # [0][0] --> 1st call, to get the tuple of args out of the call(),
    # need to go another layer deep
    result_calls = mock_get_cmd.call_args_list[0][0]

    assert result_calls[1] == expected_args
    assert len(mock_get_cmd.call_args_list) == times_called


@pytest.mark.parametrize(
    "options, cmd_to_call, expected_value",
    [
        (
            "script batch:test:0.0.0 {'image':'my-script'}",
            3,
            "batch:test:0.0.0",
        ),  # expected path
        (
            "pb batch:test:0.0.0",
            2,
            Script.Key(kind="batch", name="test", version="0.0.0"),
        ),  # expected script dict
        ("deployment MyID helm '{}'", 1, "helm"),  # expected kind
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_CREATE}.cmd_create_script")
@patch(f"{PATH_PREFIX}.{SDP_CREATE}.cmd_create_pb")
@patch(f"{PATH_PREFIX}.{SDP_CREATE}.cmd_create_deploy")
# pylint: disable=too-many-positional-arguments
def test_sdp_create_main(
    mock_deploy_cmd,
    mock_create_pb_cmd,
    mock_script_cmd,
    options,
    cmd_to_call,
    expected_value,
):
    """
    Depending on what is being created, one of the commands is called, while
    the others aren't called. We also check that some of the arguments are
    correct, especially for `create pb <script>`, where the input script
    information is converted to a dict within main.
    """
    mock_deploy_cmd.return_value = Mock()
    config = MockConfig()

    cmd_map = {
        1: mock_deploy_cmd,
        2: mock_create_pb_cmd,
        3: mock_script_cmd,
    }

    argv = ["create"] + options.split()

    # run tested function
    sdp_create.main(argv, config)

    cmd_map[cmd_to_call].assert_called()
    for i, cmd in cmd_map.items():
        if i != cmd_to_call:
            cmd.assert_not_called()

    result_calls = cmd_map[cmd_to_call].call_args_list[0][0]
    assert result_calls[1] == expected_value


@pytest.mark.parametrize(
    "cmd_to_run, options, expected_key",
    [
        (
            "update",
            "--quiet script batch:test:0.0.0 new-data",
            "/script/batch:test:0.0.0",
        ),
        (
            "update",
            "pb-state pb-20201010-test new-data",
            "/pb/pb-20201010-test/state",
        ),
        ("edit", "dpl-state new-deployment", "/deploy/new-deployment/state"),
        (
            "update",
            "eb-state eb-20201010-test new-data",
            "/eb/eb-20201010-test/state",
        ),
        ("edit", "pb-state pb-20201010-test", "/pb/pb-20201010-test/state"),
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_UPDATE}.cmd_update")
@patch(f"{PATH_PREFIX}.{SDP_UPDATE}.cmd_edit")
def test_sdp_update_main(
    mock_edit_cmd, mock_update_cmd, cmd_to_run, options, expected_key
):
    """
    cmd_update, and cmd_edit are called with the correct keys.
    """
    mock_update_cmd.return_value = Mock()
    mock_edit_cmd.return_value = Mock()
    config = MockConfig()

    cmd_map = {"update": mock_update_cmd, "edit": mock_edit_cmd}

    argv = [cmd_to_run] + options.split()

    # run tested function
    sdp_update.main(argv, config)

    cmd_map[cmd_to_run].assert_called_once()
    for cmd, mock in cmd_map.items():
        if cmd != cmd_to_run:
            mock.assert_not_called()

    result_calls = cmd_map[cmd_to_run].call_args_list[0][0]
    assert result_calls[1] == expected_key


@pytest.mark.parametrize(
    "options, expected_path, log_message",
    [
        ("--all pb", "/pb", ["type", "pb"]),
        ("--all --prefix=/test pb", "/test/pb", ["prefix", "/test"]),
        ("--all --prefix=/test prefix", "/test", ["prefix", "/test"]),
        ("deployment depID", "/deploy/depID", False),
        ("eb eb-test-20210524-00000", "/eb/eb-test-20210524-00000", False),
        ("script batch:test:0.0.0", "/script/batch:test:0.0.0", False),
        ("--prefix=/test/ deployment depID", "/test/deploy/depID", False),
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_DELETE}.cmd_delete")
@patch(f"{PATH_PREFIX}.{SDP_DELETE}._get_input", Mock(return_value="yes"))
def test_sdp_delete_main(mock_delete_cmd, options, expected_path, log_message):
    """
    sdp_delete logs warning when -a / --all is set.
    Continue is always "yes" --> cmd_delete is called with correct path.
    cmd_delete is called with correct path.
    """
    mock_delete_cmd.return_value = Mock()
    config = MockConfig()
    argv = ["delete"] + options.split()

    with patch("logging.Logger.warning") as mock_log:
        # run tested function
        sdp_delete.main(argv, config)

    if log_message:
        result_log = mock_log.call_args_list[0][0]
        assert log_message[0] in result_log[0]
        assert result_log[1] == log_message[1]

    result_calls = mock_delete_cmd.call_args_list[0][0]
    assert result_calls[1] == expected_path


@pytest.mark.parametrize(
    "options, sync",
    [
        (["import", "scripts", "my-file"], False),
        (["import", "scripts", "--sync", "my-file"], True),
        (["import", "script", "my-file"], False),
        (["import", "script", "--sync", "my-file"], True),
    ],
)
@patch(f"{PATH_PREFIX}.{SDP_IMPORT}.import_scripts")
@patch(f"{PATH_PREFIX}.{SDP_IMPORT}.read_input")
@patch(f"{PATH_PREFIX}.{SDP_IMPORT}.parse_definitions")
def test_sdp_import_main(
    mock_parse_definitions, mock_read_input, mock_import_scripts, options, sync
):
    """
    sdp_import executes correctly with both --sync True and False
    """
    mock_read_input.return_value = {}
    mock_parse_definitions.return_value = {}
    mock_import_scripts.return_value = None
    config = MockConfig()
    argv = options

    # run tested function
    sdp_import.main(argv, config)

    # [0][1] --> [1] contains the optional arguments, like --sync
    result_calls = mock_import_scripts.call_args_list[0][1]
    assert result_calls["sync"] == sync


@pytest.mark.parametrize(
    "options",
    [
        ["import", "scripts", "bad.json"],
        ["import", "script", "bad.json"],
    ],
)
def test_sdp_import_main_bad_file(options):
    """
    Error logged when the file that is used doesn't exist.
    """
    config = MockConfig()
    argv = options

    with patch("logging.Logger.error") as mock_log:
        sdp_import.main(argv, config)

    mock_log.assert_called_with("Bad file name or URL. Please fix, and retry.")


@pytest.mark.parametrize(
    "options",
    [
        ["import", "scripts", "https://foobar/bad_url.json"],
        ["import", "script", "https://foobar/bad_url.json"],
    ],
)
def test_sdp_import_main_wrong_url(options):
    """
    Error logged when the URL used is not accessible.
    """
    config = MockConfig()
    argv = options

    with patch("logging.Logger.error") as mock_log:
        sdp_import.main(argv, config)

    mock_log.assert_called_with("Bad file name or URL. Please fix, and retry.")


def test_sdp_import_script_from_file(cfg):
    """Test importing scripts from file"""
    input_data = """
        scripts:
        - kind: realtime
          name: test_realtime
          version: 0.7.0
          image: artefact.skao.int/ska-sdp-script-test-realtime:0.7.0
          sdp_version: ">=0.21.0"
        - kind: batch
          name: test_batch
          version: 0.5.0
          image: artefact.skao.int/ska-sdp-script-test-batch:0.5.0
          sdp_version: ">=0.19.0, <0.22.0"
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        script_file = f"{tmp_dir}/import.yaml"
        with open(script_file, mode="w", encoding="utf-8") as file:
            file.write(input_data)

        for txn in cfg.txn():
            scripts_in_db = txn.script.list_keys()
            # start with empty db
            assert len(scripts_in_db) == 0

        args = ["import", "scripts", script_file]

        # tested function
        sdp_import.main(args, cfg)

        for txn in cfg.txn():
            scripts_in_db = txn.script.list_keys()
            # we imported two scripts
            assert len(scripts_in_db) == 2
            assert str(scripts_in_db[0]) == "batch:test_batch:0.5.0"
            assert str(scripts_in_db[1]) == "realtime:test_realtime:0.7.0"


def test_sdp_import_system_from_file(cfg):
    """Test importing system from file"""
    input_data = """
        version: "0.17.0"
        components:
            lmc-controller:
                devicename: "test-sdp/controller/0"
                image: "artefact.skao.int/ska-sdp-lmc"
                version: "0.24.0"
            lmc-subarray-01:
                devicename: "test-sdp/subarray/01"
                image: "artefact.skao.int/ska-sdp-lmc"
                version: "0.24.0"
        dependencies:
            ska-tango-base:
                repository: "https://artefact.skao.int/helm-internal"
                version: "0.4.6"
    """

    with tempfile.TemporaryDirectory() as tmp_dir:
        system_file = f"{tmp_dir}/import_system.yaml"
        with open(system_file, mode="w", encoding="utf-8") as file:
            file.write(input_data)

        for txn in cfg.txn():
            system_in_db = txn.system.get()
            # start with empty db
            assert system_in_db is None

        args = ["import", "system", system_file]

        # tested function
        sdp_import.main(args, cfg)

        for txn in cfg.txn():
            system_in_db = txn.system.get()
            # we imported two scripts
            assert system_in_db is not None
            assert str(system_in_db.version) == "0.17.0"
            assert (
                str(system_in_db.components["lmc-controller"].devicename)
                == "test-sdp/controller/0"
            )


@pytest.mark.parametrize(
    "source",
    [
        "https://gitlab.com/ska-telescope/sdp/ska-sdp-script/-/"
        "raw/master/tmdata/ska-sdp/scripts/scripts.yaml",
        "tmdata::gitlab://gitlab.com/ska-telescope/sdp/ska-sdp-script"
        "#tmdata::ska-sdp/scripts/scripts.yaml",
    ],
)
def test_sdp_import_from_url_or_tmdata(source, cfg):
    """Test importing scripts from URL or telescope model data source"""
    args = ["import", "scripts", source]

    for txn in cfg.txn():
        scripts_in_db = txn.script.list_keys()
        # start with empty db
        assert len(scripts_in_db) == 0

    # tested function
    sdp_import.main(args, cfg)

    for txn in cfg.txn():
        scripts_in_db = txn.script.list_keys()
        # we imported two scripts
        assert len(scripts_in_db) > 10

        # test a few random things that come from scripts.yaml,
        # but won't change that quickly so that this test would
        # need updating all the time; once there is a tag for the
        # link in the tmdata directory, use that and not master
        script_data = [txn.script.get(script) for script in scripts_in_db]

        pointing_data_versions = [
            data.key.version
            for data in script_data
            if "pointing-offset" in data.key.name
        ]
        assert Version(min(pointing_data_versions)) > Version("0.3.0")

        vis_receive_imgs = [
            data.image
            for data in script_data
            if data.key.name == "vis-receive"
        ]
        assert all(
            vis_receive_img.startswith(
                "artefact.skao.int/ska-sdp-script-vis-receive"
            )
            for vis_receive_img in vis_receive_imgs
        )

        spectral_img_params = [
            data.parameters
            for data in script_data
            if "spectral-line-imaging" in data.key.name
        ][0]

        if "tmdata::" in source:
            assert "description" in spectral_img_params.keys()
            assert "properties" in spectral_img_params.keys()
        else:
            # parameters are only loaded from tmdata;
            # scripts.yaml does not contain these directly
            assert spectral_img_params == {}
