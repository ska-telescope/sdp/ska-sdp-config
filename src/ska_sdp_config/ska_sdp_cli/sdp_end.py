"""
End execution block in the configuration database.
By default it sets the status to FINISHED. If the --cancel flag is set, it sets
the status to CANCELLED.

Usage:
    ska-sdp end eb <eb-id> [options]
    ska-sdp end (-h|--help)

Arguments:
<eb-id>    ID of execution block to end

Options:
    -c, --cancel  Cancel the execution block
    -h, --help    Show this screen
    -q, --quiet   Cut back on unnecessary output
"""

import logging

from docopt import docopt

LOG = logging.getLogger("ska-sdp")


def cmd_end(txn, eb_id, value):
    """
    End the execution block.

    :param txn: config object transaction
    :param eb_id: execution block ID
    :param value: status to set in the EB

    """
    eblock_state = txn.execution_block.state(eb_id).get()

    eblock_state["scan_type"] = None
    eblock_state["scan_id"] = None
    eblock_state["status"] = value

    txn.execution_block.state(eb_id).update(eblock_state)


def main(argv, config):
    """Run ska-sdp end."""
    args = docopt(__doc__, argv=argv)

    if args["eb"]:
        for txn in config.txn():
            keys = txn.raw.list_keys("/eb", recurse=8)
            for k in keys:
                if args["<eb-id>"] in k:
                    if args["--cancel"]:
                        cmd_end(txn, args["<eb-id>"], "CANCELLED")
                        LOG.info("EB %s CANCELLED", args["<eb-id>"])
                    else:
                        cmd_end(txn, args["<eb-id>"], "FINISHED")
                        LOG.info("EB %s FINISHED ", args["<eb-id>"])
