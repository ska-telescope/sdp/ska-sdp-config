"""Script model."""

from typing import Annotated

from pydantic import Field

from .base import EntityKeyBaseModel, MultiEntityBaseModel


class Script(MultiEntityBaseModel):
    """An SDP Script."""

    class Key(EntityKeyBaseModel):
        """An SDP Script primary key."""

        kind: Annotated[str, Field(pattern=r"^(realtime)|(batch)$")]
        """The kind of this script (realtime or batch)."""

        name: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_-]+$")]
        """The name of this script."""

        version: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_\.-]+$")]
        """The version of this script."""

        def __str__(self):
            return f"{self.kind}:{self.name}:{self.version}"

    key: Annotated[Key, Field(exclude=True)]

    image: Annotated[str, Field(pattern=r"^[a-zA-Z0-9_:\./-]+$")]
    """The OCI image used to launch this script."""

    parameters: Annotated[dict, Field(default_factory=dict)]
    """Parameters for the script."""

    sdp_version: Annotated[
        str | None,
        Field(
            pattern=(
                r"^(?:[><]=?|==)\d+\.\d+\.\d+"
                r"(?:,\s*(?:[><]=?|==)\d+\.\d+\.\d+)*$"
            )
        ),
    ] = None
    """The range of SDP versions this script is compatible with."""
