"""High-level API tests on processing blocks."""

import pytest

from ska_sdp_config import ConfigCollision
from ska_sdp_config.entity import PBDependency, ProcessingBlock, Script

# pylint: disable=missing-docstring,redefined-outer-name

SCRIPT = Script.Key(kind="realtime", name="test_rt_script", version="0.0.1")

TEST_PROCESSING_BLOCK_ID = "pb-test-00000000-0000"
TEST_PROCESSING_BLOCK_STATE_FINISHED = {
    "resources_available": True,
    "state": "FINISHED",
    "receive_addresses": {"1": {"1": ["0.0.0.0", 1024]}},
}


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_pb"


def test_create_pblock(cfg):
    # Create 3 processing blocks
    for txn in cfg.txn():
        pblock1_id = txn.new_processing_block_id("test")
        pblock1 = ProcessingBlock(key=pblock1_id, eb_id=None, script=SCRIPT)
        assert txn.processing_block.get(pblock1_id) is None
        txn.processing_block.create(pblock1)
        with pytest.raises(ConfigCollision):
            txn.processing_block.create(pblock1)
        assert txn.processing_block.get(pblock1_id).key == pblock1_id

        pblock2_id = txn.new_processing_block_id("test")
        pblock2 = ProcessingBlock(key=pblock2_id, eb_id=None, script=SCRIPT)
        txn.processing_block.create(pblock2)

        pblock_ids = txn.processing_block.list_keys()
        assert pblock_ids == [pblock1_id, pblock2_id]

    # Make sure that it stuck
    for txn in cfg.txn():
        pblock_ids = txn.processing_block.list_keys()
        assert pblock_ids == [pblock1_id, pblock2_id]

    # Make sure we can update them
    for txn in cfg.txn():
        pblock1.parameters["test"] = "test"
        pblock1.dependencies.append(PBDependency(pb_id=pblock2_id, kind=[]))
        txn.processing_block.update(pblock1)

    # Check that update worked
    for txn in cfg.txn():
        pblock1x = txn.processing_block.get(pblock1.key)
        assert pblock1x.eb_id is None
        assert pblock1x.parameters == pblock1.parameters
        assert pblock1x.dependencies == pblock1.dependencies


def test_take_pblock(cfg):
    script2 = dict(SCRIPT)
    script2["name"] += "-take"

    # Create another processing block
    for txn in cfg.txn():
        pblock_id = txn.new_processing_block_id("test")
        pblock = ProcessingBlock(key=pblock_id, eb_id=None, script=script2)
        txn.processing_block.create(pblock)

    try:
        for txn in cfg.txn():
            txn.processing_block.ownership(pblock_id).take()

        for txn in cfg.txn():
            assert txn.processing_block.ownership(pblock_id).get() == cfg.owner
            assert txn.processing_block.ownership(
                pblock_id
            ).is_owned_by_this_process()
    finally:
        cfg.revoke_lease()

    for txn in cfg.txn():
        assert txn.processing_block.ownership(pblock_id).get() is None
        assert not txn.processing_block.ownership(
            pblock_id
        ).is_owned_by_this_process()


def test_pblock_state(cfg):
    pblock_id = TEST_PROCESSING_BLOCK_ID
    state1 = {
        "resources_available": True,
        "state": "RUNNING",
        "receive_addresses": {"1": {"1": ["0.0.0.0", 1024]}},
    }
    state2 = TEST_PROCESSING_BLOCK_STATE_FINISHED

    # Create processing block
    for txn in cfg.txn():
        pblock = ProcessingBlock(key=pblock_id, eb_id=None, script=SCRIPT)
        txn.processing_block.create(pblock)

    # Check PBLOCK state is None
    for txn in cfg.txn():
        state_out = txn.processing_block.state(pblock_id).get()
        assert state_out is None

    # Create PBLOCK state as state1
    for txn in cfg.txn():
        txn.processing_block.state(pblock_id).create(state1)

    # Read PBLOCK state and check it matches state1
    for txn in cfg.txn():
        state_out = txn.processing_block.state(pblock_id).get()
        assert state_out == state1

    # Try to create PBLOCK state again and check it raises a collision
    # exception
    for txn in cfg.txn():
        with pytest.raises(ConfigCollision):
            txn.processing_block.state(pblock_id).create(state1)

    # Update PBLOCK state to state2
    for txn in cfg.txn():
        txn.processing_block.state(pblock_id).update(state2)

    # Read PBLOCK state and check it now matches state2
    for txn in cfg.txn():
        state_out = txn.processing_block.state(pblock_id).get()
        assert state_out == state2


def test_delete_processing_block_no_recurse(cfg):
    pblock_id = TEST_PROCESSING_BLOCK_ID

    # Create a Processing block
    for txn in cfg.txn():
        pblock = ProcessingBlock(key=pblock_id, eb_id=None, script=SCRIPT)
        txn.processing_block.create(pblock)

    # Create a Processing block state
    for txn in cfg.txn():
        txn.processing_block.state(pblock_id).create(
            TEST_PROCESSING_BLOCK_STATE_FINISHED
        )

    # check that PB and its state now exist
    for txn in cfg.txn():
        pblocks = txn.processing_block.list_keys()
        pb_state = txn.processing_block.state(pblock_id).get()
        assert pblock_id in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED

    # delete PB --> tested method
    for txn in cfg.txn():
        txn.processing_block.delete(pblock_id, recurse=False)

    # check that pb is deleted, but its state isn't
    for txn in cfg.txn():
        pblocks = txn.processing_block.list_keys()
        pb_state = txn.processing_block.state(pblock_id).get()
        assert pblock_id not in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED


def test_delete_processing_block_with_recurse(cfg):
    pblock_id = TEST_PROCESSING_BLOCK_ID

    # Create a Processing block (PB)
    for txn in cfg.txn():
        pblock = ProcessingBlock(key=pblock_id, eb_id=None, script=SCRIPT)
        txn.processing_block.create(pblock)

    # Create a Processing block state
    for txn in cfg.txn():
        txn.processing_block.state(pblock_id).create(
            TEST_PROCESSING_BLOCK_STATE_FINISHED
        )

    # check that PB and its state now exist
    for txn in cfg.txn():
        pblocks = txn.processing_block.list_keys()
        pb_state = txn.processing_block.state(pblock_id).get()
        assert pblock_id in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED

    # delete PB and its state -- tested method
    for txn in cfg.txn():
        txn.processing_block.delete(pblock_id, recurse=True)

    # check that both PB and its state are deleted
    for txn in cfg.txn():
        pblocks = txn.processing_block.list_keys()
        pb_state = txn.processing_block.state(pblock_id).get()
        assert pblock_id not in pblocks
        assert pb_state is None


if __name__ == "__main__":
    pytest.main()
