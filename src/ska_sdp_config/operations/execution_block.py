"""
Execution Block management for SDP Configuration Database.
"""

from ..entity import ExecutionBlock
from .entity_operations import CollectiveEntityOperations


# pylint: disable=too-few-public-methods
class ExecutionBlockOperations(CollectiveEntityOperations):
    """Database operations related to resource allocation management."""

    PREFIX = "/eb"
    KEY_PARTS = {"eb_id": "[a-zA-Z0-9-_]+"}
    MODEL_CLASS = ExecutionBlock
    HAS_STATE = True
