"""System config management for the SDP Configuration Database."""

from ..base_transaction import BaseTransaction
from ..entity import System
from .entity_operations import EntityOperations


class SystemOperations(EntityOperations):
    """Database operations related to system config management."""

    PATH = "/system"
    MODEL_CLASS = System

    def __init__(self, base_txn: BaseTransaction):
        super().__init__(base_txn, self.PATH)
