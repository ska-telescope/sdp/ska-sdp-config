"""
Delete a key from the Configuration Database.

Usage:
    ska-sdp delete (-a|--all) [options] (pb|script|eb|deployment|flow|prefix)
    ska-sdp delete [options] (pb|eb|deployment|flow) <item-id>
    ska-sdp delete [options] script <script>
    ska-sdp delete (-h|--help)

Arguments:
    <item-id>   ID of the processing block, deployment, execution block, or flow
    <script>    Script definition to be deleted. Expected format: kind:name:version
    prefix      Use this "SDP Object" when deleting with a non-object-specific, user-defined prefix

Options:
    -h, --help             Show this screen
    -q, --quiet            Cut back on unnecessary output
    --prefix=<prefix>      Path prefix (if other than standard Config paths, e.g. for testing)
"""  # noqa: E501 # pylint: disable=line-too-long

# pylint: disable=too-many-branches
# pylint: disable=too-many-arguments
# pylint: disable=too-many-positional-arguments

import logging

from docopt import docopt

from ska_sdp_config import Config

LOG = logging.getLogger("ska-sdp")


def cmd_delete(
    config: Config,
    path: str,
    recurse: bool = True,
    prefix: bool = False,
    must_exist: bool = False,
    quiet: bool = False,
):
    """
    Delete a key from the Config DB.

    :param config: config object, if supplied do batch delete if recurse.
    :param path: path (prefix) within the Config DB to delete
    :param recurse: if True, run recursive query of key as a prefix
    :param prefix: Delete all keys at given level with prefix
    :param must_exist: Fail if path does not exist
    :param quiet: quiet logging
    """
    depth = 8
    recurse_level = depth if recurse else 0
    if quiet:
        keys = None
    else:
        keys, _ = config.backend.list_keys(path, recurse=recurse_level)

    config.backend.delete(
        path,
        recursive=recurse,
        must_exist=must_exist,
        prefix=prefix,
        max_depth=depth,
    )

    if keys is not None:  # only true if not quiet
        for key in keys:
            LOG.info(key)
        LOG.info("Deleted above keys with prefix %s.", path)


def _get_input():
    return input("Continue? (yes, no) ")


def check_flow(config: Config, path: str, flow_id: str):
    """
    Check pb-id associated with flow

    :param config: config object, if supplied do batch delete if recurse.
    :param path: path (prefix) within the Config DB to delete
    :param flow_id: flow id
    """

    flow_pb_id = flow_id.split(":")[0]

    for txn in config.txn():
        if txn.processing_block.get(flow_pb_id) is not None:
            LOG.warning(
                "You are attempting to delete a flow associated with "
                "running processing block %s. This might break the "
                "processing script.",
                flow_pb_id,
            )
            cont = _get_input()
            if cont == "yes":
                path = f"{path}/flow/{flow_id}"
            else:
                path = None
        else:
            path = f"{path}/flow/{flow_id}"

    return path


def main(argv, config):
    """Run ska-sdp delete."""
    args = docopt(__doc__, argv=argv)

    object_dict = {
        "pb": args["pb"],
        "script": args["script"],
        "deploy": args["deployment"],
        "eb": args["eb"],
        "flow": args["flow"],
    }
    prefix = args["--prefix"]
    all_entries = args["--all"] or args["-a"]
    path = ""

    cont = False
    if prefix:
        if all_entries:
            LOG.warning(
                "You are attempting to delete all entries with prefix %s "
                "from the Configuration DB.",
                prefix,
            )
            cont = _get_input()
            if cont == "yes":
                path = prefix.rstrip("/")
            else:
                LOG.info("Aborted")
                return
        else:
            path = prefix.rstrip("/")

    for sdp_object, exists in object_dict.items():
        if exists:
            if all_entries:
                if not cont:
                    # first time checking if user wants to delete all
                    LOG.warning(
                        "You are attempting to delete all entries of type %s "
                        "from the Configuration DB.",
                        sdp_object,
                    )
                    cont = _get_input()
                    if cont == "yes":
                        path = "/" + sdp_object
                    else:
                        LOG.info("Aborted")
                        return
                else:
                    # already checked if user wants to delete all with prefix
                    path = path + "/" + sdp_object

            elif sdp_object == "script":
                path = f"{path}/script/{args['<script>']}"

            elif sdp_object == "flow":
                path = check_flow(config, path, args["<item-id>"])
                if not path:
                    LOG.info("Aborted")
                    return
            else:
                path = f"{path}/{sdp_object}/{args['<item-id>']}"

            break  # only one can be true, or none

    cmd_delete(config, path, recurse=True, quiet=args["--quiet"])
