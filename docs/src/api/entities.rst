.. _entity_api:

Entities
--------

.. contents::
   :depth: 1
   :local:

Common
^^^^^^

.. automodule:: ska_sdp_config.entity.common
   :members:

Owner
^^^^^

.. automodule:: ska_sdp_config.entity.owner
   :members:
   :exclude-members: model_config, model_fields, model_computed_fields

Execution Block
^^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.entity.eb
   :members:
   :exclude-members: model_config, model_fields, model_computed_fields
   :undoc-members:

Processing Block
^^^^^^^^^^^^^^^^

.. automodule:: ska_sdp_config.entity.pb
   :members:
   :undoc-members:
   :exclude-members: model_config, model_fields, model_computed_fields

Deployment
^^^^^^^^^^

.. automodule:: ska_sdp_config.entity.deployment
   :members:
   :undoc-members:
   :exclude-members: model_config, model_fields, model_computed_fields


Script
^^^^^^

.. automodule:: ska_sdp_config.entity.script
   :members:
   :exclude-members: model_config, model_fields, model_computed_fields
   :undoc-members:

Flow
^^^^

.. automodule:: ska_sdp_config.entity.flow
   :members:
   :exclude-members: model_config, model_fields, model_computed_fields
   :undoc-members:
