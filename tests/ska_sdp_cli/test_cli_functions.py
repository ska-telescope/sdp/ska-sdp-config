"""Tests for ska-sdp functions."""

import json
from unittest.mock import call, patch

import pytest
import yaml

from ska_sdp_config import ConfigVanished, entity
from ska_sdp_config.entity import Script
from ska_sdp_config.ska_sdp_cli.sdp_create import (
    cmd_create_deploy,
    cmd_create_eb,
    cmd_create_flow,
    cmd_create_pb,
    cmd_create_script,
)
from ska_sdp_config.ska_sdp_cli.sdp_delete import cmd_delete
from ska_sdp_config.ska_sdp_cli.sdp_end import cmd_end
from ska_sdp_config.ska_sdp_cli.sdp_get import cmd_get
from ska_sdp_config.ska_sdp_cli.sdp_import import (
    import_scripts,
    import_system,
    parse_definitions,
)
from ska_sdp_config.ska_sdp_cli.sdp_list import cmd_list
from ska_sdp_config.ska_sdp_cli.sdp_update import cmd_update

STRUCTURED_SCRIPT = {
    "about": ["SDP processing script definitions"],
    "version": {"date-time": "2021-05-14T16:00:00Z"},
    "repositories": [{"name": "nexus", "path": "some-repo/sdp-prototype"}],
    "scripts": [
        {
            "kind": "batch",
            "name": "test_batch",
            "repository": "nexus",
            "image": "script-test-batch",
            "versions": ["0.5.0"],
            "sdp_version": ">=0.19.0, <0.22.0",
        },
        {
            "kind": "realtime",
            "name": "test_realtime",
            "repository": "nexus",
            "image": "script-test-realtime",
            "versions": ["0.7.0"],
            "sdp_version": ">=0.21.0",
        },
    ],
}

FLAT_SCRIPT = {
    "scripts": [
        {
            "kind": "realtime",
            "name": "test_realtime",
            "version": "0.7.0",
            "image": "some-repo/sdp-prototype/script-test-realtime:0.7.0",
            "sdp_version": ">=0.21.0",
        },
        {
            "kind": "batch",
            "name": "test_batch",
            "version": "0.5.0",
            "image": "some-repo/sdp-prototype/script-test-batch:0.5.0",
            "sdp_version": ">=0.19.0, <0.22.0",
        },
    ]
}


FLAT_SCRIPT_MISSING_DATA = {
    "scripts": [
        # missing sdp_version and params
        # these are optional for now, so should not break code
        {
            "kind": "realtime",
            "name": "test_realtime",
            "version": "0.7.0",
            "image": "some-repo/sdp-prototype/script-test-realtime:0.7.0",
        },
        {
            "kind": "batch",
            "name": "test_batch",
            "version": "0.5.0",
            "image": "some-repo/sdp-prototype/script-test-batch:0.5.0",
        },
    ]
}

PREFIX = "/__test_cli"


@pytest.fixture(scope="module", name="prefix")
def prefix_fixture():
    """Database prefix."""
    return PREFIX


@pytest.fixture(scope="function", name="cfg_scripts")
def cfg_with_scripts(cfg):
    """Fixture for config db with scripts in it."""
    script1 = Script(
        key=Script.Key(kind="batch", name="test", version="0.0.0"),
        image="image:0.0.0",
        sdp_version="==0.1.0",
    )
    script2 = Script(
        key=Script.Key(kind="batch", name="test", version="0.0.1"),
        image="image:0.0.1",
        sdp_version="==0.1.0",
    )

    for txn in cfg.txn():
        txn.script.create(script1)
        txn.script.create(script2)

    return cfg


@pytest.mark.parametrize(
    "quiet, expected_log",
    [
        (
            False,
            [
                call(
                    "%s = %s",
                    f"{PREFIX}/my_path",
                    '{\n    "key": "MyValue"\n}',
                )
            ],
        ),
        (True, [call('{\n    "key": "MyValue"\n}')]),
    ],
)
def test_cmd_get(quiet, expected_log, cfg_data):
    """
    Correct information is logged whether the
    --quiet switch is set to True or not.
    """
    path = f"{PREFIX}/my_path"
    with patch("logging.Logger.info") as mock_log:
        for txn in cfg_data.txn():
            cmd_get(txn, path, quiet)

        assert mock_log.call_args_list == expected_log


@pytest.mark.parametrize(
    "quiet, values, expected_calls",
    [
        (
            True,
            False,
            [
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset/state"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state"
                ),
                call(f"{PREFIX}/my_path"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
                call(f"{PREFIX}/pb/pb-test-20240821-00001"),
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),  # list --all without values
        (
            True,
            True,
            [
                call(
                    '{\n    "data_model": "PointingTable",\n'
                    '    "sink": {\n        "data_dir": "/wow",\n        '
                    '"kind": "data-product",\n        "paths": []\n    },\n'
                    '    "sources": []\n}'
                ),
                call('{\n    "status": "TEST"\n}'),
                call(
                    '{\n    "data_model": "PointingTable",\n    "sink": {\n'
                    '        "format": "json",\n        "host": '
                    '"kafka://some-kafka-host:9092",\n        "kind": '
                    '"data-queue",\n        "topics": "some-kafka-topic"\n'
                    '    },\n    "sources": []\n}'
                ),
                call('{\n    "status": "TEST"\n}'),
                call('{\n    "key": "MyValue"\n}'),
                call(
                    '{\n    "dependencies": [],\n    "eb_id": null,\n    '
                    '"key": "pb-test-20240820-00000",\n    '
                    '"parameters": {},\n    '
                    '"script": {\n        "kind": "batch",\n        '
                    '"name": "test",\n        "version": "0.0.1"\n    }\n}'
                ),
                call("{}"),
                call(
                    '{\n    "dependencies": [],\n    '
                    '"eb_id": "eb-test-20240821-00001",\n    '
                    '"key": "pb-test-20240821-00001",\n    '
                    '"parameters": {},\n    '
                    '"script": {\n        "kind": "batch",\n        '
                    '"name": "test",\n        "version": "0.0.2"\n    }\n}'
                ),
                call(
                    '{\n    "image": "image1",\n    "parameters": {}'
                    ',\n    "sdp_version": null\n}'
                ),
                call(
                    '{\n    "image": "image2",\n    "parameters": {\n        "'
                    'test": "test"\n    },\n    "sdp_version": null\n}'
                ),
            ],
        ),  # list --all with values
        (
            False,
            False,
            [
                call(f"Keys with prefix {PREFIX}/: "),
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset/state"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state"
                ),
                call(f"{PREFIX}/my_path"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
                call(f"{PREFIX}/pb/pb-test-20240821-00001"),
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),
    ],
)
def test_cmd_list_all(quiet, values, expected_calls, cfg_data):
    """
    cmd_list correctly lists all of the contents of the Config DB.
    """
    # ska-sdp uses -R=True, and is not changeable there, so we only test that
    # here -R=False doesn't behave well, if we want -R=False, that will need to
    # be added and tested separately
    args = {
        "-R": True,
        "--quiet": quiet,
        "--values": values,
        "pb": None,
        "script": None,
        "flow": None,
        "<date>": None,
        "<kind>": None,
        "<pb-id>": None,
    }

    path = f"{PREFIX}/"

    with patch("logging.Logger.info") as mock_log:
        for txn in cfg_data.txn():
            cmd_list(txn, path, args)

        assert mock_log.call_args_list == expected_calls


@pytest.mark.parametrize(
    "quiet, values, pb_date, expected_calls",
    [
        (
            True,
            False,
            "20240820",
            [
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
            ],
        ),  # pb for date in Config DB
        (True, False, "20210102", []),  # pb for date not in Config DB
        (
            True,
            False,
            None,
            [
                call(f"{PREFIX}/pb/pb-test-20240820-00000"),
                call(f"{PREFIX}/pb/pb-test-20240820-00000/state"),
                call(f"{PREFIX}/pb/pb-test-20240821-00001"),
            ],
        ),  # not searching specific pb
        (
            False,
            True,
            "20240821",
            [
                call("Processing blocks for date 20240821: "),
                call(
                    "%s = %s",
                    f"{PREFIX}/pb/pb-test-20240821-00001",
                    '{\n    "dependencies": [],\n    '
                    '"eb_id": "eb-test-20240821-00001",\n    '
                    '"key": "pb-test-20240821-00001",\n    '
                    '"parameters": {},\n    '
                    '"script": {\n        "kind": "batch",\n       '
                    ' "name": "test",\n        "version": "0.0.2"\n    }\n}',
                ),
            ],
        ),  # pb for date with values
    ],
)
def test_cmd_list_pb(quiet, values, pb_date, expected_calls, cfg_data):
    """
    cmd_list correctly lists processing block (pb) related content.
    """
    # ska-sdp uses -R=True, and is not changeable there, so we only test that
    # here -R=False doesn't behave well, if we want -R=False, that will need to
    # be added and tested separately
    args = {
        "-R": True,
        "--quiet": quiet,
        "--values": values,
        "pb": True,
        "script": None,
        "flow": None,
        "<date>": pb_date,
        "<kind>": None,
        "<pb-id>": None,
    }
    path = f"{PREFIX}/pb"

    with patch("logging.Logger.info") as mock_log:
        for txn in cfg_data.txn():
            cmd_list(txn, path, args)

        assert mock_log.call_args_list == expected_calls


@pytest.mark.parametrize(
    "quiet, values, kind, expected_calls",
    [
        (
            True,
            False,
            "batch",
            [
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),  # script with kind in db
        (True, False, "relatime", []),  # script with kind not in db
        (
            True,
            False,
            None,
            [
                call(f"{PREFIX}/script/batch:test:0.0.1"),
                call(f"{PREFIX}/script/batch:test:0.0.2"),
            ],
        ),  # not searching specific script
        (
            False,
            True,
            "batch",
            [
                call("Script definitions of kind batch: "),
                call(
                    "%s = %s",
                    f"{PREFIX}/script/batch:test:0.0.1",
                    '{\n    "image": "image1",\n    "parameters'
                    '": {},\n    "sdp_version": null\n}',
                ),
                call(
                    "%s = %s",
                    f"{PREFIX}/script/batch:test:0.0.2",
                    '{\n    "image": "image2",\n    "parameters": {\n        "'
                    'test": "test"\n    },\n    "sdp_version": null\n}',
                ),
            ],
        ),  # pb for date with values
    ],
)
def test_cmd_list_script(quiet, values, kind, expected_calls, cfg_data):
    """
    cmd_list correctly lists script definition related content.
    """
    # ska-sdp uses -R=True, and is not changeable there, so we only test that
    # here -R=False doesn't behave well, if we want -R=False, that will need to
    # be added and tested separately
    args = {
        "-R": True,
        "--quiet": quiet,
        "--values": values,
        "pb": None,
        "script": True,
        "flow": None,
        "<date>": None,
        "<kind>": kind,
        "<pb-id>": None,
    }
    path = f"{PREFIX}/script"

    with patch("logging.Logger.info") as mock_log:
        for txn in cfg_data.txn():
            cmd_list(txn, path, args)

        assert mock_log.call_args_list == expected_calls


@pytest.mark.parametrize(
    "quiet, values, pb_id, expected_calls",
    [
        (
            True,
            False,
            "pb-test-20240820-00000",
            [
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset/state"
                ),
            ],
        ),  # first PB
        (
            True,
            False,
            "pb-test-20240821-00001",
            [
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state"
                ),
            ],
        ),  # second PB
        (True, False, "pb-test-20240820-00001", []),  # PB doesn't exist
        (
            True,
            False,
            None,
            [
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240820-00000:"
                    "data-product:pointing-offset/state"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset"
                ),
                call(
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state"
                ),
            ],
        ),  # not searching specific flow
        (
            False,
            True,
            "pb-test-20240821-00001:data-queue:pointing-offset/state",
            [
                call(
                    "Flows for processing block pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state: ",
                ),
                call(
                    "%s = %s",
                    f"{PREFIX}/flow/pb-test-20240821-00001:"
                    "data-queue:pointing-offset/state",
                    '{\n    "status": "TEST"\n}',
                ),
            ],
        ),  # pb for date with values
    ],
)
def test_cmd_list_flow(quiet, values, pb_id, expected_calls, cfg_data):
    """
    cmd_list correctly lists flow definition related content.
    """
    # ska-sdp uses -R=True, and is not changeable there, so we only test that
    # here -R=False doesn't behave well, if we want -R=False, that will need to
    # be added and tested separately
    args = {
        "-R": True,
        "--quiet": quiet,
        "--values": values,
        "pb": None,
        "script": None,
        "flow": True,
        "<date>": None,
        "<kind>": None,
        "<pb-id>": pb_id,
    }
    path = f"{PREFIX}/flow"

    with patch("logging.Logger.info") as mock_log:
        for txn in cfg_data.txn():
            cmd_list(txn, path, args)

        assert mock_log.call_args_list == expected_calls


def test_cmd_create_pb(cfg_data):
    """
    cmd_create_pb correctly creates a processing block with the supplied
    script information.
    """
    script = entity.Script.Key(kind="batch", name="my-script", version="0.1.1")
    parameters = '{"param1": "my_param"}'
    eb_params = "{}"

    for txn in cfg_data.txn():
        pb_id = cmd_create_pb(txn, script, parameters, eb_params)

    for txn in cfg_data.txn():
        pb = txn.processing_block.get(pb_id)
        assert pb.script == script
        assert pb.parameters == json.loads(parameters)


def test_cmd_create_realtime_pb(cfg_data):
    """
    cmd_create_pb correctly creates a realtime processing block processing
    block.
    """
    script = entity.Script.Key(
        kind="realtime", name="my-realtime-script", version="0.3.3"
    )
    parameters = None
    eb_params = """{
    "key":"eb-test-20210630-00001",
    "scan_types": [
        {
            "scan_type_id": "science_A",
            "reference_frame": "ICRS",
            "ra": "02:42:40.771",
            "dec": "-00:00:47.84",
            "channels": [
                {"count": 5, "start": 0, "stride": 2,
                 "freq_min": 0.35e9, "freq_max": 0.368e9,
                 "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]]},
            ]
        },
        {
            "scan_type_id": "calibration_B",
            "reference_frame": "ICRS",
            "ra": "12:29:06.699",
            "dec": "02:03:08.598",
            "channels": [
                {"count": 5, "start": 0, "stride": 2,
                 "freq_min": 0.35e9, "freq_max": 0.368e9,
                 "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]]},
            ]
        }
    ]
    }"""

    for txn in cfg_data.txn():
        pb_id = cmd_create_pb(txn, script, parameters, eb_params)

    eb_pars = yaml.safe_load(eb_params)
    for txn in cfg_data.txn():
        pb = txn.processing_block.get(pb_id)
        assert pb.script == script
        eb_id = pb.eb_id
        assert eb_id == "eb-test-20210630-00001"
        eblock = txn.execution_block.get(eb_id)
        assert eblock.scan_types == eb_pars["scan_types"]


def test_cmd_create_deploy(cfg_data):
    """
    cmd_create_deploy correctly creates a new deployment.
    """
    kind = "helm"
    deploy_id = "myDeployment"
    parameters = '{"chart": "my-helm-chart", "values": {}}'
    expected_key = f"{PREFIX}/deploy/{deploy_id}"

    for txn in cfg_data.txn():
        cmd_create_deploy(txn, kind, deploy_id, parameters)

        result_keys = txn.raw.list_keys(f"{PREFIX}/", recurse=8)
        assert expected_key in result_keys

        depl = txn.raw.get(expected_key)
        result_depl = json.loads(depl)
        assert result_depl["key"] == deploy_id
        assert result_depl["args"] == json.loads(parameters)
        assert result_depl["kind"] == kind


@pytest.mark.parametrize(
    "script_value, expected_data",
    [
        (
            '{"image": "script-test:0.5.0", '
            '"sdp_version":">=0.19.0, <0.22.0"}',
            {
                "image": "script-test:0.5.0",
                "sdp_version": ">=0.19.0, <0.22.0",
                "parameters": {},
            },
        ),
        (
            '{"image": "script-test:0.5.0"}',
            {
                "image": "script-test:0.5.0",
                "parameters": {},
                "sdp_version": None,
            },
        ),
        (
            '{"image": "script-test:0.5.0", '
            '"parameters": {"chart": "my-helm", "nworkers": 1}}',
            {
                "image": "script-test:0.5.0",
                "parameters": {"chart": "my-helm", "nworkers": 1},
                "sdp_version": None,
            },
        ),
        (
            '{"image": "script-test:0.5.0", '
            '"parameters": {"chart": "my-helm", "nworkers": 1}, '
            '"some_random_entry": "should not be in config db"}',
            {
                "image": "script-test:0.5.0",
                "parameters": {"chart": "my-helm", "nworkers": 1},
                "sdp_version": None,
            },
        ),
    ],
)
def test_cmd_create_script(cfg_data, script_value, expected_data):
    """
    Unit test for cmd_create_script
    """
    script_key_parts = "batch:my_batch_script:0.5.0"
    expected_key = entity.Script.Key(
        kind="batch", name="my_batch_script", version="0.5.0"
    )

    for txn in cfg_data.txn():
        cmd_create_script(txn, script_key_parts, script_value)

        result_keys = txn.script.list_keys()
        assert expected_key in result_keys

        script_value = json.loads(script_value)

        depl = txn.script.get(expected_key)
        assert depl.image == "script-test:0.5.0"
        assert depl.model_dump() == expected_data


def test_cmd_create_flow(cfg_data):
    """
    cmd_create_flow correctly creates a new flow.
    """
    params = """{
    "key": {"pb_id":"pb-test-20250117-00000",
        "kind":"data-product",
        "name":"pointing-offset",
    },
    "sink": {
        "kind": data-product,
        "data_dir": "/wow",
        "paths": []
    },
    "sources": [],
    "data_model":"PointingTable",
    }"""

    expected_key = entity.flow.Flow.Key(
        pb_id="pb-test-20250117-00000",
        kind="data-product",
        name="pointing-offset",
    )

    for txn in cfg_data.txn():
        cmd_create_flow(txn, params)

        result_flow = txn.flow.get(expected_key)
        assert result_flow.sink.kind == "data-product"
        assert result_flow.data_model == "PointingTable"


def test_cmd_end_finished(cfg_data):
    """cmd_end end/finish execution block."""

    status = "FINISHED"
    pb_id = "pb-mvp01-20200619-00000"
    eb_pars = f"{{pb_batch:[{pb_id}]}}"

    for txn in cfg_data.txn():
        eb_id = cmd_create_eb(txn, eb_pars, pb_id)

    for txn in cfg_data.txn():
        cmd_end(txn, eb_id, status)

    for txn in cfg_data.txn():
        result = txn.execution_block.state(eb_id).get()
        assert result["status"] == status


def test_cmd_end_cancelled(cfg_data):
    """cmd_end cancel execution block."""

    eb_pars = "{}"
    status = "CANCELLED"
    pb_id = "pb-mvp01-20200619-00000"

    for txn in cfg_data.txn():
        eb_id = cmd_create_eb(txn, eb_pars, pb_id)

    for txn in cfg_data.txn():
        cmd_end(txn, eb_id, status)

    for txn in cfg_data.txn():
        result = txn.execution_block.state(eb_id).get()
        assert result["status"] == status


def test_cmd_update(cfg_data):
    """
    cmd_update updates the key with the given value.
    """
    path = "/my_path"

    for txn in cfg_data.txn():
        assert txn.arbitrary(path).get() == {"key": "MyValue"}

    for txn in cfg_data.txn():
        cmd_update(txn, f"{PREFIX}{path}", '{"key": "new_value"}')

    for txn in cfg_data.txn():
        assert txn.arbitrary(path).get() == {"key": "new_value"}


def test_cmd_update_dictionary(cfg_data):
    """
    cmd_update updates the key with the new dictionary item.
    """
    path = f"{PREFIX}/script/batch:test:0.0.1"

    for txn in cfg_data.txn():
        assert (
            txn.raw.get(path)
            == '{"image":"image1","parameters":{},"sdp_version":null}'
        )

    for txn in cfg_data.txn():
        cmd_update(txn, path, '{"add_item": "new_item"}')

    for txn in cfg_data.txn():
        assert (
            txn.raw.get(path) == '{"image": "image1", "parameters": {}, '
            '"sdp_version": null, "add_item": "new_item"}'
        )


@pytest.mark.parametrize(
    "path, raise_err",
    [(f"{PREFIX}/my_path", False), (f"{PREFIX}/my_path/not_exist", True)],
)
def test_cmd_delete_non_recursive(path, raise_err, cfg_data):
    """
    If recursion is False, then only exact paths can be deleted.
    """
    recursion = False
    if not raise_err:
        for txn in cfg_data.txn():
            assert txn.raw.get(path) is not None

        cmd_delete(cfg_data, path, recurse=recursion)

        for txn in cfg_data.txn():
            assert txn.raw.get(path) is None

    else:
        with pytest.raises(ConfigVanished) as err:
            cmd_delete(cfg_data, path, recurse=recursion, must_exist=True)
        assert str(err.value) == f"Cannot delete {path}, as it does not exist!"

        # No error without must_exist.
        cmd_delete(cfg_data, path, recurse=recursion)


def test_cmd_delete_recursive(cfg_data):
    """
    If recursion is True, everything with the prefix is deleted.
    """
    path_prefix = f"{PREFIX}/pb"
    recursion = True

    for txn in cfg_data.txn():
        keys = txn.raw.list_keys(path_prefix, recurse=8)
        assert len(keys) == 3

    cmd_delete(cfg_data, path_prefix, recurse=recursion)

    for txn in cfg_data.txn():
        keys = txn.raw.list_keys(path_prefix, recurse=8)
        assert len(keys) == 0


def test_cmd_delete_limit(cfg_data):
    """Delete keys over the operations limit.."""
    limit = 128
    prefix = "/temp"
    repeats = 2
    depth = 8

    # A test crash can result in keys being left in the database.
    cmd_delete(cfg_data, prefix, recurse=True)

    # Make sure to create more than the limit.
    for repeat in range(repeats):
        for txn in cfg_data.txn():
            offset = repeat * limit
            for num in range(limit):
                key = f"{prefix}/{(offset + num):04}"
                txn.raw.create(key, "val")

    for txn in cfg_data.txn():
        keys = txn.raw.list_keys(prefix, recurse=depth)
        assert len(keys) == repeats * limit

    cmd_delete(cfg_data, prefix, recurse=True)

    for txn in cfg_data.txn():
        keys = txn.raw.list_keys(prefix, recurse=depth)
        assert len(keys) == 0


@pytest.mark.parametrize(
    "script_def", [STRUCTURED_SCRIPT, FLAT_SCRIPT, FLAT_SCRIPT_MISSING_DATA]
)
def test_parse_definitions_for_import(script_def):
    """
    Parse script definitions from structured and from flat dictionaries.
    """
    result = parse_definitions(script_def)
    expected_keys = [
        ("batch", "test_batch", "0.5.0"),
        ("realtime", "test_realtime", "0.7.0"),
    ]
    expected_values = [
        "some-repo/sdp-prototype/script-test-batch:0.5.0",
        "some-repo/sdp-prototype/script-test-realtime:0.7.0",
    ]

    assert len(result) == 2
    assert sorted(list(result.keys())) == sorted(expected_keys)
    assert list(result.values())[0]["image"] in expected_values
    assert list(result.values())[1]["image"] in expected_values


def test_import_scripts(cfg_scripts):
    """
    Test that sdp_import correctly adds, updates,
    and deletes scripts, from input dictionary.
    """
    schema = {
        "title": "Schema",
        "type": "object",
        "properties": {
            "param1": {"type": "string", "description": "test param"}
        },
    }
    # scripts to be imported
    scripts = {
        ("batch", "test", "0.0.0"): {
            "image": "batch-test:0.0.0",
            "sdp_version": "==0.1.0",
            "parameters": schema,
        },  # to update
        ("realtime", "test", "0.1.0"): {
            "image": "realtime-test:0.1.0",
        },  # to be inserted
    }

    # double check that keys are there and value is as created above,
    # then import scripts from dict
    for txn in cfg_scripts.txn():
        scripts_in_db = txn.script.list_keys()

        # two scripts originally in db added by cfg_scripts
        assert len(scripts_in_db) == 2
        assert str(scripts_in_db[0]) == "batch:test:0.0.0"
        assert str(scripts_in_db[1]) == "batch:test:0.0.1"
        script1 = txn.script.get(scripts_in_db[0])
        assert script1.image == "image:0.0.0"
        assert script1.parameters == {}
        assert txn.script.get(scripts_in_db[0]).image == "image:0.0.0"
        assert txn.script.get(scripts_in_db[1]).image == "image:0.0.1"

    # tested function
    for txn in cfg_scripts.txn():
        import_scripts(txn, scripts, sync=True)

    # test that one key is correctly updated, one removed, and one added
    for txn in cfg_scripts.txn():
        res_scripts = txn.script.list_keys()

        assert len(res_scripts) == 2
        assert str(res_scripts[0]) == "batch:test:0.0.0"
        assert str(res_scripts[1]) == "realtime:test:0.1.0"

        result_script1 = txn.script.get(res_scripts[0])
        assert result_script1.image == "batch-test:0.0.0"
        assert result_script1.parameters == schema
        assert txn.script.get(res_scripts[1]).image == "realtime-test:0.1.0"


def test_import_system(cfg):
    """Test import_system function."""
    system = {
        "version": "0.17.0",
        "components": {
            "lmc-controller": {
                "devicename": "test-sdp/controller/0",
                "image": "artefact.skao.int/ska-sdp-lmc",
                "version": "0.24.0",
            },
            "lmc-subarray-01": {
                "devicename": "test-sdp/subarray/01",
                "image": "artefact.skao.int/ska-sdp-lmc",
                "version": "0.24.0",
            },
            "lmc-queueconnector-01": {
                "devicename": "test-sdp/queueconnector/01",
                "image": "artefact.skao.int/ska-sdp-lmc-queue-connector",
                "version": "2.0.0",
            },
            "console": {
                "image": "artefact.skao.int/ska-sdp-console",
                "version": "0.5.1",
            },
        },
        "dependencies": {
            "ska-tango-base": {
                "repository": "https://artefact.skao.int/helm-internal",
                "version": "0.4.6",
            },
        },
    }

    # tested function
    for txn in cfg.txn():
        import_system(txn, system)

    # double check that keys are there and value is as created above,
    # then import system from dict

    for txn in cfg.txn():
        system_in_db = txn.system.get()
    assert system_in_db.version == "0.17.0"
    assert system_in_db.components["lmc-controller"].version == "0.24.0"
    assert (
        system_in_db.components["lmc-subarray-01"].image
        == "artefact.skao.int/ska-sdp-lmc"
    )
    assert system_in_db.dependencies["ska-tango-base"].version == "0.4.6"

    system["version"] = "0.18.0"
    for txn in cfg.txn():
        import_system(txn, system)
    for txn in cfg.txn():
        system_in_db = txn.system.get()
    assert system_in_db.version == "0.18.0"
    assert system_in_db.components["lmc-controller"].version == "0.24.0"
