"""High-level API tests on system config."""

import pytest
from pydantic import ValidationError
from pydantic_core import Url

from ska_sdp_config import Config
from ska_sdp_config.entity.system import (
    System,
    SystemComponent,
    SystemDependency,
)

SYSTEM = System(
    version="1.0.0",
    components={
        "etcd": SystemComponent(
            devicename="dev/ice/name",
            image="artefact.skao.int/ska-sdp-etcd",
            version="3.5.12",
        )
    },
    dependencies={
        "ska-sdp-qa": SystemDependency(
            repository="https://artefact.skao.int/repository/helm-internal",
            version="0.26.0",
        )
    },
)


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_system"


def test_create_system(cfg: Config):
    """Test system config model."""

    for txn in cfg.txn():

        txn.system.create(SYSTEM)

        assert txn.system.get() == SYSTEM


# pylint: disable=unused-variable
def test_invalid_version():
    """Test invalid version format."""

    with pytest.raises(ValidationError):
        system_noversion = System(  # noqa: F841
            version="",
            components=SYSTEM.components,
            dependencies=SYSTEM.dependencies,
        )


# pylint: disable=unused-variable
def test_invalid_component():
    """Test invalid component image format."""

    components = {"etcd": {"image": "invalid image", "version": "3.5.12"}}

    with pytest.raises(ValidationError):
        system_invalid_comp = System(  # noqa: F841
            version=SYSTEM.version,
            components=components,
            dependencies=SYSTEM.dependencies,
        )


@pytest.mark.parametrize(
    "repository, expected_repo",
    [
        ("https://some-repo.co", Url("https://some-repo.co")),
        (
            "file://some/path/to/repository",
            Url("file://some/path/to/repository"),
        ),
        ("alias:some-alias", "alias:some-alias"),
        ("@some-other-alias", "@some-other-alias"),
    ],
)
def test_allowed_dependency_repos(repository, expected_repo):
    """
    Test allowed dependency repository URL.
    These are allowed repository URLs by Helm:
    https://helm.sh/docs/helm/helm_dependency/#synopsis
    """
    dependencies = {
        "ska-sdp-qa": {
            "repository": repository,
            "version": "0.26.0",
        }
    }
    result = System(  # noqa: F841
        version=SYSTEM.version,
        components=SYSTEM.components,
        dependencies=dependencies,
    )
    assert result.dependencies["ska-sdp-qa"].repository == expected_repo


@pytest.mark.parametrize(
    "invalid_repo", ["http:invalid_repository_url", "alias:", "@"]
)
def test_invalid_dependency(invalid_repo):
    """Test invalid dependency repository URL."""

    dependencies = {
        "ska-sdp-qa": {
            "repository": invalid_repo,
            "version": "0.26.0",
        }
    }
    with pytest.raises(ValidationError):
        System(  # noqa: F841
            version=SYSTEM.version,
            components=SYSTEM.components,
            dependencies=dependencies,
        )
