"""High-level API tests on execution blocks."""

import pytest

import ska_sdp_config
from ska_sdp_config.entity import ExecutionBlock

EB_STATE_INITIAL = {
    "scan_type": None,
    "scan_id": None,
    "scans": [],
    "status": "ACTIVE",
}

EB_STATE_FINISHED = {
    "scan_type": "yyy",
    "scan_id": 3,
    "scans": [
        {"scan_id": 1, "scan_type": "xxx", "status": "ABORTED"},
        {"scan_id": 2, "scan_type": "xxx", "status": "FINISHED"},
        {"scan_id": 3, "scan_type": "yyy", "status": "FINISHED"},
    ],
    "status": "FINISHED",
}


@pytest.fixture(scope="module")
def prefix():
    """Database prefix."""
    return "/__test_eb"


def test_eb_list(cfg):
    """Test EB list"""
    # Check execution block list is empty
    for txn in cfg.txn():
        assert txn.execution_block.list_keys() == []

    # Create first execution block
    for txn in cfg.txn():
        eb1_id = txn.new_execution_block_id("test")
        txn.execution_block.create(ExecutionBlock(key=eb1_id))

    # Check execution block list has entry
    for txn in cfg.txn():
        assert txn.execution_block.list_keys() == [eb1_id]

    # Create second execution block
    for txn in cfg.txn():
        eb2_id = txn.new_execution_block_id("test")
        txn.execution_block.create(ExecutionBlock(key=eb2_id))

    # Check execution block list has both entries
    for txn in cfg.txn():
        assert txn.execution_block.list_keys() == sorted([eb1_id, eb2_id])


def test_eb_create_update(cfg):
    """Test create/update EB (not including state)"""
    for txn in cfg.txn():
        eb_id = txn.new_execution_block_id("test")

    # Execution block has not been created, so should return None
    for txn in cfg.txn():
        assert txn.execution_block.get(eb_id) is None

    # Create execution block with default (empty) parameters
    eblock = ExecutionBlock(key=eb_id)
    for txn in cfg.txn():
        txn.execution_block.create(eblock)

    # Check execution block list has entry
    for txn in cfg.txn():
        assert txn.execution_block.list_keys() == [eb_id]

    # Check key has been updated correctly
    for txn in cfg.txn():
        assert txn.execution_block.get(eb_id).key == eb_id

    # Confirm EB exists using different calling methods
    for txn in cfg.txn():
        assert txn.execution_block.exists(eblock)
        assert txn.execution_block.exists(eb_id)

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.execution_block.create(eblock)

    eb_params = {
        "pb_realtime": [
            "pb-realtime-20200213-0001",
            "pb-realtime-20200213-0002",
        ],
        "pb_batch": ["pb-batch-20200213-0001", "pb-batch-20200213-0002"],
    }

    # Update execution block parameters in the EB
    for txn in cfg.txn():
        eblock = txn.execution_block.get(eb_id)
        eblock.pb_realtime = eb_params["pb_realtime"]
        eblock.pb_batch = eb_params["pb_batch"]
        txn.execution_block.update(eblock)

    # Read execution block attributes and check they are equal to eb_params
    for txn in cfg.txn():
        model_out = txn.execution_block.get(eb_id).model_dump()
        for key, value in eb_params.items():
            assert model_out[key] == value


def test_eb_delete_no_recurse(cfg):
    """Test delete EB"""
    eb_id = "eb-test-00000000-00000"

    eblock = ExecutionBlock(key=eb_id)

    # Create a Execution block (EB) and its state
    for txn in cfg.txn():
        txn.execution_block.create(eblock)
        txn.execution_block.state(eb_id).create(EB_STATE_FINISHED)

    # check that EB and state now exists
    for txn in cfg.txn():
        assert eb_id in txn.execution_block.list_keys()
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out == EB_STATE_FINISHED

    # delete EB (no recurse) -- tested method
    for txn in cfg.txn():
        txn.execution_block.delete(eb_id, recurse=False)

    # check that EB is deleted but state is not
    for txn in cfg.txn():
        assert eb_id not in txn.execution_block.list_keys()
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out == EB_STATE_FINISHED


def test_eb_delete_with_recurse(cfg):
    """Test delete EB and state"""
    eb_id = "eb-test-00000000-00000"

    eblock = ExecutionBlock(key=eb_id)

    # Create a Execution block (EB) and its state
    for txn in cfg.txn():
        txn.execution_block.create(eblock)
        txn.execution_block.state(eb_id).create(EB_STATE_FINISHED)

    # check that EB and state now exists
    for txn in cfg.txn():
        assert eb_id in txn.execution_block.list_keys()
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out == EB_STATE_FINISHED

    # delete EB -- tested method
    for txn in cfg.txn():
        txn.execution_block.delete(eb_id, recurse=True)

    # check that EB and state are deleted
    for txn in cfg.txn():
        assert eb_id not in txn.execution_block.list_keys()
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out is None


def test_eblock_create_update_state(cfg):
    """Test create/update state"""
    # Create execution block
    for txn in cfg.txn():
        eb_id = txn.new_execution_block_id("test")
        eblock = ExecutionBlock(key=eb_id)
        txn.execution_block.create(eblock)

    # Check state is None
    for txn in cfg.txn():
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out is None

    # Create initial state
    for txn in cfg.txn():
        txn.execution_block.state(eb_id).create(EB_STATE_INITIAL)

    # Read state and check it matches expected one
    for txn in cfg.txn():
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out == EB_STATE_INITIAL

    # Try to create state again and check it raises a collision
    # exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.execution_block.state(eb_id).create(EB_STATE_INITIAL)

    # Update to finished state
    for txn in cfg.txn():
        txn.execution_block.state(eb_id).update(EB_STATE_FINISHED)

    # Read state and check it now matches expected one
    for txn in cfg.txn():
        state_out = txn.execution_block.state(eb_id).get()
        assert state_out == EB_STATE_FINISHED


if __name__ == "__main__":
    pytest.main()
