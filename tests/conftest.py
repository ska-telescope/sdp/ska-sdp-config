"""General test fixtures."""

import pytest

import ska_sdp_config


@pytest.fixture(name="owned_entity")
def owned_entity_fixture() -> None:
    """The owned_entity argument used by the cfg fixture"""
    return None


@pytest.fixture(scope="function")
def cfg(prefix: str, owned_entity: tuple[str, str] | None):
    """
    Fixture to set up configuration.

    :param prefix: for database entries
    """
    with ska_sdp_config.Config(
        global_prefix=prefix, owned_entity=owned_entity
    ) as config:
        config.backend.delete(prefix, must_exist=False, recursive=True)
        yield config
        config.backend.delete(prefix, must_exist=False, recursive=True)
