import itertools
from typing import Annotated

import pytest
from pydantic import Field

from ska_sdp_config import ConfigCollision, ConfigVanished
from ska_sdp_config.entity.base import EntityKeyBaseModel, MultiEntityBaseModel
from ska_sdp_config.entity.common.id import _unique_id_pattern
from ska_sdp_config.operations.entity_operations import (
    CollectiveEntityOperations,
)


# Custom model and operations used only by this test
# with Key of complex type
class _ExampleEntity(MultiEntityBaseModel):
    class Key(EntityKeyBaseModel):
        number: Annotated[str, Field(pattern="[0-9]+")]
        alpha: Annotated[str, Field(pattern="[a-zA-Z0-9_-]+")]

    key: Key
    data: int


SAMPLE_ENTITY_KEY = _ExampleEntity.Key(number="0", alpha="a")
SAMPLE_ENTITY = _ExampleEntity(key=SAMPLE_ENTITY_KEY, data=0)


class _ExampleEntityOperations(CollectiveEntityOperations):
    MODEL_CLASS = _ExampleEntity
    PREFIX = "/example"
    HAS_OWNER = True
    HAS_STATE = True


# Custom model with Key of type string
class _ExampleEntityStrKey(MultiEntityBaseModel):
    key: Annotated[str, Field(pattern=_unique_id_pattern("example"))]
    data: int


SAMPLE_ENTITY_STR_KEY = "example-test-20240101-12345"
SAMPLE_STR_KEY_ENTITY = _ExampleEntityStrKey(key=SAMPLE_ENTITY_STR_KEY, data=1)


class _ExampleEntityStrKeyOperations(CollectiveEntityOperations):
    MODEL_CLASS = _ExampleEntityStrKey
    PREFIX = "/example"
    HAS_OWNER = True
    HAS_STATE = True


@pytest.fixture(name="prefix")
def _prefix_fixture() -> str:
    return "/__test_operations"


def _create_example_model_ops(txn):
    # pylint: disable-next=protected-access
    base_txn = txn.script._txn
    return _ExampleEntityOperations(base_txn)


def _create_example_model_ops_str_key(txn):
    # pylint: disable-next=protected-access
    base_txn = txn.script._txn
    return _ExampleEntityStrKeyOperations(base_txn)


@pytest.fixture(name="ops")
def _example_entity_ops_fixture(cfg):
    for txn in cfg.txn():
        yield _create_example_model_ops(txn)


@pytest.fixture(name="ops_str_key")
def _example_entity_str_key_ops_fixture(cfg):
    for txn in cfg.txn():
        yield _create_example_model_ops_str_key(txn)


@pytest.mark.parametrize("invalid_value", ({}, [], 2, set()))
def test_invalid_value_types(ops: _ExampleEntityOperations, invalid_value):
    with pytest.raises(ValueError, match="not an instance of _ExampleEntity"):
        ops.create(invalid_value)


@pytest.mark.parametrize(
    "ops_type, original, updated",
    [
        (
            "ops",
            _ExampleEntity(key=SAMPLE_ENTITY_KEY, data=0),
            _ExampleEntity(key=SAMPLE_ENTITY_KEY, data=1),
        ),
        (
            "ops_str_key",
            _ExampleEntityStrKey(key=SAMPLE_ENTITY_STR_KEY, data=0),
            _ExampleEntityStrKey(key=SAMPLE_ENTITY_STR_KEY, data=1),
        ),
    ],
)
def test_entity_crud_ops(ops_type, original, updated, request):
    """Create/read/update/delete work as expected."""
    entity_ops = request.getfixturevalue(ops_type)
    assert original.key == updated.key

    # Ee can't index with __call__ though
    with pytest.raises(
        NotImplementedError, match="Only available.*plain dictionaries"
    ):
        entity_ops(original)
    if entity_ops == "ops":
        with pytest.raises(
            NotImplementedError, match="Only available.*plain dictionaries"
        ):
            entity_ops(**original.key.model_dump())

    # All is empty
    assert entity_ops.get(original) is None
    assert entity_ops.get(original.key) is None
    assert not entity_ops.exists(original)
    assert not entity_ops.exists(original.key)

    # Write the entity to the DB, check in many ways that it exists
    entity_ops.create(original)
    assert entity_ops.exists(original)
    assert entity_ops.get(original) == original

    # Try to re-create it
    with pytest.raises(ConfigCollision):
        entity_ops.create(original)
    with pytest.raises(ConfigCollision):
        entity_ops.create(updated)

    # Update it and check updated contents
    # Note that we can use either "original" or "updated" to index, since they
    # shares the same key
    entity_ops.update(updated)
    assert entity_ops.get(original) == updated
    assert entity_ops.get(updated) == updated
    entity_ops.create_or_update(original)
    assert entity_ops.get(original) == original
    assert entity_ops.get(updated) == original

    # Delete
    entity_ops.delete(original)
    assert entity_ops.get(original) is None
    assert entity_ops.get(updated) is None

    # Can't delete twice
    with pytest.raises(ConfigVanished):
        entity_ops.delete(original)
    with pytest.raises(ConfigVanished):
        entity_ops.delete(updated)

    # Can't update now that it's gone
    with pytest.raises(ConfigVanished):
        entity_ops.update(original)
    with pytest.raises(ConfigVanished):
        entity_ops.update(updated)

    # Re-create
    entity_ops.create_or_update(updated)
    assert entity_ops.get(original) == updated
    assert entity_ops.get(updated) == updated


@pytest.mark.parametrize(
    "ops_func, entity",
    [
        (_create_example_model_ops, SAMPLE_ENTITY),
        (_create_example_model_ops_str_key, SAMPLE_STR_KEY_ENTITY),
    ],
)
def test_take_ownership(ops_func, entity, cfg):
    """Taking ownership works and uses the Config's ownership record."""
    for txn in cfg.txn():
        # Store an entity to check that revoking the lease doesn't remove it
        ops = ops_func(txn)
        ops.create(entity)

    def _assert_owned(ops_entity, owned, owner_info):
        ownership_ops = ops_entity.ownership(entity)
        assert owned == ownership_ops.is_owned()
        assert owned == ownership_ops.is_owned_by_this_process()
        assert owner_info == ownership_ops.get()
        assert owned == ops_entity.is_alive(entity)
        # the entity itself always remains
        assert ops_entity.exists(entity)

    # Take ownership explicitly
    for txn in cfg.txn():
        ops = ops_func(txn)
        ops.ownership(entity).take()
        _assert_owned(ops, True, cfg.owner)

    # Remove client lease, there's no ownership anymore
    cfg.revoke_lease()
    for txn in cfg.txn():
        ops = ops_func(txn)
        _assert_owned(ops, False, None)

    # Take ownership via indirect method
    for txn in cfg.txn():
        ops = ops_func(txn)
        ops.take_ownership_if_not_alive(entity)
        _assert_owned(ops, True, cfg.owner)


@pytest.mark.parametrize(
    "ops_type, index",
    [
        ("ops", SAMPLE_ENTITY_KEY),
        ("ops", SAMPLE_ENTITY),
        ("ops_str_key", SAMPLE_ENTITY_STR_KEY),
        ("ops_str_key", SAMPLE_STR_KEY_ENTITY),
    ],
)
def test_entity_state(ops_type, index, request):
    """State can be managed successfully."""
    entity_ops = request.getfixturevalue(ops_type)
    state_ops = entity_ops.state(index)

    sate_original = {"a": 1}
    sate_updated = {"a": 0}
    assert state_ops.get() is None
    state_ops.create(sate_original)
    assert state_ops.get() == sate_original
    state_ops.update(sate_updated)
    assert state_ops.get() == sate_updated


@pytest.mark.parametrize(
    "ops_type, index, expected_path",
    [
        ("ops", SAMPLE_ENTITY_KEY, "/example/0:a"),
        ("ops", SAMPLE_ENTITY, "/example/0:a"),
        (
            "ops_str_key",
            SAMPLE_ENTITY_STR_KEY,
            "/example/example-test-20240101-12345",
        ),
        (
            "ops_str_key",
            SAMPLE_STR_KEY_ENTITY,
            "/example/example-test-20240101-12345",
        ),
    ],
)
def test_path(ops_type, index, expected_path, request):
    """Path information for an individual entity"""
    entity_ops = request.getfixturevalue(ops_type)
    assert entity_ops.path(index) == expected_path


@pytest.fixture(name="populated_ops")
def populated_ops_fixture(
    ops: _ExampleEntityOperations,
) -> _ExampleEntityOperations:
    assert ops.list_values() == []
    for number, alpha in itertools.product(
        ("000", "010", "020", "999"), ("aaa", "aba", "aca", "zzz")
    ):
        key = _ExampleEntity.Key(number=number, alpha=alpha)
        ops.create(_ExampleEntity(key=key, data=0))
    assert len(ops.list_values()) == 16
    return ops


def test_list_keys(populated_ops: _ExampleEntityOperations):
    """Listing keys works as expected, with exact/prefix/suffix variants."""
    ops = populated_ops
    for key in ops.query_keys():
        assert isinstance(key, _ExampleEntity.Key)
    assert len(ops.list_keys(number="000")) == 4
    assert len(ops.list_keys(number="010")) == 4
    assert len(ops.list_keys(number_prefix="0")) == 12
    assert len(ops.list_keys(number_prefix="9")) == 4
    assert len(ops.list_keys(number_suffix="9")) == 4


def test_empty_query_argument(populated_ops: _ExampleEntityOperations):
    """Empty query arguments are ignored."""
    ops = populated_ops
    assert len(ops.list_keys(number="")) == 16
    assert len(ops.list_keys(number="", alpha_prefix="a")) == 12


def test_list_values(populated_ops: _ExampleEntityOperations):
    """Listing values works as expected."""
    for key, value in populated_ops.query_values():
        assert key == value.key
        assert isinstance(value, _ExampleEntity)


def test_old_entity_ops(cfg, ops_str_key):
    """
    EntityOperations work on entities loaded from
    an older version of the Configuration DB,
    i.e. where "key" is not part of the saved
    JSON/dict data.
    """
    for txn in cfg.txn():
        # creating an entry but with data that does not contain the "key";
        # create directly with txn.arbitrary, instead
        # of the given entity's operations
        txn.arbitrary(
            f"{_ExampleEntityStrKeyOperations.PREFIX}/{SAMPLE_ENTITY_STR_KEY}"
        ).create({"data": 1})

    keys = ops_str_key.list_keys()
    assert len(keys) == 1
    assert keys[0] == SAMPLE_ENTITY_STR_KEY

    result = ops_str_key.get(SAMPLE_ENTITY_STR_KEY)
    assert result == _ExampleEntityStrKey(key=SAMPLE_ENTITY_STR_KEY, data=1)
